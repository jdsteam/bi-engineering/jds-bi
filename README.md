# JDS BI Monorepo

> Tired to copy and paste the code

## Library

- [CDK](https://www.npmjs.com/package/@jds-bi/cdk)
- [Core](https://www.npmjs.com/package/@jds-bi/core)
- [Icons](https://www.npmjs.com/package/@jds-bi/icons)

This project was generated using [Nx](https://nx.dev).
