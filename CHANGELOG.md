# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [5.0.0](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v4.0.2...v5.0.0) (2024-07-10)

### [4.0.2](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v4.0.1...v4.0.2) (2024-07-10)

### [4.0.1](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v4.0.0...v4.0.1) (2024-07-10)

## [4.0.0](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v3.1.0...v4.0.0) (2024-07-10)

## [3.1.0](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v3.0.1...v3.1.0) (2023-06-28)

### [3.0.1](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v3.0.0...v3.0.1) (2023-06-27)

## [3.0.0](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.13...v3.0.0) (2023-06-27)

### [2.0.13](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.12...v2.0.13) (2023-04-20)

### Bug Fixes

- **icons:** bars, bell, & circle question ([b301e0d](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/b301e0d797bb585c1311986ce82c049f3e28d13e))

### [2.0.12](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.11...v2.0.12) (2023-04-17)

### Bug Fixes

- **icons:** check & check double ([8dc1a49](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/8dc1a4948e56f5fb7dffd0129f61646c47548a77))

### [2.0.11](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.10...v2.0.11) (2023-04-17)

### Bug Fixes

- **icons:** check & check double ([baf5592](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/baf55926f8e722d33a0e112258634338857eae00))

### [2.0.10](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.9...v2.0.10) (2023-04-14)

### Bug Fixes

- **core:** dropdown width ([768945a](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/768945aa27f8fb43927679aec837ee19a949401b))

### [2.0.9](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.8...v2.0.9) (2023-03-15)

### Features

- **icons:** building & map ([e60f6c1](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/e60f6c16abf9c67d6a7bb54bab010a2b9a6e9ebd))

### [2.0.8](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.7...v2.0.8) (2023-03-06)

### Bug Fixes

- **core:** nav item injection token ([2de5fc3](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/2de5fc3239af86ea9c3598d2f3f357165b44882e))

### [2.0.7](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.6...v2.0.7) (2023-03-06)

### Bug Fixes

- **core:** nav injection token ([fb3731b](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/fb3731baeb71a595e44f6156306e59b94a478fb5))

### [2.0.6](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.5...v2.0.6) (2023-03-06)

### Bug Fixes

- **core:** button disabled ([fd96097](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/fd960974f93d110cd9a7e671a6c868c05cf42506))

### [2.0.5](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.4...v2.0.5) (2023-03-06)

### Bug Fixes

- **core:** modal model ([adf326a](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/adf326a412e593fa7d8b66fdb1ea79d915643059))

### [2.0.4](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.3...v2.0.4) (2023-03-03)

### Bug Fixes

- **core:** dropdown scroll ([0935447](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/0935447ea69e805d7968da30d61aecc521aacd0e))

### [2.0.3](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.2...v2.0.3) (2023-03-03)

### Bug Fixes

- **core:** accordion collapse ([df4d9d3](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/df4d9d341fb9cf73bbcc273c6328df6ec11bc150))

### [2.0.2](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.1...v2.0.2) (2023-03-02)

### Bug Fixes

- publish ([2d886ba](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/2d886ba66c030b66e0f18a213f1733fa69e4afcb))

### [2.0.1](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v2.0.0...v2.0.1) (2023-03-02)

### Bug Fixes

- **cdk:** version ([17a5aef](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/17a5aefdd83da0e877d53fa00c588fa08d0440dc))

## [2.0.0](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.2.4...v2.0.0) (2023-03-02)

### Features

- **core:** modal ([6bff7ff](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/6bff7ff468f6cb16c3524385361f9dc07ba5b439))

### Bug Fixes

- **core:** dropdown hover ([b94d184](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/b94d184b847bccf519102eec01c05db67f26483b))
- **core:** scroll ([6b10edf](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/6b10edf2192064c85e112edac6d9865655e000f5))
- **core:** scrollbar ([5151835](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/5151835ab831d7d31d57b26170b2da83dc6350c8))
- readme ([856ecf7](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/856ecf75fb6b8500010f5a244bd0c1de2400f805))
- version ([f1b11d8](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/f1b11d83de32b1c157f145e64cab4d770d797850))

### [1.2.4](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.2.3...v1.2.4) (2023-02-18)

### Bug Fixes

- **angular:** icon - add arrow & cloud ([1f1b0df](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/1f1b0dfc11512b24d705c55cae1652fa55424aac))

### [1.2.3](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.2.2...v1.2.3) (2023-02-16)

### Bug Fixes

- **angular:** button - focus within ([c91bc82](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/c91bc82d5f1e4208c4bc5ee745a45e47dbca0364))

### [1.2.2](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.2.1...v1.2.2) (2023-02-16)

### Bug Fixes

- **angular:** icon - color ([e265762](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/e265762eb5bbf3fa4fa848997a0b096579fb8951))

### [1.2.1](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.2.0...v1.2.1) (2023-02-16)

### Bug Fixes

- **angular:** icon - circle xmark ([370716c](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/370716c4816aa8183f30aac613801e429dc6c9b7))

## [1.2.0](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.1.6...v1.2.0) (2023-02-16)

### Bug Fixes

- **angular:** input size ([ab31c30](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/ab31c300027c9bf78982242e3147aa6344360021))

### [1.1.6](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.1.5...v1.1.6) (2023-02-11)

### [1.1.5](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.1.4...v1.1.5) (2023-02-11)

### [1.1.4](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.1.3...v1.1.4) (2023-02-10)

### [1.1.3](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.1.2...v1.1.3) (2023-02-10)

### [1.1.2](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/compare/v1.1.1...v1.1.2) (2023-02-10)

### 1.1.1 (2023-02-10)

### Features

- angular ([1ef8537](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/1ef8537f05d2e4dd6c3d0e7374f2c7fa21f9bf4d))
- angular ([e81caf6](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/e81caf61ca950e2793f5fefcbc5d14020431e6c9))
- angular library ([5cc3e42](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/5cc3e4216668e648d80e43702f129f3712161ec6))
- angular library ([7f4df82](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/7f4df82152b164d6415866d4aff51d7b53fddc1b))
- **angular:** accordion ([265f918](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/265f918f0a3589ab9480cd8fc51719a52a518831))
- **angular:** accordion ([022c261](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/022c261cb081395c4d6928588133b096b20a7ada))
- **angular:** badge ([2bf640a](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/2bf640a311b930baaa0c4adc64f1ad3e3046fbee))
- **angular:** button ([702eaa7](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/702eaa7508dd63dba0b173628b3ac5271477e3bb))
- **angular:** icon ([0f1d130](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/0f1d1302dc9595af919f09fca6af4a0cf0793285))
- **angular:** input ([6987d73](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/6987d73d096cc195811880428e50846c194e50ad))
- **angular:** nav ([1d0ef41](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/1d0ef412f453c2263ea3c6b7ad43ff36047f1eb3))
- **angular:** pagination ([b709d75](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/b709d75196e52848b9059577abc384ab89698ff6))
- **angular:** readme ([08c3a66](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/08c3a66b66e86de2454c620cbb7ea4075bfe4137))
- **angular:** readme ([fa14f94](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/fa14f94700f6ed3799f7094ad8d17b996867b3c8))
- **angular:** table head ([87d5e47](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/87d5e471d73f21dac9ede8b952429ec94f069d98))
- **angular:** version ([9bb2b0d](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/9bb2b0dfb22b5feef773262a18569df6dae4d0c2))
- **angular:** version ([bc3bcff](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/bc3bcffab7c5ca665d6f79bb7b3fe072784ef167))
- **angular:** version ([1ca11f9](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/1ca11f9ba06984b14cadefa43bac99f77970f2b1))
- **angular:** version ([1ca5cf3](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/1ca5cf33faf732c9de6d81c01d9961784887b8a7))
- **angular:** version ([0ed7478](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/0ed747842df064964dee148bb95e40ce6359f0b5))
- **angular:** version ([ea3d2e8](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/ea3d2e898f64b8cf8d6279231f0b84bf1ec559e1))
- **angular:** version ([849681f](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/849681f4a9b403ac89595c7a61be144960ddf86b))
- constants & utils library ([56e72e6](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/56e72e6e2c4a6779517297681fa7e2f7e9fb2edf))
- first commit ([29c3e62](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/29c3e62dfc177d5bcb72e1829c182ad47c958bbc))
- np ([8ea04d7](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/8ea04d717328bcbf53bd38be321502907cf66183))
- packages ([83703bf](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/83703bfa79c3cd97d05092a03eff7d7f54955131))
- setup ([0ced638](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/0ced638498d27698fcd58e7c1afdd3f69db84c83))
- **utils:** version ([efbd7bf](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/efbd7bfbdfbe48d8a26121466e55c198a8f8e747))
- vscode settings ([e8612f6](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/e8612f6b972d2ad9ad7a7847b433956f2edc32f1))

### Bug Fixes

- **all:** readme ([2a95cff](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/2a95cff6f8df5e3c03833712be5e83e20f62ce29))
- **all:** version ([b43be63](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/b43be63107cadfd722a09512a03a45262ec3e55f))
- **angular:** accordion ([4759e28](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/4759e28c38d2ae0ead7fe073043b5c0bf50fbf58))
- **angular:** accordion ([d7a222b](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/d7a222b42d27f38a5f8f7c956b4f5a0825f6383c))
- **angular:** accordion ([89b3487](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/89b3487cfb3802639ced82ba4020bb2641c8f37b))
- **angular:** button ([22ad861](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/22ad861427e1510c4f39b48c6414f615e84f8e6f))
- **angular:** button ([fc4f31b](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/fc4f31b0c00f74a755efe4845c7e80454cfde2ff))
- **angular:** change detection ([7286e69](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/7286e6911da06bc7e3e2e20190e3f70a295b25fb))
- **angular:** change detection ([c61a229](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/c61a229bd4b5b64dc48754d42bfd2b3c382b9737))
- **angular:** dependency ([c6ab989](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/c6ab989e60b1683e593b1e2873c522eb2950938a))
- **angular:** icon ([a11af1e](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/a11af1e320e1497cabd10d2a28161ea0d3953386))
- **angular:** icon ([3097663](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/3097663facf4adc522197e7ec24730570d3bfcd6))
- **angular:** icon ([113600d](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/113600da5e0dd06d2be34639fdd11ebe602235ce))
- **angular:** icon ([9da7770](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/9da7770b22c6fb5483a4541e01d1771f023c41ff))
- **angular:** icon ([3337250](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/33372507ab9d534b3cc645279b3b71873729a7ac))
- **angular:** icon ([70bcae8](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/70bcae855ba9d3d87e42b329c6e12953b0cbbbf1))
- **angular:** icon ([aba8f47](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/aba8f47281f69c96c3aafd55276f281005339a5c))
- **angular:** icon ([84d3431](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/84d3431611ee841159e1b9705015aa2e8a791f6b))
- **angular:** icon ([c985d3c](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/c985d3c9eb01f0a195ed569936705608010bd5ff))
- **angular:** icon ([d0226f5](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/d0226f518f281c03338aea2770870e3c9431179a))
- **angular:** icon ([cb19d98](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/cb19d98cdfee5de87c725a0689701fbeafd4719f))
- **angular:** icon ([36b8f26](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/36b8f263b8d03d798db24bf940e968f6a8a7dbf8))
- **angular:** icon ([033574d](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/033574d98cc6cea310eecacb1a48054f5833d3e8))
- **angular:** nav ([f67080f](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/f67080fa17a04b4d74069f90b8291218bf0e2a77))
- **angular:** package ([0cdc8b9](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/0cdc8b9272cad4cc380c970e19799221fc427d66))
- **angular:** pagination ([3eddc74](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/3eddc74a81d4f5ab218056d71e4b4dc707dddcc8))
- **angular:** pagination - style ([8208b15](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/8208b154466103efb3256b7f67d8265aa6cb5d38))
- **angular:** readme ([a4eadb5](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/a4eadb543f7d7b84bed177c45eb063d52c271838))
- **angular:** redame ([847da48](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/847da48df2049d0fef2305243d2f01494afa5773))
- **angular:** rxjs ([156a85d](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/156a85d396b08b9f5b80d22d850cd0095b5d6791))
- **angular:** table head ([b593b47](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/b593b4769a0b33b285b0ab31aab7b20c185d2ff4))
- **angular:** table head ([132a949](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/132a9492730b8c83b89721ba1877f735cf7a0a5b))
- **angular:** version ([a3451c7](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/a3451c77f2aeb88c18520cf4e61d2f15493ca8f0))
- **angular:** version ([1d3ba09](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/1d3ba091a34f0cfb08ebc9740bef80a48ba90604))
- **angular:** version ([1c0df01](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/1c0df01365c7774159d17d04a06859feee087c3e))
- **angular:** version ([cdb6ffe](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/cdb6ffe8d9f39e3c11e4b213203dd32cbfce9007))
- **angular:** version ([46af64d](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/46af64dbe2b3172d7fa47f9580701e2b7f95fa36))
- **angular:** version ([9f9f9ce](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/9f9f9ce30581813a132b005d968aaff1d284b135))
- **angular:** version ([6816ff6](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/6816ff6f5ee15a0192c576850dc2b8a18375f335))
- **angular:** version ([122655c](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/122655c3f10287e31931d4890cc554c3fb8c3f10))
- **angular:** version ([60af71b](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/60af71b9a5279ff676757a9d5acf5e580fd2eab1))
- **angular:** version ([abb9fd2](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/abb9fd2fb1feed6187ba6ffcb478977d437c1450))
- **angular:** version ([17c9785](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/17c9785420f6f91087ebca1bb3fc7d6d491962cd))
- **angular:** version ([1cc560c](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/1cc560c4c2570633dec05b03c3da30b6d43db2d7))
- **angular:** version ([9383e71](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/9383e71ef63a53d9531d2465389f84d873885946))
- **angular:** version ([8c03a47](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/8c03a475da459b217aee1c2be585e2bdb473591c))
- **angular:** version ([19d5ed4](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/19d5ed4827578d6145de1909d2aa96ca116312dd))
- **angular:** version ([9239d08](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/9239d08b82e5dd6872762e85fd0dc2453c9fbc92))
- **angular:** version ([9c79691](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/9c796910998607ff0e322b208a670e6aa0a675b9))
- **angular:** version ([4b39f18](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/4b39f189dda0e691afa1da6e25aa2c6918dd57c7))
- app angular ([2305e47](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/2305e47a7af419d16891f445a4ada4e907a029a4))
- components ([ad19a39](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/ad19a3929838a4b64cee451d0e63a3ca37b1f1ae))
- **constants:** version ([839f372](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/839f3729d8b4248f2b6ab538d58d805b2d97a984))
- eslint ([145bb46](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/145bb46db980bf21c4709be6d2ad863a397c68eb))
- readme ([24d66c4](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/24d66c46e045e9d9e7b165b857abef69b0be6759))
- tailwind ([4c9d3a2](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/4c9d3a2566a3921480823bde46b2b616dfea85dd))
- test ([78a621b](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/78a621ba81f039030994f5930ea55c0abebe1d05))
- **utils:** color scheme ([7304c63](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/7304c63ea9369c7a6213947adb8c4c4a5730f2fb))
- **utils:** version ([e56f875](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/e56f8750fba3a9ab946e1fef529ab5050113dc3b))
- version ([4012ac3](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/4012ac3303410d4d5550af0a3038dc6ab614c742))
- version ([d9fe1bc](https://gitlab.com/jdsteam/bi-engineering/jds-bi-monorepo/commit/d9fe1bc7a550d99f525e18319c3bb935d2a7bcd6))
