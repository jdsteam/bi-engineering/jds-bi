FROM node:18-alpine AS build-stage
WORKDIR /app/
COPY package*.json /app/
RUN apk update
RUN apk upgrade
RUN apk add --no-cache git
RUN npm ci
COPY ./ /app/
RUN npm run build:storybook-angular

FROM nginx:1.23.3-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build-stage /app/dist/storybook/storybook-angular/ /usr/share/nginx/html/
