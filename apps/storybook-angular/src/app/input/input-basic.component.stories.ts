import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiInputSize } from '@jds-bi/core';
import { JdsBiInputBasicComponent } from './input-basic.component';

const meta: Meta<JdsBiInputBasicComponent> = {
  title: 'Components/Input/Basic',
  component: JdsBiInputBasicComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
    size: {
      options: JdsBiInputSize,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiInputBasicComponent>;

const template = `
<input
  [colorScheme]="colorScheme"
  [size]="size"
  [valid]="valid"
  [invalid]="invalid"
  jdsBiInput
  type="text"
  placeholder="Contoh: Test Placeholder"
/>
`;

export const Basic: Story = {
  args: {
    colorScheme: 'green',
    size: 'md',
    valid: false,
    invalid: false,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
