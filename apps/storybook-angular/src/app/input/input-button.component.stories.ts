import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiInputSize } from '@jds-bi/core';
import { JdsBiInputButtonComponent } from './input-button.component';

const meta: Meta<JdsBiInputButtonComponent> = {
  title: 'Components/Input/Inline Button',
  component: JdsBiInputButtonComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
    size: {
      options: JdsBiInputSize,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiInputButtonComponent>;

const template = `
<div class="flex gap-4">
  <input
    [colorScheme]="colorScheme"
    [size]="size"
    [valid]="valid"
    [invalid]="invalid"
    jdsBiInput
    type="text"
    placeholder="Contoh: Test Placeholder"
  />
  <button
    [colorScheme]="colorScheme"
    [size]="size"
    jdsBiButton
  >
    Simpan
  </button>
</div>
`;

export const InlineButton: Story = {
  args: {
    colorScheme: 'green',
    size: 'md',
    valid: false,
    invalid: false,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
