import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type { JdsBiInputSize } from '@jds-bi/core';
import { JdsBiInputModule, JdsBiButtonModule } from '@jds-bi/core';

@Component({
  selector: 'jds-bi-input-button-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiInputModule, JdsBiButtonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="w-3/4">
      <div class="flex gap-4">
        <input
          [colorScheme]="colorScheme"
          [size]="size"
          [valid]="valid"
          [invalid]="invalid"
          jdsBiInput
          type="text"
          placeholder="Contoh: Test Placeholder"
        />
        <button
          [colorScheme]="colorScheme"
          [size]="size"
          jdsBiButton
        >
          Simpan
        </button>
      </div>
    </div>
  `,
})
export class JdsBiInputButtonComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  size: JdsBiInputSize = 'md';

  @Input()
  @jdsBiDefaultProp()
  valid = false;

  @Input()
  @jdsBiDefaultProp()
  invalid = false;
}
