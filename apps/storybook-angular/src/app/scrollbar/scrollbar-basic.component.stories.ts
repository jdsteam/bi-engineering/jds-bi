import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiScrollbarBasicComponent } from './scrollbar-basic.component';

const meta: Meta<JdsBiScrollbarBasicComponent> = {
  title: 'Components/Scrollbar/Basic',
  component: JdsBiScrollbarBasicComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
};

export default meta;
type Story = StoryObj<JdsBiScrollbarBasicComponent>;

const template = `
<jds-bi-scrollbar
  class="w-96 h-48 border-solid border-2 border-red-600"
  [hidden]="hidden"
>
  <div class="p-4">
    Competently embrace interoperable partnerships vis-a-vis multimedia
    based opportunities. Appropriately grow innovative innovation for
    extensive resources. Dramatically cultivate wireless value for
    cutting-edge collaboration and idea-sharing. Energistically negotiate
    virtual testing procedures without worldwide alignments.
    Collaboratively expedite parallel alignments with an expanded array of
    systems. Objectively extend orthogonal customer service whereas
    error-free best practices. Dynamically simplify resource maximizing
    infomediaries vis-a-vis emerging "outside the box" thinking.
    Dynamically incentivize unique platforms before fully researched
    results. Holisticly disseminate technically sound e-services via
    process-centric leadership skills. Synergistically strategize resource
    sucking innovation through intuitive "outside the box" thinking.
    Monotonectally architect standardized synergy with turnkey mindshare.
    Distinctively evolve distributed results and prospective bandwidth.
    Rapidiously create cooperative functionalities and adaptive
    alignments. Synergistically actualize one-to-one platforms rather than
    equity invested synergy. Compellingly transition go forward
    infrastructures rather than virtual data. Progressively architect
    backend e-commerce after cross-platform deliverables. Dynamically
    repurpose multifunctional experiences rather than leading-edge
    results. Phosfluorescently.
  </div>
</jds-bi-scrollbar>

<jds-bi-scrollbar
  class="w-96 h-48 border-solid border-2 border-indigo-600"
  [hidden]="hidden"
>
  <div class="p-4 whitespace-nowrap">
    Competently embrace interoperable partnerships vis-a-vis multimedia
    based opportunities.
  </div>
</jds-bi-scrollbar>
`;

export const Basic: Story = {
  args: {
    hidden: false,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
