import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { jdsBiDefaultProp } from '@jds-bi/cdk';

import { JdsBiScrollbarModule } from '@jds-bi/core';

@Component({
  selector: 'jds-bi-scrollbar-basic-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiScrollbarModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="flex flex-col gap-4">
      <jds-bi-scrollbar
        class="w-96 h-48 border-solid border-2 border-red-600"
        [hidden]="hidden"
      >
        <div class="p-4">
          Competently embrace interoperable partnerships vis-a-vis multimedia
          based opportunities. Appropriately grow innovative innovation for
          extensive resources. Dramatically cultivate wireless value for
          cutting-edge collaboration and idea-sharing. Energistically negotiate
          virtual testing procedures without worldwide alignments.
          Collaboratively expedite parallel alignments with an expanded array of
          systems. Objectively extend orthogonal customer service whereas
          error-free best practices. Dynamically simplify resource maximizing
          infomediaries vis-a-vis emerging "outside the box" thinking.
          Dynamically incentivize unique platforms before fully researched
          results. Holisticly disseminate technically sound e-services via
          process-centric leadership skills. Synergistically strategize resource
          sucking innovation through intuitive "outside the box" thinking.
          Monotonectally architect standardized synergy with turnkey mindshare.
          Distinctively evolve distributed results and prospective bandwidth.
          Rapidiously create cooperative functionalities and adaptive
          alignments. Synergistically actualize one-to-one platforms rather than
          equity invested synergy. Compellingly transition go forward
          infrastructures rather than virtual data. Progressively architect
          backend e-commerce after cross-platform deliverables. Dynamically
          repurpose multifunctional experiences rather than leading-edge
          results. Phosfluorescently.
        </div>
      </jds-bi-scrollbar>

      <jds-bi-scrollbar
        class="w-96 h-48 border-solid border-2 border-indigo-600"
        [hidden]="hidden"
      >
        <div class="p-4 whitespace-nowrap">
          Competently embrace interoperable partnerships vis-a-vis multimedia
          based opportunities.
        </div>
      </jds-bi-scrollbar>
    </div>
  `,
})
export class JdsBiScrollbarBasicComponent {
  @Input()
  @jdsBiDefaultProp()
  hidden = false;
}
