import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiInputGroupVariant, JdsBiInputSize } from '@jds-bi/core';
import { JdsBiInputGroupSearchComponent } from './input-group-search.component';

const meta: Meta<JdsBiInputGroupSearchComponent> = {
  title: 'Components/Input Group/Search',
  component: JdsBiInputGroupSearchComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
    size: {
      options: JdsBiInputSize,
      control: { type: 'select' },
    },
    variant: {
      options: JdsBiInputGroupVariant,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiInputGroupSearchComponent>;

const template = `
<div
  [size]="size"
  variant="element"
  jdsBiInputGroup
>
  <input
    [colorScheme]="colorScheme"
    [size]="size"
    [valid]="valid"
    [invalid]="invalid"
    jdsBiInput
    type="text"
    placeholder="Cari"
  />
  <jds-bi-input-right-element>
    <button
      jdsBiButton
      [colorScheme]="colorScheme"
      [size]="size === 'sm' ? 'sm' : 'md'"
      variant="solid"
    >
      <span>Cari</span>
      <jds-bi-icon
        [size]="size === 'sm' ? 12 : 14"
        class="ml-2"
        name="magnifying-glass"
        />
    </button>
  </jds-bi-input-right-element>
</div>
`;

export const Search: Story = {
  args: {
    colorScheme: 'green',
    size: 'md',
    variant: 'element',
    valid: false,
    invalid: false,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
