import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type { JdsBiInputGroupVariant, JdsBiInputSize } from '@jds-bi/core';
import { JdsBiInputModule } from '@jds-bi/core';

import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconMagnifyingGlass,
} from '@jds-bi/icons';

@Component({
  selector: 'jds-bi-input-group-element-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiInputModule, JdsBiIconsModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="w-3/4">
      <div class="flex flex-col gap-4">
        <div
          [size]="size"
          variant="element"
          jdsBiInputGroup
        >
          <jds-bi-input-left-element>
            <jds-bi-icon
              [size]="size === 'sm' ? 12 : 14"
              name="magnifying-glass"
            />
          </jds-bi-input-left-element>
          <input
            [colorScheme]="colorScheme"
            [size]="size"
            [valid]="valid"
            [invalid]="invalid"
            jdsBiInput
            type="text"
            placeholder="Cari"
          />
        </div>

        <div
          [size]="size"
          variant="element"
          jdsBiInputGroup
        >
          <input
            [colorScheme]="colorScheme"
            [size]="size"
            [valid]="valid"
            [invalid]="invalid"
            jdsBiInput
            type="text"
            placeholder="Cari"
          />
          <jds-bi-input-right-element>
            <jds-bi-icon
              [size]="size === 'sm' ? 12 : 14"
              name="magnifying-glass"
            />
          </jds-bi-input-right-element>
        </div>
      </div>
    </div>
  `,
})
export class JdsBiInputGroupElementComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  size: JdsBiInputSize = 'md';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiInputGroupVariant = 'element';

  @Input()
  @jdsBiDefaultProp()
  valid = false;

  @Input()
  @jdsBiDefaultProp()
  invalid = false;

  constructor(private jdsBiIconsService: JdsBiIconsService) {
    this.jdsBiIconsService.registerIcons([iconMagnifyingGlass]);
  }
}
