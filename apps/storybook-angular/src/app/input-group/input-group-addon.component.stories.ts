import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiInputGroupVariant, JdsBiInputSize } from '@jds-bi/core';
import { JdsBiInputGroupAddonComponent } from './input-group-addon.component';

const meta: Meta<JdsBiInputGroupAddonComponent> = {
  title: 'Components/Input Group/Addon',
  component: JdsBiInputGroupAddonComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
    size: {
      options: JdsBiInputSize,
      control: { type: 'select' },
    },
    variant: {
      options: JdsBiInputGroupVariant,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiInputGroupAddonComponent>;

const template = `
<div
  [size]="size"
  jdsBiInputGroup
>
  <span jdsBiInputLeftAddon>Prefix</span>
  <input
    [colorScheme]="colorScheme"
    [size]="size"
    [valid]="valid"
    [invalid]="invalid"
    jdsBiInput
    type="text"
    placeholder="Contoh: Test Placeholder"
  />
</div>

<div
  [size]="size"
  jdsBiInputGroup
>
  <input
    [colorScheme]="colorScheme"
    [size]="size"
    [valid]="valid"
    [invalid]="invalid"
    jdsBiInput
    type="text"
    placeholder="Contoh: Test Placeholder"
  />
  <span jdsBiInputRightAddon>Suffix</span>
</div>

<div
  [size]="size"
  jdsBiInputGroup
>
  <input
    [colorScheme]="colorScheme"
    [size]="size"
    [valid]="valid"
    [invalid]="invalid"
    jdsBiInput
    type="text"
    placeholder="Contoh: Test Placeholder"
  />
  <button
    [colorScheme]="colorScheme"
    [size]="size"
    jdsBiButton
    jdsBiInputRightAddon
    variant="input"
  >
    Button
  </button>
</div>
`;

export const Addon: Story = {
  args: {
    colorScheme: 'green',
    size: 'md',
    variant: 'addon',
    valid: false,
    invalid: false,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
