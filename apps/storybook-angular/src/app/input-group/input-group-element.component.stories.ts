import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiInputGroupVariant, JdsBiInputSize } from '@jds-bi/core';
import { JdsBiInputGroupElementComponent } from './input-group-element.component';

const meta: Meta<JdsBiInputGroupElementComponent> = {
  title: 'Components/Input Group/Element',
  component: JdsBiInputGroupElementComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
    size: {
      options: JdsBiInputSize,
      control: { type: 'select' },
    },
    variant: {
      options: JdsBiInputGroupVariant,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiInputGroupElementComponent>;

const template = `
<div
  [size]="size"
  variant="element"
  jdsBiInputGroup
>
  <jds-bi-input-left-element>
    <jds-bi-icon
      [size]="size === 'sm' ? 12 : 14"
      name="magnifying-glass"
    />
  </jds-bi-input-left-element>
  <input
    [colorScheme]="colorScheme"
    [size]="size"
    [valid]="valid"
    [invalid]="invalid"
    jdsBiInput
    type="text"
    placeholder="Cari"
  />
</div>

<div
  [size]="size"
  variant="element"
  jdsBiInputGroup
>
  <input
    [colorScheme]="colorScheme"
    [size]="size"
    [valid]="valid"
    [invalid]="invalid"
    jdsBiInput
    type="text"
    placeholder="Cari"
  />
  <jds-bi-input-right-element>
    <jds-bi-icon
      [size]="size === 'sm' ? 12 : 14"
      name="magnifying-glass"
    />
  </jds-bi-input-right-element>
</div>
`;

export const Element: Story = {
  args: {
    colorScheme: 'green',
    size: 'md',
    variant: 'element',
    valid: false,
    invalid: false,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
