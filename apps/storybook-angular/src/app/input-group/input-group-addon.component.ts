import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type { JdsBiInputGroupVariant, JdsBiInputSize } from '@jds-bi/core';
import { JdsBiInputModule, JdsBiButtonModule } from '@jds-bi/core';

@Component({
  selector: 'jds-bi-input-group-addon-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiInputModule, JdsBiButtonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="w-3/4">
      <div class="flex flex-col gap-4">
        <div
          [size]="size"
          jdsBiInputGroup
        >
          <span jdsBiInputLeftAddon>Prefix</span>
          <input
            [colorScheme]="colorScheme"
            [size]="size"
            [valid]="valid"
            [invalid]="invalid"
            jdsBiInput
            type="text"
            placeholder="Contoh: Test Placeholder"
          />
        </div>

        <div
          [size]="size"
          jdsBiInputGroup
        >
          <input
            [colorScheme]="colorScheme"
            [size]="size"
            [valid]="valid"
            [invalid]="invalid"
            jdsBiInput
            type="text"
            placeholder="Contoh: Test Placeholder"
          />
          <span jdsBiInputRightAddon>Suffix</span>
        </div>

        <div
          [size]="size"
          jdsBiInputGroup
        >
          <input
            [colorScheme]="colorScheme"
            [size]="size"
            [valid]="valid"
            [invalid]="invalid"
            jdsBiInput
            type="text"
            placeholder="Contoh: Test Placeholder"
          />
          <button
            [colorScheme]="colorScheme"
            [size]="size"
            jdsBiButton
            jdsBiInputRightAddon
            variant="input"
          >
            Button
          </button>
        </div>
      </div>
    </div>
  `,
})
export class JdsBiInputGroupAddonComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  size: JdsBiInputSize = 'md';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiInputGroupVariant = 'addon';

  @Input()
  @jdsBiDefaultProp()
  valid = false;

  @Input()
  @jdsBiDefaultProp()
  invalid = false;
}
