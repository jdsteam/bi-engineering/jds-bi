import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type { JdsBiNavOrientation, JdsBiNavVariant } from '@jds-bi/core';
import { JdsBiNavModule } from '@jds-bi/core';

@Component({
  selector: 'jds-bi-nav-basic-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiNavModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ul
      #nav="jdsBiNav"
      [(activeId)]="activeId"
      [variant]="variant"
      [orientation]="orientation"
      [destroyOnHide]="destroyOnHide"
      jdsBiNav
    >
      <li [jdsBiNavItem]="1">
        <a jdsBiNavLink>Dataset</a>
        <ng-template jdsBiNavContent>
          <p>
            Raw denim you probably haven't heard of them jean shorts Austin.
            Nesciunt tofu stumptown aliqua, retro synth master cleanse.
          </p>
        </ng-template>
      </li>
      <li [jdsBiNavItem]="2">
        <a jdsBiNavLink>Indikator</a>
        <ng-template jdsBiNavContent>
          <p>
            Exercitation +1 labore velit, blog sartorial PBR leggings next level
            wes anderson artisan four loko farm-to-table craft beer twee.
          </p>
        </ng-template>
      </li>
      <li [jdsBiNavItem]="3">
        <a jdsBiNavLink>Visualisasi</a>
        <ng-template jdsBiNavContent>
          <p>
            Homo nostrud organic, assumenda labore aesthetic magna delectus
            mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente
            labore stumptown.
          </p>
        </ng-template>
      </li>
    </ul>

    <div
      class="mt-2"
      [jdsBiNavOutlet]="nav"
    ></div>
  `,
})
export class JdsBiNavBasicComponent {
  @Input()
  @jdsBiDefaultProp()
  activeId = 1;

  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiNavVariant = 'outline';

  @Input()
  @jdsBiDefaultProp()
  orientation: JdsBiNavOrientation = 'horizontal';

  @Input()
  @jdsBiDefaultProp()
  destroyOnHide = true;
}
