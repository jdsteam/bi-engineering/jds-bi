import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiNavOrientation, JdsBiNavVariant } from '@jds-bi/core';
import { JdsBiNavBasicComponent } from './nav-basic.component';

const meta: Meta<JdsBiNavBasicComponent> = {
  title: 'Components/Nav/Basic',
  component: JdsBiNavBasicComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
    variant: {
      options: JdsBiNavVariant,
      control: { type: 'select' },
    },
    orientation: {
      options: JdsBiNavOrientation,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiNavBasicComponent>;

const template = `
<ul
  #nav="jdsBiNav"
  [(activeId)]="activeId"
  [variant]="variant"
  [orientation]="orientation"
  [destroyOnHide]="destroyOnHide"
  jdsBiNav
>
  <li [jdsBiNavItem]="1">
    <a jdsBiNavLink>Dataset</a>
    <ng-template jdsBiNavContent>
      <p>
        Raw denim you probably haven't heard of them jean shorts Austin.
        Nesciunt tofu stumptown aliqua, retro synth master cleanse.
      </p>
    </ng-template>
  </li>
  <li [jdsBiNavItem]="2">
    <a jdsBiNavLink>Indikator</a>
    <ng-template jdsBiNavContent>
      <p>
        Exercitation +1 labore velit, blog sartorial PBR leggings next level
        wes anderson artisan four loko farm-to-table craft beer twee.
      </p>
    </ng-template>
  </li>
  <li [jdsBiNavItem]="3">
    <a jdsBiNavLink>Visualisasi</a>
    <ng-template jdsBiNavContent>
      <p>
        Homo nostrud organic, assumenda labore aesthetic magna delectus
        mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente
        labore stumptown.
      </p>
    </ng-template>
  </li>
</ul>

<div
  class="mt-2"
  [jdsBiNavOutlet]="nav"
></div>
`;

export const Basic: Story = {
  args: {
    activeId: 1,
    colorScheme: 'green',
    variant: 'outline',
    orientation: 'horizontal',
    destroyOnHide: true,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
