import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type { JdsBiModalSize, JdsBiModalPosition } from '@jds-bi/core';
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
} from '@jds-bi/core';

import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

@Component({
  selector: 'jds-bi-modal-basic-storybook',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiButtonModule,
    JdsBiModalModule,
    JdsBiIconsModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <button
      (click)="openModal()"
      jdsBiButton
    >
      Open Modal
    </button>

    <jds-bi-modal
      id="modal-basic-storybook"
      #modal
      [size]="size"
      [position]="position"
      [isClose]="isClose"
    >
      <ng-container ngProjectAs="header">
        <h3>Title</h3>
        <button
          class="t-button-close"
          (click)="closeModal()"
          role="button"
          type="button"
        >
          <jds-bi-icon
            [size]="14"
            name="xmark"
          />
        </button>
      </ng-container>
      <ng-container ngProjectAs="body">
        <div>Body</div>
      </ng-container>
      <ng-container ngProjectAs="footer">
        <div>Footer</div>
      </ng-container>
    </jds-bi-modal>
  `,
})
export class JdsBiModalBasicComponent {
  @Input()
  @jdsBiDefaultProp()
  size: JdsBiModalSize = 'md';

  @Input()
  @jdsBiDefaultProp()
  position: JdsBiModalPosition = 'start';

  @Input()
  @jdsBiDefaultProp()
  isClose = true;

  constructor(
    private jdsBiModalService: JdsBiModalService,
    private JdsBiIconService: JdsBiIconsService,
  ) {
    this.JdsBiIconService.registerIcons([iconXmark]);
  }

  openModal() {
    this.jdsBiModalService.open('modal-basic-storybook');
  }

  closeModal() {
    this.jdsBiModalService.close('modal-basic-storybook');
  }
}
