import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiModalSize, JdsBiModalPosition } from '@jds-bi/core';
import { JdsBiModalBasicComponent } from './modal-basic.component';

const meta: Meta<JdsBiModalBasicComponent> = {
  title: 'Components/Modal/Basic',
  component: JdsBiModalBasicComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    size: {
      options: JdsBiModalSize,
      control: { type: 'select' },
    },
    position: {
      options: JdsBiModalPosition,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiModalBasicComponent>;

const template = `
<button
  (click)="openModal()"
  jdsBiButton
>
  Open Modal
</button>

<jds-bi-modal
  id="modal-basic-storybook"
  #modal
  [size]="size"
  [position]="position"
  [isClose]="isClose"
>
  <ng-container ngProjectAs="header">
    <h3>Title</h3>
    <button
      class="t-button-close"
      (click)="closeModal()"
      role="button"
      type="button"
    >
      <jds-bi-icon
        [size]="14"
        name="xmark"
      />
    </button>
  </ng-container>
  <ng-container ngProjectAs="body">
    <div>Body</div>
  </ng-container>
  <ng-container ngProjectAs="footer">
    <div>Footer</div>
  </ng-container>
</jds-bi-modal>
`;

export const Basic: Story = {
  args: {
    size: 'md',
    position: 'start',
    isClose: true,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
