import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  JdsBiRootModule,
  JdsBiDropdownModule,
  JdsBiButtonModule,
  JdsBiDropdownWidth,
  JdsBiHorizontalDirection,
  JdsBiVerticalDirection,
} from '@jds-bi/core';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

@Component({
  selector: 'jds-bi-dropdown-hover-storybook',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiRootModule,
    JdsBiDropdownModule,
    JdsBiButtonModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <jds-bi-root>
      <button
        [jdsBiDropdownAlign]="jdsBiDropdownAlign"
        [jdsBiDropdownDirection]="jdsBiDropdownDirection"
        [jdsBiDropdownLimitWidth]="jdsBiDropdownLimitWidth"
        [jdsBiDropdownMinWidth]="jdsBiDropdownMinWidth"
        [jdsBiDropdownMinHeight]="jdsBiDropdownMinHeight"
        [jdsBiDropdownMaxHeight]="jdsBiDropdownMaxHeight"
        [jdsBiDropdownOffset]="jdsBiDropdownOffset"
        [jdsBiDropdown]="dropdownContent"
        jdsBiDropdownHover
        jdsBiButton
        type="button"
      >
        Hover me
      </button>

      <ng-template #dropdownContent>
        <div class="p-4">
          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
          terry richardson ad squid. 3 wolf moon officia aute, non cupidatat
          skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
          Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
          single-origin coffee nulla assumenda shoreditch et. Nihil anim
          keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
          sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
          you probably haven't heard of them accusamus labore sustainable VHS.
          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
          terry richardson ad squid. 3 wolf moon officia aute, non cupidatat
          skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
          Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
          single-origin coffee nulla assumenda shoreditch et. Nihil anim
          keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
          sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
          you probably haven't heard of them accusamus labore sustainable VHS.
        </div>
      </ng-template>
    </jds-bi-root>
  `,
})
export class JdsBiDropdownHoverComponent {
  @Input()
  @jdsBiDefaultProp()
  jdsBiDropdownAlign: JdsBiHorizontalDirection = 'left';

  @Input()
  @jdsBiDefaultProp()
  jdsBiDropdownDirection: JdsBiVerticalDirection | null = null;

  @Input()
  @jdsBiDefaultProp()
  jdsBiDropdownLimitWidth: JdsBiDropdownWidth = 'auto';

  @Input()
  @jdsBiDefaultProp()
  jdsBiDropdownMinWidth = 160;

  @Input()
  @jdsBiDefaultProp()
  jdsBiDropdownMinHeight = 80;

  @Input()
  @jdsBiDefaultProp()
  jdsBiDropdownMaxHeight = 400;

  @Input()
  @jdsBiDefaultProp()
  jdsBiDropdownOffset = 4;

  open = false;

  onActiveZone(active: boolean): void {
    this.open = active && this.open;
  }

  onObscured(obscured: boolean): void {
    if (obscured) {
      this.open = false;
    }
  }

  onClick(): void {
    this.open = !this.open;
  }
}
