import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JdsBiDropdownHoverComponent } from './dropdown-hover.component';

const meta: Meta<JdsBiDropdownHoverComponent> = {
  title: 'Components/Dropdown/Hover',
  component: JdsBiDropdownHoverComponent,
  decorators: [
    moduleMetadata({
      imports: [BrowserModule, BrowserAnimationsModule],
    }),
  ],
  argTypes: {
    jdsBiDropdownAlign: {
      options: ['left', 'right'],
      control: { type: 'select' },
    },
    jdsBiDropdownDirection: {
      options: [null, 'bottom', 'top'],
      control: { type: 'select' },
    },
    jdsBiDropdownLimitWidth: {
      options: ['auto', 'fixed', 'min', 'custom'],
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiDropdownHoverComponent>;

const template = `
<button
  [jdsBiDropdownAlign]="jdsBiDropdownAlign"
  [jdsBiDropdownDirection]="jdsBiDropdownDirection"
  [jdsBiDropdownLimitWidth]="jdsBiDropdownLimitWidth"
  [jdsBiDropdownMinWidth]="jdsBiDropdownMinWidth"
  [jdsBiDropdownMinHeight]="jdsBiDropdownMinHeight"
  [jdsBiDropdownMaxHeight]="jdsBiDropdownMaxHeight"
  [jdsBiDropdownOffset]="jdsBiDropdownOffset"
  [jdsBiDropdown]="dropdownContent"
  jdsBiDropdownHover
  jdsBiButton
  type="button"
>
  Hover me
</button>

<ng-template #dropdownContent>
  <div class="p-4">
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
    terry richardson ad squid. 3 wolf moon officia aute, non cupidatat
    skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
    Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
    single-origin coffee nulla assumenda shoreditch et. Nihil anim
    keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
    sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
    occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
    you probably haven't heard of them accusamus labore sustainable VHS.
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
    terry richardson ad squid. 3 wolf moon officia aute, non cupidatat
    skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
    Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
    single-origin coffee nulla assumenda shoreditch et. Nihil anim
    keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
    sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
    occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
    you probably haven't heard of them accusamus labore sustainable VHS.
  </div>
</ng-template>
`;

export const Hover: Story = {
  args: {
    jdsBiDropdownAlign: 'left',
    jdsBiDropdownDirection: null,
    jdsBiDropdownLimitWidth: 'auto',
    jdsBiDropdownMinWidth: 160,
    jdsBiDropdownMinHeight: 80,
    jdsBiDropdownMaxHeight: 400,
    jdsBiDropdownOffset: 4,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
