import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import { JdsBiTableHeadModule } from '@jds-bi/core';
import { isEmpty } from 'lodash';

@Component({
  selector: 'jds-bi-table-head-sortable-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiTableHeadModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <table
      class="table-fixed w-full bg-white border-collapse border border-solid border-slate-400"
    >
      <thead class="bg-slate-50">
        <tr>
          <th
            class="border border-solid border-slate-300 text-left py-2 px-3"
            scope="col"
          >
            Normal
          </th>
          <th
            class="border border-solid border-slate-300 py-2 px-3"
            [colorScheme]="colorScheme"
            [isSortable]="isSortable"
            [currentSort]="{
              sort: filter.sort,
              direction: filter.directory
            }"
            (filterSort)="filterSort($event)"
            scope="col"
            jdsBiTableHead
            column="sortable"
          >
            Sortable
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="border border-solid border-slate-300 text-left py-2 px-3">
            Body 1
          </td>
          <td class="border border-solid border-slate-300 text-left py-2 px-3">
            Body 2
          </td>
        </tr>
      </tbody>
    </table>
  `,
})
export class JdsBiTableHeadSortableComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  isSortable = true;

  // Variable
  filter = {
    sort: 'sortable',
    directory: 'desc',
  };

  filterSort(sort: string): void {
    const value = isEmpty(sort) === false ? sort : '';

    let dir = '';
    if (this.filter.directory === 'desc') {
      dir = 'asc';
    } else if (this.filter.directory === 'asc') {
      dir = 'desc';
    }

    this.filter.sort = value;
    this.filter.directory = dir;
  }
}
