import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JdsBiTableHeadCustomComponent } from './table-head-custom.component';

const meta: Meta<JdsBiTableHeadCustomComponent> = {
  title: 'Components/Table Head/Custom',
  component: JdsBiTableHeadCustomComponent,
  decorators: [
    moduleMetadata({
      imports: [BrowserModule, BrowserAnimationsModule],
    }),
  ],
  argTypes: {
    colorScheme: { control: { type: 'color' } },
  },
};

export default meta;
type Story = StoryObj<JdsBiTableHeadCustomComponent>;

const template = `
<table
  class="table-fixed w-full bg-white border-collapse border border-solid border-slate-400"
>
  <thead class="bg-green-700">
    <tr>
      <th
        class="border border-solid border-slate-300 text-left text-white py-2 px-3"
        scope="col"
      >
        Normal
      </th>
      <th
        class="border border-solid border-slate-300 text-white py-2 px-3"
        [colorScheme]="colorScheme"
        [colorCustom]="colorCustom"
        [isSortable]="isSortable"
        [currentSort]="{
          sort: filter.sort,
          direction: filter.directory
        }"
        (filterSort)="filterSort($event)"
        scope="col"
        jdsBiTableHead
        column="sortable"
      >
        Sortable
      </th>
      <th
        class="border border-solid border-slate-300 text-white py-2 px-3"
        [colorScheme]="colorScheme"
        [colorCustom]="colorCustom"
        [isFilterable]="isFilterable"
        scope="col"
        jdsBiTableHead
        column="filterable"
      >
        <span>Filterable</span>

        <div
          [style.cursor]="'pointer'"
          filter
          role="button"
        >
          <jds-bi-hosted-dropdown
            [(open)]="open"
            [jdsBiDropdownLimitWidth]="'custom'"
            [jdsBiDropdownMinWidth]="240"
            [jdsBiDropdownAlign]="'right'"
            [content]="dropdownContent"
          >
            <i
              class="tblhd-icon"
              [ngClass]="['active']"
            ></i>
          </jds-bi-hosted-dropdown>
        </div>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td
        class="border border-solid border-slate-300 text-left py-2 px-3"
      >
        Body 1
      </td>
      <td
        class="border border-solid border-slate-300 text-left py-2 px-3"
      >
        Body 2
      </td>
      <td
        class="border border-solid border-slate-300 text-left py-2 px-3"
      >
        Body 3
      </td>
    </tr>
  </tbody>
</table>

<ng-template #dropdownContent>
  <div class="p-4">
    <div>Filter</div>
    <div>Checkbox</div>
    <div>Select</div>
  </div>
</ng-template>
`;

export const Template: Story = {
  args: {
    colorScheme: '#FFF',
    colorCustom: true,
    isSortable: true,
    isFilterable: true,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
