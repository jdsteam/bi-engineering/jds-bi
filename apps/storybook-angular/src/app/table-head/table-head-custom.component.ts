import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import {
  JdsBiRootModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiTableHeadModule,
} from '@jds-bi/core';
import { isEmpty } from 'lodash';

@Component({
  selector: 'jds-bi-table-head-custom-storybook',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiRootModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiTableHeadModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <jds-bi-root>
      <table
        class="table-fixed w-full bg-white border-collapse border border-solid border-slate-400"
      >
        <thead class="bg-green-700">
          <tr>
            <th
              class="border border-solid border-slate-300 text-left text-white py-2 px-3"
              scope="col"
            >
              Normal
            </th>
            <th
              class="border border-solid border-slate-300 text-white py-2 px-3"
              [colorScheme]="colorScheme"
              [colorCustom]="colorCustom"
              [isSortable]="isSortable"
              [currentSort]="{
                sort: filter.sort,
                direction: filter.directory
              }"
              (filterSort)="filterSort($event)"
              scope="col"
              jdsBiTableHead
              column="sortable"
            >
              Sortable
            </th>
            <th
              class="border border-solid border-slate-300 text-white py-2 px-3"
              [colorScheme]="colorScheme"
              [colorCustom]="colorCustom"
              [isFilterable]="isFilterable"
              scope="col"
              jdsBiTableHead
              column="filterable"
            >
              <span>Filterable</span>

              <div
                [style.cursor]="'pointer'"
                filter
                role="button"
              >
                <jds-bi-hosted-dropdown
                  [(open)]="open"
                  [jdsBiDropdownLimitWidth]="'custom'"
                  [jdsBiDropdownMinWidth]="240"
                  [jdsBiDropdownAlign]="'right'"
                  [content]="dropdownContent"
                >
                  <i
                    class="tblhd-icon"
                    [ngClass]="['active']"
                  ></i>
                </jds-bi-hosted-dropdown>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td
              class="border border-solid border-slate-300 text-left py-2 px-3"
            >
              Body 1
            </td>
            <td
              class="border border-solid border-slate-300 text-left py-2 px-3"
            >
              Body 2
            </td>
            <td
              class="border border-solid border-slate-300 text-left py-2 px-3"
            >
              Body 3
            </td>
          </tr>
        </tbody>
      </table>

      <ng-template #dropdownContent>
        <div class="p-4">
          <div>Filter</div>
          <div>Checkbox</div>
          <div>Select</div>
        </div>
      </ng-template>
    </jds-bi-root>
  `,
})
export class JdsBiTableHeadCustomComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme = 'green';

  @Input()
  @jdsBiDefaultProp()
  colorCustom = true;

  @Input()
  @jdsBiDefaultProp()
  isSortable = false;

  @Input()
  @jdsBiDefaultProp()
  isFilterable = false;

  // Variable
  open = false;
  filter = {
    sort: 'sortable',
    directory: 'desc',
  };

  filterSort(sort: string): void {
    const value = isEmpty(sort) === false ? sort : '';

    let dir = '';
    if (this.filter.directory === 'desc') {
      dir = 'asc';
    } else if (this.filter.directory === 'asc') {
      dir = 'desc';
    }

    this.filter.sort = value;
    this.filter.directory = dir;
  }
}
