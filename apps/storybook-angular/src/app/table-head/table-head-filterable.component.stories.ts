import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JdsBiTableHeadFilterableComponent } from './table-head-filterable.component';

const meta: Meta<JdsBiTableHeadFilterableComponent> = {
  title: 'Components/Table Head/Filterable',
  component: JdsBiTableHeadFilterableComponent,
  decorators: [
    moduleMetadata({
      imports: [BrowserModule, BrowserAnimationsModule],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiTableHeadFilterableComponent>;

const template = `
<table
  class="table-fixed w-full bg-white border-collapse border border-solid border-slate-400"
>
  <thead class="bg-slate-50">
    <tr>
      <th
        class="border border-solid border-slate-300 text-left py-2 px-3"
        scope="col"
      >
        Normal
      </th>
      <th
        class="border border-solid border-slate-300 py-2 px-3"
        [colorScheme]="colorScheme"
        [isFilterable]="isFilterable"
        scope="col"
        jdsBiTableHead
        column="filterable"
      >
        <span>Filterable</span>

        <div
          [style.cursor]="'pointer'"
          filter
          role="button"
        >
          <jds-bi-hosted-dropdown
            [(open)]="open"
            [jdsBiDropdownLimitWidth]="'custom'"
            [jdsBiDropdownMinWidth]="240"
            [jdsBiDropdownAlign]="'right'"
            [content]="dropdownContent"
          >
            <i
              class="tblhd-icon"
              [ngClass]="['active']"
            ></i>
          </jds-bi-hosted-dropdown>
        </div>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td
        class="border border-solid border-slate-300 text-left py-2 px-3"
      >
        Body 1
      </td>
      <td
        class="border border-solid border-slate-300 text-left py-2 px-3"
      >
        Body 2
      </td>
    </tr>
  </tbody>
</table>

<ng-template #dropdownContent>
  <div class="p-4">
    <div>Filter</div>
    <div>Checkbox</div>
    <div>Select</div>
  </div>
</ng-template>
`;

export const Filterable: Story = {
  args: {
    colorScheme: 'green',
    isFilterable: true,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
