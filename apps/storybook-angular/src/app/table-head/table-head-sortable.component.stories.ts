import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiTableHeadSortableComponent } from './table-head-sortable.component';

const meta: Meta<JdsBiTableHeadSortableComponent> = {
  title: 'Components/Table Head/Sortable',
  component: JdsBiTableHeadSortableComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiTableHeadSortableComponent>;

const template = `
<table
  class="table-fixed w-full bg-white border-collapse border border-solid border-slate-400"
>
  <thead class="bg-slate-50">
    <tr>
      <th
        class="border border-solid border-slate-300 text-left py-2 px-3"
        scope="col"
      >
        Normal
      </th>
      <th
        class="border border-solid border-slate-300 py-2 px-3"
        [colorScheme]="colorScheme"
        [isSortable]="isSortable"
        [currentSort]="{
          sort: filter.sort,
          direction: filter.directory
        }"
        (filterSort)="filterSort($event)"
        scope="col"
        jdsBiTableHead
        column="sortable"
      >
        Sortable
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="border border-solid border-slate-300 text-left py-2 px-3">
        Body 1
      </td>
      <td class="border border-solid border-slate-300 text-left py-2 px-3">
        Body 2
      </td>
    </tr>
  </tbody>
</table>
`;

export const Sortable: Story = {
  args: {
    colorScheme: 'green',
    isSortable: true,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
