import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import {
  JdsBiRootModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiTableHeadModule,
} from '@jds-bi/core';

@Component({
  selector: 'jds-bi-table-head-filterable-storybook',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiRootModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiTableHeadModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <jds-bi-root>
      <table
        class="table-fixed w-full bg-white border-collapse border border-solid border-slate-400"
      >
        <thead class="bg-slate-50">
          <tr>
            <th
              class="border border-solid border-slate-300 text-left py-2 px-3"
              scope="col"
            >
              Normal
            </th>
            <th
              class="border border-solid border-slate-300 py-2 px-3"
              [colorScheme]="colorScheme"
              [isFilterable]="isFilterable"
              scope="col"
              jdsBiTableHead
              column="filterable"
            >
              <span>Filterable</span>

              <div
                [style.cursor]="'pointer'"
                filter
                role="button"
              >
                <jds-bi-hosted-dropdown
                  [(open)]="open"
                  [jdsBiDropdownLimitWidth]="'custom'"
                  [jdsBiDropdownMinWidth]="240"
                  [jdsBiDropdownAlign]="'right'"
                  [content]="dropdownContent"
                >
                  <i
                    class="tblhd-icon"
                    [ngClass]="['active']"
                  ></i>
                </jds-bi-hosted-dropdown>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td
              class="border border-solid border-slate-300 text-left py-2 px-3"
            >
              Body 1
            </td>
            <td
              class="border border-solid border-slate-300 text-left py-2 px-3"
            >
              Body 2
            </td>
          </tr>
        </tbody>
      </table>

      <ng-template #dropdownContent>
        <div class="p-4">
          <div>Filter</div>
          <div>Checkbox</div>
          <div>Select</div>
        </div>
      </ng-template>
    </jds-bi-root>
  `,
})
export class JdsBiTableHeadFilterableComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  isFilterable = false;

  open = false;
}
