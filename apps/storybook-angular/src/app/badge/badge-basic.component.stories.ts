import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiBadgeVariant } from '@jds-bi/core';
import { JdsBiBadgeBasicComponent } from './badge-basic.component';

const meta: Meta<JdsBiBadgeBasicComponent> = {
  title: 'Components/Badge/Basic',
  component: JdsBiBadgeBasicComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
    variant: {
      options: JdsBiBadgeVariant,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiBadgeBasicComponent>;

const template = `
<span
  [colorScheme]="colorScheme"
  [variant]="variant"
  [isRounded]="isRounded"
  jdsBiBadge
>
  Publik
</span>
`;

export const Basic: Story = {
  args: {
    colorScheme: 'green',
    variant: 'solid',
    isRounded: false,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
