import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type { JdsBiBadgeVariant } from '@jds-bi/core';
import { JdsBiBadgeModule } from '@jds-bi/core';

@Component({
  selector: 'jds-bi-badge-basic-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiBadgeModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <span
      class="font-body"
      [colorScheme]="colorScheme"
      [variant]="variant"
      [isRounded]="isRounded"
      jdsBiBadge
    >
      Publik
    </span>
  `,
})
export class JdsBiBadgeBasicComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiBadgeVariant = 'solid';

  @Input()
  @jdsBiDefaultProp()
  isRounded = false;
}
