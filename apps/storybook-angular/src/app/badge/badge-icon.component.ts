import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type { JdsBiBadgeVariant } from '@jds-bi/core';
import { JdsBiBadgeModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCircleCheck,
} from '@jds-bi/icons';

@Component({
  selector: 'jds-bi-badge-icon-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiBadgeModule, JdsBiIconsModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <span
      class="font-body"
      [colorScheme]="colorScheme"
      [variant]="variant"
      [isRounded]="isRounded"
      jdsBiBadge
    >
      <jds-bi-icon
        class="mr-1"
        name="circle-check"
      />
      <span>Publik</span>
    </span>
  `,
})
export class JdsBiBadgeIconComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiBadgeVariant = 'solid';

  @Input()
  @jdsBiDefaultProp()
  isRounded = false;

  constructor(private jdsBiIconsService: JdsBiIconsService) {
    this.jdsBiIconsService.registerIcons([iconCircleCheck]);
  }
}
