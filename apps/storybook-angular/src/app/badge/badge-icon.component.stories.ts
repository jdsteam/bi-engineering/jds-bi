import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiBadgeVariant } from '@jds-bi/core';
import { JdsBiBadgeIconComponent } from './badge-icon.component';

const meta: Meta<JdsBiBadgeIconComponent> = {
  title: 'Components/Badge/Icon',
  component: JdsBiBadgeIconComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
    variant: {
      options: JdsBiBadgeVariant,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiBadgeIconComponent>;

const template = `
<span
  [colorScheme]="colorScheme"
  [variant]="variant"
  [isRounded]="isRounded"
  jdsBiBadge
>
  <jds-bi-icon class="mr-1" name="circle-check" />
  <span>Publik</span>
</span>
`;

export const Icon: Story = {
  args: {
    colorScheme: 'green',
    variant: 'solid',
    isRounded: false,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
