import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type { JdsBiPaginationItems } from '@jds-bi/core';
import { JdsBiPaginationModule } from '@jds-bi/core';

@Component({
  selector: 'jds-bi-pagination-basic-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiPaginationModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <jds-bi-pagination
      [colorScheme]="colorScheme"
      [perPageItems]="perPageItems"
      [perPage]="perPage"
      [page]="page"
      [pagination]="{ empty: false, total_pages: 100, total_items: 1000 }"
    />
  `,
})
export class JdsBiPaginationBasicComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  perPageItems: JdsBiPaginationItems[] = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];

  @Input()
  @jdsBiDefaultProp()
  pageItems: JdsBiPaginationItems[] = [];

  @Input()
  @jdsBiDefaultProp()
  perPage = 10;

  @Input()
  @jdsBiDefaultProp()
  page = 1;
}
