import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiPaginationBasicComponent } from './pagination-basic.component';

const meta: Meta<JdsBiPaginationBasicComponent> = {
  title: 'Components/Pagination/Basic',
  component: JdsBiPaginationBasicComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiPaginationBasicComponent>;

const template = `
<jds-bi-pagination
  [colorScheme]="colorScheme"
  [perPageItems]="perPageItems"
  [perPage]="perPage"
  [page]="page"
  [pagination]="{ empty: false, total_pages: 100, total_items: 1000 }"
/>
`;

export const Basic: Story = {
  args: {
    colorScheme: 'green',
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
