import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { jdsBiDefaultProp } from '@jds-bi/cdk';
import { JdsBiIconsName } from '@jds-bi/icons';

import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconArrowRight,
  iconArrowUp,
  iconArrowUpRightFromSquare,
  iconBan,
  iconBars,
  iconBell,
  iconBook,
  iconBoxArchive,
  iconBuilding,
  iconBuildingColumns,
  iconBriefcase,
  iconCalendar,
  iconCabinetFiling,
  iconChartSimple,
  iconCheck,
  iconCheckDouble,
  iconChevronDown,
  iconChevronLeft,
  iconChevronRight,
  iconCircleCheck,
  iconCircleDown,
  iconCircleInfo,
  iconCirclePlus,
  iconCircleQuestion,
  iconCircleXmark,
  iconClockRotateLeft,
  iconClockThree,
  iconCloudArrowDown,
  iconComments,
  iconDatabase,
  iconDownload,
  iconEllipsis,
  iconEnvelope,
  iconEye,
  iconEyeSlash,
  iconFileTimer,
  iconGlobe,
  iconLocationDot,
  iconMagnifyingGlass,
  iconMapLocation,
  iconPencil,
  iconPhone,
  iconPlus,
  iconStar,
  iconTrash,
  iconUserLock,
  iconWhatsapp,
  iconXmark,
} from '@jds-bi/icons';

@Component({
  selector: 'jds-bi-icon-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiIconsModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="grid grid-cols-5 gap-4">
      <div
        class="border border-solid rounded p-4 text-center"
        *ngFor="let item of allIcons"
      >
        <jds-bi-icon
          [size]="size"
          [color]="color"
          [name]="item"
        />
        <div class="mt-3 text-sm">{{ item }}</div>
      </div>
    </div>
  `,
})
export class JdsBiIconComponent {
  @Input()
  @jdsBiDefaultProp()
  size = 14;

  @Input()
  @jdsBiDefaultProp()
  color = 'currentColor';

  // Variable
  allIcons = JdsBiIconsName;

  constructor(private jdsBiIconsService: JdsBiIconsService) {
    this.jdsBiIconsService.registerIcons([
      iconArrowRight,
      iconArrowUp,
      iconArrowUpRightFromSquare,
      iconBan,
      iconBars,
      iconBell,
      iconBook,
      iconBoxArchive,
      iconBuilding,
      iconBuildingColumns,
      iconBriefcase,
      iconCalendar,
      iconCabinetFiling,
      iconChartSimple,
      iconCheck,
      iconCheckDouble,
      iconChevronDown,
      iconChevronLeft,
      iconChevronRight,
      iconCircleCheck,
      iconCircleDown,
      iconCircleInfo,
      iconCirclePlus,
      iconCircleQuestion,
      iconCircleXmark,
      iconClockRotateLeft,
      iconClockThree,
      iconCloudArrowDown,
      iconComments,
      iconDatabase,
      iconDownload,
      iconEllipsis,
      iconEnvelope,
      iconEye,
      iconEyeSlash,
      iconFileTimer,
      iconGlobe,
      iconLocationDot,
      iconMagnifyingGlass,
      iconMapLocation,
      iconPencil,
      iconPhone,
      iconPlus,
      iconStar,
      iconTrash,
      iconUserLock,
      iconWhatsapp,
      iconXmark,
    ]);
  }
}
