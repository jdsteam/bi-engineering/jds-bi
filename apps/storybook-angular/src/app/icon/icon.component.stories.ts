import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiIconComponent } from './icon.component';

const meta: Meta<JdsBiIconComponent> = {
  title: 'Components/Icon/Basic',
  component: JdsBiIconComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    color: { control: { type: 'color' } },
  },
};

export default meta;
type Story = StoryObj<JdsBiIconComponent>;

export const Basic: Story = {
  args: {
    size: 14,
    color: 'currentColor',
  },
};
