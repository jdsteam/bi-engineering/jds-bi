import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';

import { JdsBiAccordionBasicComponent } from './accordion-basic.component';

const meta: Meta<JdsBiAccordionBasicComponent> = {
  title: 'Components/Accordion/Basic',
  component: JdsBiAccordionBasicComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiAccordionBasicComponent>;

const template = `
<jds-bi-accordion
  #acc="jdsBiAccordion"
  [destroyOnHide]="destroyOnHide"
  [closeOthers]="closeOtherPanels"
  [activeIds]="activeIds"
  [colorScheme]="colorScheme"
>
  <jds-bi-panel
    id="panel-1"
    title="Basic 1"
  >
    <ng-template jdsBiPanelContent>
      Content Panel 1
    </ng-template>
  </jds-bi-panel>
  <jds-bi-panel
    id="panel-2"
    title="Basic 2"
  >
    <ng-template jdsBiPanelContent>
      Content Panel 2
    </ng-template>
  </jds-bi-panel>
  <jds-bi-panel
    id="panel-3"
    [disabled]="true"
    title="Disabled"
  >
    <ng-template jdsBiPanelContent>
      Disabled 
    </ng-template>
  </jds-bi-panel>
</jds-bi-accordion>
`;

export const Basic: Story = {
  args: {
    colorScheme: 'green',
    activeIds: ['panel-1'],
    closeOtherPanels: false,
    destroyOnHide: true,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
