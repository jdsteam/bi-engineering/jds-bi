import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';

import { JdsBiAccordionHeaderComponent } from './accordion-header.component';

const meta: Meta<JdsBiAccordionHeaderComponent> = {
  title: 'Components/Accordion/Custom Header',
  component: JdsBiAccordionHeaderComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiAccordionHeaderComponent>;

const template = `
<jds-bi-accordion
  #acc="jdsBiAccordion"
  [destroyOnHide]="destroyOnHide"
  [closeOthers]="closeOtherPanels"
  [activeIds]="activeIds"
  [colorScheme]="colorScheme"
>
  <jds-bi-panel id="panel-1">
    <ng-template jdsBiPanelTitle>
      <span>&#9733; <b>Custom</b> title &#9733;</span>
    </ng-template>
    <ng-template jdsBiPanelContent>
      Content Panel 1
    </ng-template>
  </jds-bi-panel>
  <jds-bi-panel id="panel-2">
    <ng-template
      jdsBiPanelHeader
      let-opened="opened"
    >
      <div
        class="t-accordion-button justify-between"
        [class.t-collapsed]="!opened"
      >
        <p class="m-0">
          Second Panel - {{ opened ? 'opened' : 'collapsed' }}
        </p>
        <button jdsBiPanelToggle>Custom Toggle</button>
      </div>
    </ng-template>
    <ng-template jdsBiPanelContent>
      Content Panel 2
    </ng-template>
  </jds-bi-panel>
</jds-bi-accordion>
`;

export const CustomHeader: Story = {
  args: {
    colorScheme: 'green',
    activeIds: ['panel-1'],
    closeOtherPanels: false,
    destroyOnHide: true,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
