import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';
import { JdsBiAccordionModule } from '@jds-bi/core';

@Component({
  selector: 'jds-bi-accordion-basic-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiAccordionModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <jds-bi-accordion
      #acc="jdsBiAccordion"
      [destroyOnHide]="destroyOnHide"
      [closeOthers]="closeOtherPanels"
      [activeIds]="activeIds"
      [colorScheme]="colorScheme"
    >
      <jds-bi-panel
        id="panel-1"
        title="Basic 1"
      >
        <ng-template jdsBiPanelContent>
          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
          terry richardson ad squid. 3 wolf moon officia aute, non cupidatat
          skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
          Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
          single-origin coffee nulla assumenda shoreditch et. Nihil anim
          keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
          sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
          you probably haven't heard of them accusamus labore sustainable VHS.
        </ng-template>
      </jds-bi-panel>
      <jds-bi-panel
        id="panel-2"
        title="Basic 2"
      >
        <ng-template jdsBiPanelContent>
          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
          terry richardson ad squid. 3 wolf moon officia aute, non cupidatat
          skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
          Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
          single-origin coffee nulla assumenda shoreditch et. Nihil anim
          keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
          sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
          you probably haven't heard of them accusamus labore sustainable VHS.
        </ng-template>
      </jds-bi-panel>
      <jds-bi-panel
        id="panel-2"
        [disabled]="true"
        title="Disabled"
      >
        <ng-template jdsBiPanelContent> Disabled </ng-template>
      </jds-bi-panel>
    </jds-bi-accordion>
  `,
})
export class JdsBiAccordionBasicComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  activeIds: string | readonly string[] = ['panel-1'];

  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('closeOthers')
  @jdsBiDefaultProp()
  closeOtherPanels = false;

  @Input()
  @jdsBiDefaultProp()
  destroyOnHide = true;
}
