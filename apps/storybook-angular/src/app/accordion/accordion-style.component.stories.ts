import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';

import { JdsBiAccordionStyleComponent } from './accordion-style.component';

const meta: Meta<JdsBiAccordionStyleComponent> = {
  title: 'Components/Accordion/Custom Style',
  component: JdsBiAccordionStyleComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiAccordionStyleComponent>;

const template = `
<jds-bi-accordion
  class="accordion-storybook"
  #acc="jdsBiAccordion"
  [destroyOnHide]="destroyOnHide"
  [closeOthers]="closeOtherPanels"
  [activeIds]="activeIds"
  [colorScheme]="colorScheme"
>
  <jds-bi-panel id="panel-1">
    <ng-template jdsBiPanelTitle> Padding Right 100px </ng-template>
    <ng-template jdsBiPanelContent>
      Content Panel 1
    </ng-template>
  </jds-bi-panel>
  <jds-bi-panel id="panel-2">
    <ng-template
      jdsBiPanelHeader
      let-opened="opened"
    >
      <div
        class="t-accordion-button justify-between"
        [class.t-collapsed]="!opened"
      >
        <p class="m-0">
          Second Panel - {{ opened ? 'opened' : 'collapsed' }}
        </p>
        <button jdsBiPanelToggle>Custom Toggle</button>
      </div>
    </ng-template>
    <ng-template jdsBiPanelContent>
      Content Panel 2
    </ng-template>
  </jds-bi-panel>
  <jds-bi-panel id="panel-3">
    <ng-template jdsBiPanelTitle> Border Red </ng-template>
    <ng-template jdsBiPanelContent>
      Content Panel 3
    </ng-template>
  </jds-bi-panel>
</jds-bi-accordion>
`;

export const CustomStyle: Story = {
  args: {
    colorScheme: 'red',
    activeIds: ['panel-1'],
    closeOtherPanels: false,
    destroyOnHide: true,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
