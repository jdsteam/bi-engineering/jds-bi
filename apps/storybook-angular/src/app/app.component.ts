import { NxWelcomeComponent } from './nx-welcome.component';
import { Component } from '@angular/core';

@Component({
  standalone: true,
  imports: [NxWelcomeComponent],
  selector: 'jds-bi-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'storybook';
}
