import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type { JdsBiButtonVariant, JdsBiButtonSize } from '@jds-bi/core';
import { JdsBiButtonModule } from '@jds-bi/core';

@Component({
  selector: 'jds-bi-button-basic-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiButtonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="flex gap-4">
      <button
        [colorScheme]="colorScheme"
        [variant]="variant"
        [size]="size"
        [isLoading]="isLoading"
        [disabled]="disabled"
        jdsBiButton
      >
        Simpan
      </button>
      <a
        [colorScheme]="colorScheme"
        [variant]="variant"
        [size]="size"
        [isLoading]="isLoading"
        jdsBiButton
        href="https://ekosistemdatajabar.id/"
        target="_blank"
      >
        Link
      </a>
    </div>
  `,
})
export class JdsBiButtonBasicComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiButtonVariant = 'solid';

  @Input()
  @jdsBiDefaultProp()
  size: JdsBiButtonSize = 'md';

  @Input()
  @jdsBiDefaultProp()
  isLoading = false;

  @Input()
  @jdsBiDefaultProp()
  disabled = false;
}
