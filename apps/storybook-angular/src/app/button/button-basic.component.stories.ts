import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiButtonVariant, JdsBiButtonSize } from '@jds-bi/core';
import { JdsBiButtonBasicComponent } from './button-basic.component';

const meta: Meta<JdsBiButtonBasicComponent> = {
  title: 'Components/Button/Basic',
  component: JdsBiButtonBasicComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
    variant: {
      options: JdsBiButtonVariant,
      control: { type: 'select' },
    },
    size: {
      options: JdsBiButtonSize,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiButtonBasicComponent>;

const template = `
<button
  [colorScheme]="colorScheme"
  [variant]="variant"
  [size]="size"
  [isLoading]="isLoading"
  [disabled]="disabled"
  jdsBiButton
>
  Simpan
</button>
<a
  [colorScheme]="colorScheme"
  [variant]="variant"
  [size]="size"
  [isLoading]="isLoading"
  jdsBiButton
  href="https://ekosistemdatajabar.id/"
  target="_blank"
>
  Link
</a>
`;

export const Basic: Story = {
  args: {
    colorScheme: 'green',
    variant: 'solid',
    size: 'md',
    isLoading: false,
    disabled: false,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
