import type { StoryObj, Meta } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { JdsBiColor } from '@jds-bi/cdk';
import { JdsBiButtonVariant, JdsBiButtonSize } from '@jds-bi/core';
import { JdsBiButtonIconComponent } from './button-icon.component';

const meta: Meta<JdsBiButtonIconComponent> = {
  title: 'Components/Button/Icon Only',
  component: JdsBiButtonIconComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    colorScheme: {
      options: JdsBiColor,
      control: { type: 'select' },
    },
    variant: {
      options: JdsBiButtonVariant,
      control: { type: 'select' },
    },
    size: {
      options: JdsBiButtonSize,
      control: { type: 'select' },
    },
  },
};

export default meta;
type Story = StoryObj<JdsBiButtonIconComponent>;

const template = `
<button
  [colorScheme]="colorScheme"
  [variant]="variant"
  [size]="size"
  [isLoading]="isLoading"
  [isIconOnly]="isIconOnly"
  [disabled]="disabled"
  jdsBiButton
>
  <jds-bi-icon name="download" />
</button>
`;

export const IconOnly: Story = {
  args: {
    colorScheme: 'green',
    variant: 'solid',
    size: 'md',
    isLoading: false,
    disabled: false,
  },
  parameters: {
    docs: {
      source: {
        code: template,
      },
    },
  },
};
