import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type { JdsBiButtonVariant, JdsBiButtonSize } from '@jds-bi/core';
import { JdsBiButtonModule } from '@jds-bi/core';

import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconDownload,
} from '@jds-bi/icons';

@Component({
  selector: 'jds-bi-button-icon-storybook',
  standalone: true,
  imports: [CommonModule, JdsBiButtonModule, JdsBiIconsModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
      <button
        [colorScheme]="colorScheme"
        [variant]="variant"
        [size]="size"
        [isLoading]="isLoading"
        [isIconOnly]="isIconOnly"
        [disabled]="disabled"
        jdsBiButton
      >
        <jds-bi-icon name="download" />
      </button>
  `,
})
export class JdsBiButtonIconComponent {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiButtonVariant = 'solid';

  @Input()
  @jdsBiDefaultProp()
  size: JdsBiButtonSize = 'md';

  @Input()
  @jdsBiDefaultProp()
  isLoading = false;

  @Input()
  @jdsBiDefaultProp()
  isIconOnly = true;

  @Input()
  @jdsBiDefaultProp()
  disabled = true;

  constructor(private jdsBiIconsService: JdsBiIconsService) {
    this.jdsBiIconsService.registerIcons([iconDownload]);
  }
}
