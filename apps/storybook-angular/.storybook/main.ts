import type { StorybookConfig } from '@storybook/angular';
const config: StorybookConfig = {
  // ...rootMain,
  core: {
    disableTelemetry: true,
  },
  stories: [
    // ...rootMain.stories,
    '../src/app/**/*.stories.mdx',
    '../src/app/**/*.stories.@(js|jsx|ts|tsx)',
  ],
  addons: [
    '@storybook/addon-essentials',
    // ...(rootMain.addons || [])
  ],
  framework: {
    name: '@storybook/angular',
    options: {
      enableIvy: true,
    },
  },
  webpackFinal: async (config) => {
    // apply any global webpack configs that might have been specified in .storybook/main.ts
    // if (rootMain?.webpackFinal) {
    //   config = await rootMain.webpackFinal(config, {
    //     configType,
    //   } as any);
    // }

    // add your own webpack tweaks if needed

    return config;
  },
  docs: {
    autodocs: true,
  },
  // features: {
  //   storyStoreV7: false,
  // },
};
module.exports = config;
