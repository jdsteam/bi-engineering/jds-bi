import { CommonModule } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { ScrollingModule } from '@angular/cdk/scrolling';
// import { FormsModule } from '@angular/forms';

// import { CdkAccordionModule } from '@angular/cdk/accordion';
// import { testla } from '@jds-bi/core';
import {
  JdsBiRootModule,
  JdsBiScrollbarModule,
  JdsBiAccordionModule,
  JdsBiButtonModule,
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiInputModule,
  JdsBiModalModule,
  JdsBiNavModule,
  JdsBiPaginationModule,
  JdsBiTableHeadModule,
} from '@jds-bi/core';

import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconBook,
  iconUserLock,
} from '@jds-bi/icons';

// import {
//   // AngularComponent,
//   // AccordionModule,
//   // BadgeModule,
//   // ButtonModule,
//   // TableHeadComponent,
//   // PaginationComponent,
//   // IconComponent,
//   // InputModule,
//   // NavModule,
//   // IconService,
//   // iconBook,
//   // iconBuilding,
//   // iconBriefcase,
//   // iconCalendar,
//   // iconCabinetFiling,
//   // iconCircleCheck,
//   // iconCircleDown,
//   // iconClockRotateLeft,
//   // iconClockThree,
//   // iconEnvelope,
//   // iconEye,
//   // iconFileTimer,
//   // iconPencil,
//   // iconPhone,
//   // iconStar,
//   // iconTrash,
//   // iconUserLock,
// } from '@jds-bi/angular';
// import { NgSelectModule } from '@ng-select/ng-select';

import { TestModalComponent } from './components/modal/test-modal/test-modal.component';
import { JdsBiNavBasicComponent } from './components/nav/nav-basic.component';

@Component({
  standalone: true,
  imports: [
    CommonModule,
    // FormsModule,
    // CdkAccordionModule,
    // AngularComponent,
    ScrollingModule,
    JdsBiRootModule,
    JdsBiScrollbarModule,
    JdsBiAccordionModule,
    JdsBiButtonModule,
    JdsBiBadgeModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiIconsModule,
    JdsBiInputModule,
    JdsBiModalModule,
    JdsBiNavModule,
    JdsBiPaginationModule,
    JdsBiTableHeadModule,
    // AccordionModule,
    // BadgeModule,
    // ButtonModule,
    // TableHeadComponent,
    // PaginationComponent,
    // IconComponent,
    // InputModule,
    // NavModule,
    // NgSelectModule,
    TestModalComponent,
    JdsBiNavBasicComponent,
  ],
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'app-root',
  templateUrl: './app-v2.component.html',
})
export class AppComponent {
  title = 'demo-angular';

  @ViewChild(TestModalComponent)
  testModalComponent!: TestModalComponent;

  dropdownOpen = false;
  dropdownNotificationOpen = false;

  // selectedCar!: number;
  // cars = [
  //   { id: 1, name: 'Volvo' },
  //   { id: 2, name: 'Saab' },
  //   { id: 3, name: 'Opel' },
  //   { id: 4, name: 'Audi' },
  // ];

  // perPageItems = [
  //   { value: 10, label: '10' },
  //   { value: 25, label: '25' },
  // ];
  // perPage = 10;
  // currentPage = 1;
  // pagination = {
  //   empty: false,
  //   infinite: false,
  //   total_items: 5500,
  //   current_page: 1,
  //   previous_page: 0,
  //   next_page: 2,
  //   page_size: 10,
  //   total_pages: 156,
  //   start_page: 1,
  //   end_page: 6,
  //   start_index: 0,
  //   end_index: 5,
  //   pages: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  // };

  activeNav = 1;
  // activeIds = ['panel-1', 'panel-2'];

  // buttonScheme = 'green';
  // buttonloading = false;
  // inputInvalid = false;

  open = false;

  constructor(private iconService: JdsBiIconsService) {
    this.iconService.registerIcons([iconBook, iconUserLock]);

    // console.log(testla());
  }

  filterSort(sort: string): void {
    console.warn(sort);
  }

  onClick(): void {
    this.open = !this.open;
  }

  onObscured(obscured: boolean): void {
    if (obscured) {
      this.open = false;
    }
  }

  onActiveZone(active: boolean): void {
    this.open = active && this.open;
  }

  onTestModal() {
    this.testModalComponent.openModal();
  }

  // filterPerPage(perPage: number): void {
  //   this.perPage = perPage;
  // }

  // filterPage(page: number): void {
  //   this.currentPage = page;
  // }

  // changeInvalid() {
  //   return (this.inputInvalid = !this.inputInvalid);
  // }
}
