import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JdsBiModalModule, JdsBiModalService } from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

@Component({
  selector: 'jds-bi-modal-test',
  standalone: true,
  imports: [CommonModule, JdsBiModalModule, JdsBiIconsModule],
  templateUrl: './test-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestModalComponent {
  constructor(
    private jdsBiModalService: JdsBiModalService,
    private JdsBiIconService: JdsBiIconsService,
  ) {
    this.JdsBiIconService.registerIcons([iconXmark]);
  }

  openModal() {
    this.jdsBiModalService.open('modal-test');
  }

  closeModal() {
    this.jdsBiModalService.close('modal-test');
  }
}
