import { TransitionStartFn } from './transition';

export interface CollapseCtx {
  direction: 'show' | 'hide';
  dimension: 'width' | 'height';
  maxSize?: string;
}

function reflow(element: HTMLElement) {
  return (element || document.body).getBoundingClientRect();
}

function measureCollapsingElementDimensionPx(
  element: HTMLElement,
  dimension: 'width' | 'height',
): string {
  // SSR fix for without injecting the PlatformId
  if (typeof navigator === 'undefined') {
    return '0px';
  }

  const { classList } = element;
  const hasShownClass = classList.contains('show');
  if (!hasShownClass) {
    classList.add('t-show');
  }

  element.style[dimension] = '';
  const dimensionSize = `${element.getBoundingClientRect()[dimension]}px`;

  if (!hasShownClass) {
    classList.remove('t-show');
  }

  return dimensionSize;
}

export const CollapsingTransition: TransitionStartFn<CollapseCtx> = (
  element: HTMLElement,
  animation: boolean,
  context: CollapseCtx,
) => {
  let { maxSize } = context;
  const { direction, dimension } = context;
  const { classList } = element;

  function setInitialClasses() {
    classList.add('t-collapse');
    if (direction === 'show') {
      classList.add('t-show');
    } else {
      classList.remove('t-show');
    }
  }

  // without animations we just need to set initial classes
  if (!animation) {
    setInitialClasses();
    return;
  }

  // No maxHeight -> running the transition for the first time
  if (!maxSize) {
    maxSize = measureCollapsingElementDimensionPx(element, dimension);
    context.maxSize = maxSize;

    // Fix the height before starting the animation
    element.style[dimension] = direction !== 'show' ? maxSize : '0px';

    classList.remove('t-collapse');
    classList.remove('t-collapsing');
    classList.remove('t-show');

    reflow(element);

    // Start the animation
    classList.add('t-collapsing');
  }

  // Start or revert the animation
  element.style[dimension] = direction === 'show' ? maxSize : '0px';

  // eslint-disable-next-line consistent-return
  return () => {
    setInitialClasses();
    classList.remove('t-collapsing');
    element.style[dimension] = '';
  };
};
