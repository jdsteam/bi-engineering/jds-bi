import { JdsBiMedia } from '@jds-bi/core/interfaces';
import { jdsBiGetViewportWidth } from '@jds-bi/core/utils/dom';

export function jdsBiIsMobile(
  windowRef: Window,
  { mobile }: JdsBiMedia,
): boolean {
  return jdsBiGetViewportWidth(windowRef) < mobile;
}
