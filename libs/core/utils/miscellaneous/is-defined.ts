export function jdsBiIsDefined(value: any): boolean {
  return value !== undefined && value !== null;
}
