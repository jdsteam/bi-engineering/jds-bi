import { jdsBiEditingKeys } from '@jds-bi/core/constants';

/**
 * Check if pressed key is interactive in terms of input field
 */
export function jdsBiIsEditingKey(key: string): boolean {
  return key.length === 1 || jdsBiEditingKeys.includes(key);
}
