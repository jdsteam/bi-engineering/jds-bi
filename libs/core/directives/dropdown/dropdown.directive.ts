import {
  AfterViewChecked,
  ComponentRef,
  Directive,
  ElementRef,
  Inject,
  INJECTOR,
  Input,
  OnChanges,
  OnDestroy,
} from '@angular/core';
import {
  JdsBiActiveZoneDirective,
  JdsBiContextWithImplicit,
  jdsBiDefaultProp,
  JdsBiDropdownPortalService,
  jdsBiPure,
} from '@jds-bi/cdk';
import {
  jdsBiAsRectAccessor,
  jdsBiAsVehicle,
  JdsBiRectAccessor,
  JdsBiVehicle,
} from '@jds-bi/core/abstract';
import { JdsBiPortalItem } from '@jds-bi/core/interfaces';
import { jdsBiCheckFixedPosition } from '@jds-bi/core/utils';
import {
  PolymorpheusComponent,
  PolymorpheusContent,
} from '@tinkoff/ng-polymorpheus';

// TODO: find the best way for prevent cycle
import { JDS_BI_DROPDOWN_COMPONENT } from './dropdown.providers';

@Directive({
  selector: '[jdsBiDropdown]:not(ng-container)',
  exportAs: 'jdsBiDropdown',
  providers: [
    jdsBiAsRectAccessor(JdsBiDropdownDirective),
    jdsBiAsVehicle(JdsBiDropdownDirective),
    {
      provide: PolymorpheusComponent,
      deps: [JDS_BI_DROPDOWN_COMPONENT, INJECTOR],
      useClass: PolymorpheusComponent,
    },
  ],
})
export class JdsBiDropdownDirective
  implements
    AfterViewChecked,
    OnDestroy,
    OnChanges,
    JdsBiPortalItem,
    JdsBiRectAccessor,
    JdsBiVehicle
{
  @Input('jdsBiDropdown')
  @jdsBiDefaultProp()
  content: PolymorpheusContent<
    JdsBiContextWithImplicit<JdsBiActiveZoneDirective>
  > = '';

  dropdownBoxRef: ComponentRef<unknown> | null = null;

  constructor(
    @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>,
    @Inject(PolymorpheusComponent)
    readonly component: PolymorpheusComponent<unknown, any>,
    @Inject(JdsBiDropdownPortalService)
    private readonly dropdownService: JdsBiDropdownPortalService,
  ) {}

  @jdsBiPure
  get position(): 'absolute' | 'fixed' {
    return jdsBiCheckFixedPosition(this.elementRef.nativeElement)
      ? 'fixed'
      : 'absolute';
  }

  ngAfterViewChecked(): void {
    this.dropdownBoxRef?.changeDetectorRef.detectChanges();
    this.dropdownBoxRef?.changeDetectorRef.markForCheck();
  }

  ngOnChanges(): void {
    if (!this.content) {
      this.toggle(false);
    }
  }

  ngOnDestroy(): void {
    this.toggle(false);
  }

  getClientRect(): ClientRect {
    return this.elementRef.nativeElement.getBoundingClientRect();
  }

  toggle(show: boolean): void {
    if (show && this.content && !this.dropdownBoxRef) {
      this.dropdownBoxRef = this.dropdownService.add(this.component);
    } else if (!show && this.dropdownBoxRef) {
      this.dropdownService.remove(this.dropdownBoxRef);
      this.dropdownBoxRef = null;
    }
  }
}
