import { Directive, Inject } from '@angular/core';
import {
  jdsBiAsPositionAccessor,
  JdsBiPositionAccessor,
  JdsBiRectAccessor,
} from '@jds-bi/core/abstract';
import { JDS_BI_VIEWPORT } from '@jds-bi/core/tokens';
import { JdsBiPoint, JdsBiVerticalDirection } from '@jds-bi/core/types';

import {
  JDS_BI_DROPDOWN_OPTIONS,
  JdsBiDropdownOptions,
} from './dropdown-options.directive';

@Directive({
  selector:
    '[jdsBiDropdown]:not([jdsBiDropdownCustomPosition]):not([jdsBiDropdownSided])',
  providers: [jdsBiAsPositionAccessor(JdsBiDropdownPositionDirective)],
})
export class JdsBiDropdownPositionDirective implements JdsBiPositionAccessor {
  private previous?: JdsBiVerticalDirection;

  constructor(
    @Inject(JDS_BI_DROPDOWN_OPTIONS)
    private readonly options: JdsBiDropdownOptions,
    @Inject(JDS_BI_VIEWPORT) private readonly viewport: JdsBiRectAccessor,
    @Inject(JdsBiRectAccessor) private readonly accessor: JdsBiRectAccessor,
  ) {}

  getPosition({ width, height }: ClientRect): JdsBiPoint {
    const hostRect = this.accessor.getClientRect();
    const viewport = this.viewport.getClientRect();
    const { minHeight, align, direction, offset } = this.options;
    const previous = this.previous || direction || 'bottom';
    const right = Math.max(hostRect.right - width, offset);
    const available = {
      top: hostRect.top - 2 * offset - viewport.top,
      bottom: viewport.bottom - hostRect.bottom - 2 * offset,
    } as const;
    const position = {
      top: hostRect.top - offset - height,
      bottom: hostRect.bottom + offset,
      right,
      left:
        hostRect.left + width < viewport.right - offset ? hostRect.left : right,
    } as const;
    const better = available.top > available.bottom ? 'top' : 'bottom';

    if (
      (available[previous] > minHeight && direction) ||
      available[previous] > height
    ) {
      return [position[previous], position[align]];
    }

    this.previous = better;

    return [position[better], position[align]];
  }
}
