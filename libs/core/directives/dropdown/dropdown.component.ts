import { AnimationOptions } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  Inject,
  OnDestroy,
  Optional,
  Self,
} from '@angular/core';
import { WINDOW } from '@ng-web-apis/common';
import {
  AbstractJdsBiPortalHostComponent,
  JdsBiDestroyService,
  jdsBiGetClosestFocusable,
  jdsBiPx,
} from '@jds-bi/cdk';
import { JdsBiRectAccessor } from '@jds-bi/core/abstract';
import { jdsBiDropdownAnimation } from '@jds-bi/core/animations';
import { JdsBiDropdownAnimation } from '@jds-bi/core/enums';
import { JdsBiPositionService } from '@jds-bi/core/services';
import { JDS_BI_ANIMATION_OPTIONS } from '@jds-bi/core/tokens';
import { JdsBiPoint } from '@jds-bi/core/types';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

// TODO: find the best way for prevent cycle
import { JdsBiDropdownDirective } from './dropdown.directive';
import { JdsBiDropdownHoverDirective } from './dropdown-hover.directive';
import {
  JDS_BI_DROPDOWN_OPTIONS,
  JdsBiDropdownOptions,
} from './dropdown-options.directive';

/**
 * @description:
 * This component is used to show template in a portal
 * using default style of white rounded box with a shadow
 */
@Component({
  selector: 'jds-bi-dropdown',
  templateUrl: './dropdown.template.html',
  styleUrls: ['./dropdown.style.scss'],
  providers: [JdsBiDestroyService, JdsBiPositionService],
  animations: [jdsBiDropdownAnimation],
  // @bad TODO: OnPush
  // eslint-disable-next-line @angular-eslint/prefer-on-push-component-change-detection
  changeDetection: ChangeDetectionStrategy.Default,
})
export class JdsBiDropdownComponent implements OnDestroy {
  @HostBinding('@jdsBiDropdownAnimation')
  readonly dropdownAnimation = {
    value: JdsBiDropdownAnimation.FadeInTop,
    ...this.animationOptions,
  };

  constructor(
    @Inject(JdsBiPositionService) position$: Observable<JdsBiPoint>,
    @Self() @Inject(JdsBiDestroyService) destroy$: Observable<void>,
    @Inject(JdsBiDropdownDirective) readonly directive: JdsBiDropdownDirective,
    @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>,
    @Inject(AbstractJdsBiPortalHostComponent)
    private readonly host: AbstractJdsBiPortalHostComponent,
    @Inject(JdsBiRectAccessor) private readonly accessor: JdsBiRectAccessor,
    @Inject(WINDOW) private readonly windowRef: Window,
    @Inject(JDS_BI_ANIMATION_OPTIONS)
    private readonly animationOptions: AnimationOptions,
    @Inject(JDS_BI_DROPDOWN_OPTIONS)
    private readonly options: JdsBiDropdownOptions,
    @Optional()
    @Inject(JdsBiDropdownHoverDirective)
    private readonly hoverDirective: JdsBiDropdownHoverDirective | null,
  ) {
    position$.pipe(takeUntil(destroy$)).subscribe(([top, left]) => {
      this.update(top, left);
    });

    this.updateWidth();
  }

  // ngAfterViewInit(): void {
  //   const content = this.elementRef.nativeElement.querySelector('.t-content');

  //   const dirHeight = content?.getBoundingClientRect().height || 0;
  //   const comHeight = this.options.maxHeight;

  //   if (dirHeight > comHeight) {
  //     this.isScroll = true;
  //   }
  // }

  ngOnDestroy(): void {
    this.onHoveredChange(false);
  }

  onHoveredChange(hovered: boolean): void {
    if (this.hoverDirective) {
      this.hoverDirective.toggle(hovered);
    }
  }

  onTopFocus(): void {
    this.moveFocusOutside(true);
  }

  onBottomFocus(): void {
    this.moveFocusOutside(false);
  }

  private update(top: number, left: number): void {
    const { style } = this.elementRef.nativeElement;
    const { right } = this.elementRef.nativeElement.getBoundingClientRect();
    const { maxHeight, offset } = this.options;
    const { innerHeight } = this.windowRef;
    const { clientRect } = this.host;
    const { position } = this.directive;
    const rect = this.accessor.getClientRect();
    const offsetX = position === 'fixed' ? 0 : -clientRect.left;
    const offsetY = position === 'fixed' ? 0 : -clientRect.top;

    top += offsetY;
    left += offsetX;

    const isIntersecting =
      left < rect.right && right > rect.left && top < offsetY + 2 * offset;
    const available = isIntersecting
      ? rect.top - 2 * offset
      : offsetY + innerHeight - top - offset;

    style.position = position;
    style.top = jdsBiPx(Math.max(top, offsetY + offset));
    style.left = jdsBiPx(left);
    style.maxHeight = jdsBiPx(Math.min(maxHeight, available));
    style.width = '';
    style.minWidth = '';

    this.updateWidth();
  }

  private updateWidth(): void {
    const { style } = this.elementRef.nativeElement;
    const rect = this.accessor.getClientRect();
    const { limitWidth, minWidth } = this.options;

    switch (limitWidth) {
      case 'min':
        style.minWidth = jdsBiPx(rect.width);
        break;
      case 'fixed':
        style.maxWidth = jdsBiPx(rect.width);
        break;
      case 'custom':
        style.maxWidth = jdsBiPx(minWidth);
        style.width = '100%';
        break;
    }
  }

  private moveFocusOutside(previous: boolean): void {
    const host = document.createElement('div');
    const { ownerDocument } = host;
    const root = ownerDocument ? ownerDocument.body : host;
    let focusable = jdsBiGetClosestFocusable({ initial: host, root, previous });

    while (focusable !== null && host.contains(focusable)) {
      focusable = jdsBiGetClosestFocusable({
        initial: focusable,
        root,
        previous,
      });
    }

    focusable?.focus();
  }
}
