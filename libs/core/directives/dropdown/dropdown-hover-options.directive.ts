import { InjectionToken, ValueProvider } from '@angular/core';

export interface JdsBiDropdownHoverOptions {
  readonly showDelay: number;
  readonly hideDelay: number;
}

/** Default values for hint options */
export const JDS_BI_DROPDOWN_HOVER_DEFAULT_OPTIONS: JdsBiDropdownHoverOptions =
  {
    showDelay: 200,
    hideDelay: 500,
  };

export const JDS_BI_DROPDOWN_HOVER_OPTIONS =
  new InjectionToken<JdsBiDropdownHoverOptions>(
    '[JDS_BI_DROPDOWN_HOVER_OPTIONS] Default parameters for dropdown hover directive',
    {
      factory: () => JDS_BI_DROPDOWN_HOVER_DEFAULT_OPTIONS,
    },
  );

export const jdsBiDropdownHoverOptionsProvider: (
  options: Partial<JdsBiDropdownHoverOptions>,
) => ValueProvider = (options: Partial<JdsBiDropdownHoverOptions>) => ({
  provide: JDS_BI_DROPDOWN_HOVER_DEFAULT_OPTIONS,
  useValue: { ...JDS_BI_DROPDOWN_HOVER_DEFAULT_OPTIONS, ...options },
});
