import {
  Directive,
  FactoryProvider,
  forwardRef,
  Inject,
  InjectionToken,
  Input,
  Optional,
  SkipSelf,
} from '@angular/core';
import { jdsBiDefaultProp } from '@jds-bi/cdk';
import {
  JdsBiDropdownWidth,
  JdsBiHorizontalDirection,
  JdsBiVerticalDirection,
} from '@jds-bi/core/types';
import { jdsBiOverrideOptions } from '@jds-bi/core/utils';

export interface JdsBiDropdownOptions {
  readonly align: JdsBiHorizontalDirection;
  readonly direction: JdsBiVerticalDirection | null;
  readonly limitWidth: JdsBiDropdownWidth;
  readonly minWidth: number;
  readonly minHeight: number;
  readonly maxHeight: number;
  readonly offset: number;
}

/** Default values for dropdown options */
export const JDS_BI_DROPDOWN_DEFAULT_OPTIONS: JdsBiDropdownOptions = {
  align: 'left',
  direction: null,
  limitWidth: 'auto',
  minWidth: 160,
  maxHeight: 400,
  minHeight: 80,
  offset: 4,
};

export const JDS_BI_DROPDOWN_OPTIONS = new InjectionToken<JdsBiDropdownOptions>(
  '[JDS_BI_DROPDOWN_OPTIONS] Default parameters for dropdown directive',
  {
    factory: () => JDS_BI_DROPDOWN_DEFAULT_OPTIONS,
  },
);

export const jdsBiDropdownOptionsProvider: (
  options: Partial<JdsBiDropdownOptions>,
) => FactoryProvider = (override: Partial<JdsBiDropdownOptions>) => ({
  provide: JDS_BI_DROPDOWN_OPTIONS,
  deps: [
    [new Optional(), JdsBiDropdownOptionsDirective],
    [new SkipSelf(), JDS_BI_DROPDOWN_OPTIONS],
  ],
  useFactory: jdsBiOverrideOptions(override),
});

@Directive({
  selector:
    '[jdsBiDropdownAlign], [jdsBiDropdownDirection], [jdsBiDropdownLimitWidth], [jdsBiDropdownMinWidth], [jdsBiDropdownMinHeight], [jdsBiDropdownMaxHeight], [jdsBiDropdownOffset]',
  providers: [
    {
      provide: JDS_BI_DROPDOWN_OPTIONS,
      useExisting: forwardRef(() => JdsBiDropdownOptionsDirective),
    },
  ],
})
export class JdsBiDropdownOptionsDirective implements JdsBiDropdownOptions {
  @Input('jdsBiDropdownAlign')
  @jdsBiDefaultProp()
  align = this.options.align;

  @Input('jdsBiDropdownDirection')
  @jdsBiDefaultProp()
  direction = this.options.direction;

  @Input('jdsBiDropdownLimitWidth')
  @jdsBiDefaultProp()
  limitWidth = this.options.limitWidth;

  @Input('jdsBiDropdownMinWidth')
  @jdsBiDefaultProp()
  minWidth = this.options.minWidth;

  @Input('jdsBiDropdownMinHeight')
  @jdsBiDefaultProp()
  minHeight = this.options.minHeight;

  @Input('jdsBiDropdownMaxHeight')
  @jdsBiDefaultProp()
  maxHeight = this.options.maxHeight;

  @Input('jdsBiDropdownOffset')
  @jdsBiDefaultProp()
  offset = this.options.offset;

  constructor(
    @SkipSelf()
    @Inject(JDS_BI_DROPDOWN_OPTIONS)
    private readonly options: JdsBiDropdownOptions,
  ) {}
}
