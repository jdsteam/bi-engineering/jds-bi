import { Directive, Input, OnChanges } from '@angular/core';
import { jdsBiAsDriver, JdsBiDriver } from '@jds-bi/core/abstract';
import { Subject } from 'rxjs';

@Directive({
  selector: '[jdsBiDropdown][jdsBiDropdownManual]',
  providers: [jdsBiAsDriver(JdsBiDropdownManualDirective)],
})
export class JdsBiDropdownManualDirective
  extends JdsBiDriver
  implements OnChanges
{
  private readonly stream$ = new Subject<boolean>();

  @Input()
  jdsBiDropdownManual = false;

  constructor() {
    super((subscriber) => this.stream$.subscribe(subscriber));
  }

  ngOnChanges(): void {
    this.stream$.next(this.jdsBiDropdownManual);
  }
}
