import { Directive, Input } from '@angular/core';
import { EMPTY_CLIENT_RECT } from '@jds-bi/cdk';
import { jdsBiAsRectAccessor, JdsBiRectAccessor } from '@jds-bi/core/abstract';

@Directive({
  selector: '[jdsBiDropdown][jdsBiDropdownHost]',
  providers: [jdsBiAsRectAccessor(JdsBiDropdownHostDirective)],
})
export class JdsBiDropdownHostDirective extends JdsBiRectAccessor {
  @Input()
  jdsBiDropdownHost?: HTMLElement;

  getClientRect(): ClientRect {
    return this.jdsBiDropdownHost?.getBoundingClientRect() || EMPTY_CLIENT_RECT;
  }
}
