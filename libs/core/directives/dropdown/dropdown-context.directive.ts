import { Directive, HostListener, Inject } from '@angular/core';
import {
  EMPTY_CLIENT_RECT,
  JdsBiActiveZoneDirective,
  jdsBiPointToClientRect,
} from '@jds-bi/cdk';
import {
  jdsBiAsDriver,
  jdsBiAsRectAccessor,
  JdsBiDriver,
  JdsBiRectAccessor,
} from '@jds-bi/core/abstract';
import { shouldCall } from '@tinkoff/ng-event-plugins';
import { Subject } from 'rxjs';

function activeZoneFilter(
  this: JdsBiDropdownContextDirective,
  target: Element,
): boolean {
  return !this.activeZone.contains(target);
}

@Directive({
  selector: '[jdsBiDropdown][jdsBiDropdownContext]',
  providers: [
    JdsBiActiveZoneDirective,
    jdsBiAsDriver(JdsBiDropdownContextDirective),
    jdsBiAsRectAccessor(JdsBiDropdownContextDirective),
  ],
})
export class JdsBiDropdownContextDirective
  extends JdsBiDriver
  implements JdsBiRectAccessor
{
  private readonly stream$ = new Subject<boolean>();

  private currentRect = EMPTY_CLIENT_RECT;

  constructor(
    @Inject(JdsBiActiveZoneDirective)
    readonly activeZone: JdsBiActiveZoneDirective,
  ) {
    super((subscriber) => this.stream$.subscribe(subscriber));
  }

  @HostListener('contextmenu.prevent.stop', [
    '$event.clientX',
    '$event.clientY',
  ])
  onContextMenu(x: number, y: number): void {
    this.currentRect = jdsBiPointToClientRect(x, y);
    this.stream$.next(true);
  }

  @shouldCall(activeZoneFilter)
  @HostListener('document:click.silent', ['$event.target'])
  @HostListener('document:contextmenu.capture.silent', ['$event.target'])
  @HostListener('document:keydown.esc', ['$event.currentTarget'])
  closeDropdown(): void {
    this.stream$.next(false);
  }

  getClientRect(): ClientRect {
    return this.currentRect;
  }
}
