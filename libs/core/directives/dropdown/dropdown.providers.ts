import { InjectionToken, Type } from '@angular/core';

// TODO: find the best way for prevent cycle
import { JdsBiDropdownComponent } from './dropdown.component';

export const JDS_BI_DROPDOWN_COMPONENT = new InjectionToken<Type<any>>(
  `[JDS_BI_DROPDOWN_COMPONENT] A component to display a dropdown`,
  {
    factory: () => JdsBiDropdownComponent,
  },
);
