import { Directive, Inject, Input } from '@angular/core';
import { jdsBiDefaultProp, JdsBiHoveredService } from '@jds-bi/cdk';
import { jdsBiAsDriver, JdsBiDriver } from '@jds-bi/core/abstract';
import { merge, Observable, of, Subject } from 'rxjs';
import { delay, share, switchMap, tap } from 'rxjs/operators';

import {
  JDS_BI_DROPDOWN_HOVER_OPTIONS,
  JdsBiDropdownHoverOptions,
} from './dropdown-hover-options.directive';

@Directive({
  selector: '[jdsBiDropdownHover]:not(ng-container)',
  providers: [jdsBiAsDriver(JdsBiDropdownHoverDirective), JdsBiHoveredService],
})
export class JdsBiDropdownHoverDirective extends JdsBiDriver {
  private readonly toggle$ = new Subject<boolean>();
  private readonly stream$ = merge(this.toggle$, this.hovered$).pipe(
    switchMap((visible) =>
      of(visible).pipe(delay(visible ? this.showDelay : this.hideDelay)),
    ),
    tap((visible) => {
      this.hovered = visible;
    }),
    share(),
  );

  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('jdsBiDropdownShowDelay')
  @jdsBiDefaultProp()
  showDelay = this.options.showDelay;

  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('jdsBiDropdownHideDelay')
  @jdsBiDefaultProp()
  hideDelay = this.options.hideDelay;

  hovered = false;

  constructor(
    @Inject(JdsBiHoveredService) private readonly hovered$: Observable<boolean>,
    @Inject(JDS_BI_DROPDOWN_HOVER_OPTIONS)
    private readonly options: JdsBiDropdownHoverOptions,
  ) {
    super((subscriber) => this.stream$.subscribe(subscriber));
  }

  toggle(visible: boolean): void {
    this.toggle$.next(visible);
  }
}
