import { NgModule } from '@angular/core';
import {
  JdsBiActiveZoneModule,
  JdsBiHoveredModule,
  JdsBiOverscrollModule,
} from '@jds-bi/cdk';
import { JdsBiScrollbarModule } from '@jds-bi/core/components/scrollbar';
import { JdsBiModeModule } from '@jds-bi/core/directives/mode';
import { PolymorpheusModule } from '@tinkoff/ng-polymorpheus';

import { JdsBiDropdownComponent } from './dropdown.component';
import { JdsBiDropdownDirective } from './dropdown.directive';
import { JdsBiDropdownContextDirective } from './dropdown-context.directive';
import { JdsBiDropdownDriverDirective } from './dropdown-driver.directive';
import { JdsBiDropdownHostDirective } from './dropdown-host.directive';
import { JdsBiDropdownHoverDirective } from './dropdown-hover.directive';
import { JdsBiDropdownManualDirective } from './dropdown-manual.directive';
import { JdsBiDropdownOptionsDirective } from './dropdown-options.directive';
import { JdsBiDropdownPositionDirective } from './dropdown-position.directive';
import { JdsBiDropdownPositionSidedDirective } from './dropdown-position-sided.directive';
import { JdsBiDropdownSelectionDirective } from './dropdown-selection.directive';

@NgModule({
  imports: [
    PolymorpheusModule,
    JdsBiActiveZoneModule,
    JdsBiOverscrollModule,
    JdsBiScrollbarModule,
    JdsBiModeModule,
    JdsBiHoveredModule,
  ],
  declarations: [
    JdsBiDropdownDirective,
    JdsBiDropdownComponent,
    JdsBiDropdownOptionsDirective,
    JdsBiDropdownHostDirective,
    JdsBiDropdownDriverDirective,
    JdsBiDropdownManualDirective,
    JdsBiDropdownHoverDirective,
    JdsBiDropdownContextDirective,
    JdsBiDropdownPositionDirective,
    JdsBiDropdownPositionSidedDirective,
    JdsBiDropdownSelectionDirective,
  ],
  exports: [
    JdsBiDropdownDirective,
    JdsBiDropdownComponent,
    JdsBiDropdownOptionsDirective,
    JdsBiDropdownHostDirective,
    JdsBiDropdownDriverDirective,
    JdsBiDropdownManualDirective,
    JdsBiDropdownHoverDirective,
    JdsBiDropdownContextDirective,
    JdsBiDropdownPositionDirective,
    JdsBiDropdownPositionSidedDirective,
    JdsBiDropdownSelectionDirective,
  ],
})
export class JdsBiDropdownModule {}
