import { Directive } from '@angular/core';
import { JdsBiDestroyService } from '@jds-bi/cdk';
import { AbstractJdsBiDriverDirective } from '@jds-bi/core/abstract';

@Directive({
  selector: '[jdsBiDropdown]',
  providers: [JdsBiDestroyService],
})
export class JdsBiDropdownDriverDirective extends AbstractJdsBiDriverDirective {}
