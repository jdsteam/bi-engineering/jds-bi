import { Directive, Inject, Input } from '@angular/core';
import {
  jdsBiAsPositionAccessor,
  JdsBiPositionAccessor,
  JdsBiRectAccessor,
} from '@jds-bi/core/abstract';
import { JDS_BI_VIEWPORT } from '@jds-bi/core/tokens';
import { JdsBiPoint } from '@jds-bi/core/types';

import {
  JDS_BI_DROPDOWN_OPTIONS,
  JdsBiDropdownOptions,
} from './dropdown-options.directive';
import { JdsBiDropdownPositionDirective } from './dropdown-position.directive';

@Directive({
  selector: '[jdsBiDropdownSided]',
  providers: [
    JdsBiDropdownPositionDirective,
    jdsBiAsPositionAccessor(JdsBiDropdownPositionSidedDirective),
  ],
})
export class JdsBiDropdownPositionSidedDirective
  implements JdsBiPositionAccessor
{
  private previous = this.options.direction || 'bottom';

  @Input()
  jdsBiDropdownSided: boolean | string = '';

  @Input()
  jdsBiDropdownSidedOffset = 4;

  constructor(
    @Inject(JDS_BI_DROPDOWN_OPTIONS)
    private readonly options: JdsBiDropdownOptions,
    @Inject(JDS_BI_VIEWPORT) private readonly viewport: JdsBiRectAccessor,
    @Inject(JdsBiRectAccessor) private readonly accessor: JdsBiRectAccessor,
    @Inject(JdsBiDropdownPositionDirective)
    private readonly vertical: JdsBiPositionAccessor,
  ) {}

  getPosition(rect: ClientRect): JdsBiPoint {
    if (this.jdsBiDropdownSided === false) {
      return this.vertical.getPosition(rect);
    }

    const { height, width } = rect;
    const hostRect = this.accessor.getClientRect();
    const viewport = this.viewport.getClientRect();
    const { align, direction, minHeight, offset } = this.options;
    const available = {
      top: hostRect.bottom - viewport.top,
      left: hostRect.left - offset - viewport.left,
      right: viewport.right - hostRect.right - offset,
      bottom: viewport.bottom - hostRect.top,
    } as const;
    const position = {
      top: hostRect.bottom - height + this.jdsBiDropdownSidedOffset + 1, // 1 for border
      left: hostRect.left - width - offset,
      right: hostRect.right + offset,
      bottom: hostRect.top - this.jdsBiDropdownSidedOffset - 1, // 1 for border
    } as const;
    const better = available.top > available.bottom ? 'top' : 'bottom';
    const maxLeft =
      available.left > available.right ? position.left : position.right;
    const left = available[align] > width ? position[align] : maxLeft;

    if (
      (available[this.previous] > minHeight && direction) ||
      this.previous === better
    ) {
      return [position[this.previous], left];
    }

    this.previous = better;

    return [position[better], left];
  }
}
