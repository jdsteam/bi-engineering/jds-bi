import { DOCUMENT } from '@angular/common';
import {
  Directive,
  ElementRef,
  Inject,
  Input,
  OnDestroy,
  ViewContainerRef,
} from '@angular/core';
import {
  ALWAYS_TRUE_HANDLER,
  CHAR_NO_BREAK_SPACE,
  CHAR_ZERO_WIDTH_SPACE,
  EMPTY_CLIENT_RECT,
  JDS_BI_RANGE,
  JdsBiBooleanHandler,
  jdsBiDefaultProp,
  jdsBiGetNativeFocused,
  jdsBiIsElement,
  jdsBiIsString,
  jdsBiIsTextfield,
  jdsBiIsTextNode,
  jdsBiPx,
} from '@jds-bi/cdk';
import {
  jdsBiAsDriver,
  jdsBiAsRectAccessor,
  JdsBiDriver,
  JdsBiRectAccessor,
} from '@jds-bi/core/abstract';
import { JDS_BI_SELECTION_STREAM } from '@jds-bi/core/tokens';
import { jdsBiGetWordRange } from '@jds-bi/core/utils';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { JdsBiDropdownDirective } from './dropdown.directive';

@Directive({
  selector: '[jdsBiDropdown][jdsBiDropdownSelection]',
  providers: [
    jdsBiAsDriver(JdsBiDropdownSelectionDirective),
    jdsBiAsRectAccessor(JdsBiDropdownSelectionDirective),
  ],
})
export class JdsBiDropdownSelectionDirective
  extends JdsBiDriver
  implements JdsBiRectAccessor, OnDestroy
{
  private readonly handler$ = new BehaviorSubject<JdsBiBooleanHandler<Range>>(
    ALWAYS_TRUE_HANDLER,
  );

  private readonly stream$ = combineLatest([
    this.handler$,
    this.selection$.pipe(
      map(() => this.getRange()),
      distinctUntilChanged(),
    ),
  ]).pipe(
    map(([handler, range]) => {
      const contained = this.elementRef.nativeElement.contains(
        range.commonAncestorContainer,
      );

      this.range =
        contained && jdsBiIsTextNode(range.commonAncestorContainer)
          ? range
          : this.range;

      return (contained && handler(this.range)) || this.inDropdown(range);
    }),
  );

  private ghost?: HTMLElement;

  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('jdsBiDropdownSelectionPosition')
  @jdsBiDefaultProp()
  position: 'selection' | 'tag' | 'word' = 'selection';

  @Input()
  set jdsBiDropdownSelection(visible: JdsBiBooleanHandler<Range> | string) {
    if (!jdsBiIsString(visible)) {
      this.handler$.next(visible);
    }
  }

  constructor(
    @Inject(JDS_BI_RANGE) private range: Range,
    @Inject(DOCUMENT) private readonly documentRef: Document,
    @Inject(JDS_BI_SELECTION_STREAM)
    private readonly selection$: Observable<unknown>,
    @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>,
    @Inject(ViewContainerRef)
    private readonly viewContainerRef: ViewContainerRef,
    @Inject(JdsBiDropdownDirective)
    private readonly dropdown: JdsBiDropdownDirective,
  ) {
    super((subscriber) => this.stream$.subscribe(subscriber));
  }

  getClientRect(): ClientRect {
    switch (this.position) {
      case 'tag': {
        const { commonAncestorContainer } = this.range;
        const element = jdsBiIsElement(commonAncestorContainer)
          ? commonAncestorContainer
          : commonAncestorContainer.parentNode;

        return element && jdsBiIsElement(element)
          ? element.getBoundingClientRect()
          : EMPTY_CLIENT_RECT;
      }
      case 'word':
        return jdsBiGetWordRange(this.range).getBoundingClientRect();
      default:
        return this.range.getBoundingClientRect();
    }
  }

  ngOnDestroy(): void {
    if (this.ghost) {
      this.viewContainerRef.element.nativeElement.removeChild(this.ghost);
    }
  }

  private getRange(): Range {
    const { nativeElement } = this.elementRef;
    const active = jdsBiGetNativeFocused(this.documentRef);
    const selection = this.documentRef.getSelection();

    if (active && jdsBiIsTextfield(active) && nativeElement.contains(active)) {
      return this.veryVerySadInputFix(active);
    }

    return selection?.rangeCount ? selection.getRangeAt(0) : this.range;
  }

  /**
   * Check if Node is inside dropdown
   */
  private boxContains(node: Node): boolean {
    return !!this.dropdown.dropdownBoxRef?.location.nativeElement.contains(
      node,
    );
  }

  /**
   * Check if given range is at least partially inside dropdown
   */
  private inDropdown(range: Range): boolean {
    const { startContainer, endContainer } = range;
    const { nativeElement } = this.elementRef;
    const inDropdown = this.boxContains(range.commonAncestorContainer);
    const hostToDropdown =
      this.boxContains(endContainer) && nativeElement.contains(startContainer);
    const dropdownToHost =
      this.boxContains(startContainer) && nativeElement.contains(endContainer);

    return inDropdown || hostToDropdown || dropdownToHost;
  }

  private veryVerySadInputFix(
    element: HTMLInputElement | HTMLTextAreaElement,
  ): Range {
    const { ghost = this.initGhost(element) } = this;
    const { top, left, width, height } = element.getBoundingClientRect();
    const { selectionStart, selectionEnd, value } = element;
    const range = this.documentRef.createRange();
    const hostRect = this.elementRef.nativeElement.getBoundingClientRect();

    ghost.style.top = jdsBiPx(top - hostRect.top);
    ghost.style.left = jdsBiPx(left - hostRect.left);
    ghost.style.width = jdsBiPx(width);
    ghost.style.height = jdsBiPx(height);
    ghost.textContent = CHAR_ZERO_WIDTH_SPACE + value + CHAR_NO_BREAK_SPACE;

    range.setStart(ghost.firstChild as Node, selectionStart || 0);
    range.setEnd(ghost.firstChild as Node, selectionEnd || 0);

    return range;
  }

  /**
   * Create an invisible DIV styled exactly like input/textarea element inside directive
   */
  private initGhost(
    element: HTMLInputElement | HTMLTextAreaElement,
  ): HTMLElement {
    const ghost = this.documentRef.createElement('div');
    const { nativeElement } = this.viewContainerRef.element;
    const { font, letterSpacing, textTransform, padding } =
      getComputedStyle(element);

    ghost.style.position = 'absolute';
    ghost.style.pointerEvents = 'none';
    ghost.style.opacity = '0';
    ghost.style.whiteSpace = 'pre-wrap';
    ghost.style.font = font;
    ghost.style.letterSpacing = letterSpacing;
    ghost.style.textTransform = textTransform;
    ghost.style.padding = padding;

    nativeElement.appendChild(ghost);
    this.ghost = ghost;

    return ghost;
  }
}
