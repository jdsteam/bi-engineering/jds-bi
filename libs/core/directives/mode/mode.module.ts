import { NgModule } from '@angular/core';

import { JdsBiModeDirective } from './mode.directive';

@NgModule({
  declarations: [JdsBiModeDirective],
  exports: [JdsBiModeDirective],
})
export class JdsBiModeModule {}
