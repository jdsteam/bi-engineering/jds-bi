import { Directive, Input } from '@angular/core';
import { AbstractJdsBiController } from '@jds-bi/cdk';
import { JdsBiBrightness } from '@jds-bi/core/types';

@Directive({
  selector: '[jdsBiMode]',
})
export class JdsBiModeDirective extends AbstractJdsBiController {
  @Input('jdsBiMode')
  mode: JdsBiBrightness | null = null;
}
