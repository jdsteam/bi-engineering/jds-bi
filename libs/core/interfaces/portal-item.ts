import { JdsBiActiveZoneDirective } from '@jds-bi/cdk';
import {
  PolymorpheusComponent,
  PolymorpheusContent,
} from '@tinkoff/ng-polymorpheus';

export interface JdsBiPortalItem<C = any> {
  readonly component: PolymorpheusComponent<any, any>;
  readonly content: PolymorpheusContent<C>;
  readonly context?: C;
  readonly appearance?: string;
  readonly activeZone?: JdsBiActiveZoneDirective | null;
}
