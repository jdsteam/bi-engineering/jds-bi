export interface JdsBiMedia {
  readonly mobile: number;
  readonly tablet?: number;
  readonly desktopSmall: number;
  readonly desktopLarge: number;
}
