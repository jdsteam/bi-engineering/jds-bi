import { ExistingProvider, Type } from '@angular/core';

// TODO: Rename to getBoundingClientRect to match the DOM API
// eslint-disable-next-line @typescript-eslint/naming-convention
export abstract class JdsBiRectAccessor {
  abstract getClientRect(): ClientRect;
}

export function jdsBiAsRectAccessor(
  useExisting: Type<JdsBiRectAccessor>,
): ExistingProvider {
  return {
    provide: JdsBiRectAccessor,
    useExisting,
  };
}
