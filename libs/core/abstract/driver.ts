import { ExistingProvider, Type } from '@angular/core';
import { Observable } from 'rxjs';

// eslint-disable-next-line @typescript-eslint/naming-convention
export abstract class JdsBiDriver extends Observable<boolean> {}

export function jdsBiAsDriver(
  useExisting: Type<JdsBiDriver>,
): ExistingProvider {
  return {
    provide: JdsBiDriver,
    useExisting,
  };
}
