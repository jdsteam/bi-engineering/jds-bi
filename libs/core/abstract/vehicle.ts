import { ExistingProvider, Type } from '@angular/core';

// eslint-disable-next-line @typescript-eslint/naming-convention
export abstract class JdsBiVehicle {
  abstract toggle(value: boolean): void;
}

export function jdsBiAsVehicle(
  useExisting: Type<JdsBiVehicle>,
): ExistingProvider {
  return {
    provide: JdsBiVehicle,
    useExisting,
  };
}
