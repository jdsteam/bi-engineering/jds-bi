import { Directive, Inject, Self } from '@angular/core';
import { JdsBiDestroyService } from '@jds-bi/cdk';
import { Observable } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { JdsBiDriver } from './driver';
import { JdsBiVehicle } from './vehicle';

@Directive()
export abstract class AbstractJdsBiDriverDirective {
  constructor(
    @Self() @Inject(JdsBiDestroyService) destroy$: Observable<unknown>,
    @Inject(JdsBiDriver) driver$: Observable<boolean>,
    @Inject(JdsBiVehicle) vehicle: JdsBiVehicle,
  ) {
    driver$
      .pipe(distinctUntilChanged(), takeUntil(destroy$))
      .subscribe((value) => {
        vehicle.toggle(value);
      });
  }
}
