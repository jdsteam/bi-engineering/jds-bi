import { ExistingProvider, Type } from '@angular/core';
import { JdsBiPoint } from '@jds-bi/core/types';

// eslint-disable-next-line @typescript-eslint/naming-convention
export abstract class JdsBiPositionAccessor {
  abstract getPosition(rect: ClientRect): JdsBiPoint;
}

export function jdsBiAsPositionAccessor(
  useExisting: Type<JdsBiPositionAccessor>,
): ExistingProvider {
  return {
    provide: JdsBiPositionAccessor,
    useExisting,
  };
}
