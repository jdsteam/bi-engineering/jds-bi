import { ElementRef, Optional, Provider } from '@angular/core';
import { JdsBiModeDirective } from '@jds-bi/core/directives/mode';
import { JDS_BI_MODE } from '@jds-bi/core/tokens';
import { JdsBiBrightness } from '@jds-bi/core/types';
import { Observable, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export const MODE_PROVIDER: Provider = {
  provide: JDS_BI_MODE,
  deps: [[new Optional(), JdsBiModeDirective], ElementRef],
  useFactory: (
    mode: JdsBiModeDirective | null,
    { nativeElement }: ElementRef,
  ): Observable<JdsBiBrightness | null> => {
    const mode$ = mode
      ? mode.change$.pipe(
          startWith(null),
          map(() => mode.mode),
        )
      : of(null);

    nativeElement[`$.data-mode.attr`] = mode$;

    return mode$;
  },
};
