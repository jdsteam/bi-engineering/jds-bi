import { ElementRef, Provider, SkipSelf } from '@angular/core';
import { JDS_BI_IS_MOBILE_RES } from '@jds-bi/core/tokens';
import { Observable } from 'rxjs';

export const JDS_BI_IS_MOBILE_RES_PROVIDER: Provider = {
  provide: JDS_BI_IS_MOBILE_RES,
  deps: [[new SkipSelf(), JDS_BI_IS_MOBILE_RES], ElementRef],
  useFactory: (
    mobile$: Observable<boolean>,
    { nativeElement }: ElementRef,
  ): Observable<boolean> => {
    nativeElement[`$.class._mobile`] = mobile$;

    return mobile$;
  },
};
