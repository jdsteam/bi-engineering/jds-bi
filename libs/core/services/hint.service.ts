import { Injectable } from '@angular/core';
import { JdsBiPortalItem } from '@jds-bi/core/interfaces';
import { BehaviorSubject } from 'rxjs';

/**
 * Service for displaying hints/tooltips
 */
@Injectable({
  providedIn: `root`,
})
export class JdsBiHintService extends BehaviorSubject<
  readonly JdsBiPortalItem[]
> {
  constructor() {
    super([]);
  }

  add(directive: JdsBiPortalItem): void {
    this.next(this.value.concat(directive));
  }

  remove(directive: JdsBiPortalItem): void {
    if (this.value.includes(directive)) {
      this.next(this.value.filter((hint) => hint !== directive));
    }
  }
}
