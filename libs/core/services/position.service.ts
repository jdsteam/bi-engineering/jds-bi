import { ElementRef, Inject, Injectable, NgZone } from '@angular/core';
import { ANIMATION_FRAME } from '@ng-web-apis/common';
import { jdsBiZonefree } from '@jds-bi/cdk';
import { JdsBiPositionAccessor } from '@jds-bi/core/abstract';
import { JdsBiPoint } from '@jds-bi/core/types';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class JdsBiPositionService extends Observable<JdsBiPoint> {
  constructor(
    @Inject(ElementRef) { nativeElement }: ElementRef<HTMLElement>,
    @Inject(ANIMATION_FRAME) animationFrame: Observable<unknown>,
    @Inject(NgZone) ngZone: NgZone,
    @Inject(JdsBiPositionAccessor) accessor: JdsBiPositionAccessor,
  ) {
    super((subscriber) =>
      animationFrame
        .pipe(
          map(() => nativeElement.getBoundingClientRect()),
          map((rect) => accessor.getPosition(rect)),
          jdsBiZonefree(ngZone),
        )
        .subscribe(subscriber),
    );
  }
}
