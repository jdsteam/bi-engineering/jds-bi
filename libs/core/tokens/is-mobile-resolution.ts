import { inject, InjectionToken, NgZone } from '@angular/core';
import { WINDOW } from '@ng-web-apis/common';
import { jdsBiTypedFromEvent, jdsBiZoneOptimized } from '@jds-bi/cdk';
import { jdsBiIsMobile } from '@jds-bi/core/utils';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, share, startWith } from 'rxjs/operators';

import { JDS_BI_MEDIA } from './media';

export const JDS_BI_IS_MOBILE_RES = new InjectionToken<Observable<boolean>>(
  `[JDS_BI_IS_MOBILE_RES]: Mobile resolution stream for private providers`,
  {
    factory: () => {
      const windowRef = inject(WINDOW);
      const media = inject(JDS_BI_MEDIA);

      return jdsBiTypedFromEvent(windowRef, `resize`).pipe(
        share(),
        startWith(null),
        map(() => jdsBiIsMobile(windowRef, media)),
        distinctUntilChanged(),
        jdsBiZoneOptimized(inject(NgZone)),
      );
    },
  },
);
