import { ElementRef, InjectionToken } from '@angular/core';

export const JDS_BI_SCROLL_REF = new InjectionToken<ElementRef<HTMLElement>>(
  `[JDS_BI_SCROLL_REF]: Scrollable container`,
);
