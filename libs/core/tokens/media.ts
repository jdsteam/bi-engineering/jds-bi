import { InjectionToken } from '@angular/core';
import { JdsBiMedia } from '@jds-bi/core/interfaces';

export const JDS_BI_MEDIA = new InjectionToken<JdsBiMedia>(
  `[JDS_BI_MEDIA]: Token for media constant`,
  {
    factory: () => ({
      mobile: 768,
      desktopSmall: 1024,
      desktopLarge: 1280,
    }),
  },
);
