import { InjectionToken } from '@angular/core';
import { JdsBiBrightness } from '@jds-bi/core/types';
import { Observable } from 'rxjs';

export const JDS_BI_MODE: InjectionToken<Observable<JdsBiBrightness | null>> =
  new InjectionToken<Observable<JdsBiBrightness | null>>(
    `[JDS_BI_MODE]: Mode stream for private providers`,
  );
