import { ElementRef, InjectionToken } from '@angular/core';

export const JDS_BI_ELEMENT_REF = new InjectionToken<ElementRef>(
  `[JDS_BI_ELEMENT_REF]: ElementRef when you cannot use @Input for single time injection`,
);
