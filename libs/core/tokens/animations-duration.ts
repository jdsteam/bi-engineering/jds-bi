import { InjectionToken } from '@angular/core';

export const JDS_BI_ANIMATIONS_DURATION = new InjectionToken<number>(
  `[JDS_BI_ANIMATIONS_DURATION]: Duration of all JDS BI animations in ms`,
  {
    factory: () => 300,
  },
);
