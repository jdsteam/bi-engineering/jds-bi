import { DOCUMENT } from '@angular/common';
import { inject, InjectionToken } from '@angular/core';
import { jdsBiTypedFromEvent } from '@jds-bi/cdk';
import { merge, Observable } from 'rxjs';
import { share, switchMap, takeUntil } from 'rxjs/operators';

export const JDS_BI_SELECTION_STREAM = new InjectionToken<Observable<unknown>>(
  `[JDS_BI_SELECTION_STREAM]: A stream of possible selection changes`,
  {
    factory: () => {
      const documentRef = inject(DOCUMENT);

      return merge(
        jdsBiTypedFromEvent(documentRef, `selectionchange`),
        jdsBiTypedFromEvent(documentRef, `mouseup`),
        jdsBiTypedFromEvent(documentRef, `mousedown`).pipe(
          switchMap(() =>
            jdsBiTypedFromEvent(documentRef, `mousemove`).pipe(
              takeUntil(jdsBiTypedFromEvent(documentRef, `mouseup`)),
            ),
          ),
        ),
        jdsBiTypedFromEvent(documentRef, `keydown`),
        jdsBiTypedFromEvent(documentRef, `keyup`),
      ).pipe(share());
    },
  },
);
