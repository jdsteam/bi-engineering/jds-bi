import { InjectionToken, isDevMode } from '@angular/core';

export const JDS_BI_ASSERT_ENABLED = new InjectionToken(
  `[JDS_BI_ASSERT_ENABLED]: Flag to enable assertions across JDS BI`,
  {
    factory: () => isDevMode(),
  },
);
