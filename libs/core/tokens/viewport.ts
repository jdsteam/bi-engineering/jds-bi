import { inject, InjectionToken, Provider, Type } from '@angular/core';
import { WINDOW } from '@ng-web-apis/common';
import { JdsBiRectAccessor } from '@jds-bi/core/abstract';

export const JDS_BI_VIEWPORT = new InjectionToken<JdsBiRectAccessor | any>(
  `[JDS_BI_VIEWPORT]: Viewport accessor`,
  {
    factory: () => {
      const windowRef = inject(WINDOW);

      return {
        getClientRect() {
          return {
            top: 0,
            left: 0,
            right: windowRef.innerWidth,
            bottom: windowRef.innerHeight,
            width: windowRef.innerWidth,
            height: windowRef.innerHeight,
          };
        },
      };
    },
  },
);

export function jdsBiAsViewport(
  useExisting: Type<JdsBiRectAccessor>,
): Provider {
  return {
    provide: JDS_BI_VIEWPORT,
    useExisting,
  };
}
