import { InjectionToken } from '@angular/core';

export const JDS_BI_THEME = new InjectionToken<string>(
  `[JDS_BI_THEME]: Theme name`,
  {
    factory: () => `JDSBI`,
  },
);
