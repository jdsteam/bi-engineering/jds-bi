import { AnimationOptions } from '@angular/animations';
import { inject, InjectionToken } from '@angular/core';

import { JDS_BI_ANIMATIONS_DURATION } from './animations-duration';

export const JDS_BI_ANIMATION_OPTIONS = new InjectionToken<AnimationOptions>(
  `[JDS_BI_ANIMATION_OPTIONS]: Options for JDS BI animations`,
  {
    factory: () => ({
      params: {
        duration: inject(JDS_BI_ANIMATIONS_DURATION),
      },
    }),
  },
);
