import {
  animate,
  query,
  stagger,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { JdsBiDropdownAnimation } from '@jds-bi/core/enums';

const TRANSITION = `{{duration}}ms ease-in-out`;
const DURATION = { params: { duration: 300 } };
const STAGGER = 300;

export interface JdsBiDurationOptions {
  value: string;
  params: { duration: number };
}

export const jdsBiHeightCollapse = trigger(`jdsBiHeightCollapse`, [
  transition(
    `:enter`,
    [style({ height: 0 }), animate(TRANSITION, style({ height: `*` }))],
    DURATION,
  ),
  transition(
    `:leave`,
    [style({ height: `*` }), animate(TRANSITION, style({ height: 0 }))],
    DURATION,
  ),
]);

export const jdsBiHeightCollapseList = trigger(`jdsBiHeightCollapseList`, [
  transition(
    `* => *`,
    [
      query(
        `:enter`,
        [
          style({ height: 0 }),
          stagger(STAGGER, [animate(TRANSITION, style({ height: `*` }))]),
        ],
        {
          optional: true,
        },
      ),
      query(
        `:leave`,
        [
          style({ height: `*` }),
          stagger(STAGGER, [animate(TRANSITION, style({ height: 0 }))]),
        ],
        {
          optional: true,
        },
      ),
    ],
    DURATION,
  ),
]);

export const jdsBiWidthCollapse = trigger(`jdsBiWidthCollapse`, [
  transition(
    `:enter`,
    [style({ width: 0 }), animate(TRANSITION, style({ width: `*` }))],
    DURATION,
  ),
  transition(
    `:leave`,
    [style({ width: `*` }), animate(TRANSITION, style({ width: 0 }))],
    DURATION,
  ),
]);

export const jdsBiWidthCollapseList = trigger(`jdsBiWidthCollapseList`, [
  transition(
    `* => *`,
    [
      query(
        `:enter`,
        [
          style({ width: 0 }),
          stagger(STAGGER, [animate(TRANSITION, style({ width: `*` }))]),
        ],
        {
          optional: true,
        },
      ),
      query(
        `:leave`,
        [
          style({ width: `*` }),
          stagger(STAGGER, [animate(TRANSITION, style({ width: 0 }))]),
        ],
        {
          optional: true,
        },
      ),
    ],
    DURATION,
  ),
]);

export const jdsBiFadeIn = trigger(`jdsBiFadeIn`, [
  transition(
    `:enter`,
    [style({ opacity: 0 }), animate(TRANSITION, style({ opacity: 1 }))],
    DURATION,
  ),
  transition(
    `:leave`,
    [style({ opacity: 1 }), animate(TRANSITION, style({ opacity: 0 }))],
    DURATION,
  ),
]);

export const jdsBiFadeInList = trigger(`jdsBiFadeInList`, [
  transition(
    `* => *`,
    [
      query(
        `:enter`,
        [
          style({ opacity: 0 }),
          stagger(STAGGER, [animate(TRANSITION, style({ opacity: 1 }))]),
        ],
        {
          optional: true,
        },
      ),
      query(
        `:leave`,
        [
          style({ opacity: 1 }),
          stagger(STAGGER, [animate(TRANSITION, style({ opacity: 0 }))]),
        ],
        {
          optional: true,
        },
      ),
    ],
    DURATION,
  ),
]);

export const jdsBiFadeInTop = trigger(`jdsBiFadeInTop`, [
  transition(
    `:enter`,
    [
      style({ transform: `translateY(-10px)`, opacity: 0 }),
      animate(TRANSITION, style({ transform: `translateY(0)`, opacity: 1 })),
    ],
    DURATION,
  ),
  transition(
    `:leave`,
    [
      style({ transform: `translateY(0)`, opacity: 1 }),
      animate(
        TRANSITION,
        style({ transform: `translateY(-10px)`, opacity: 0 }),
      ),
    ],
    DURATION,
  ),
]);

export const jdsBiFadeInBottom = trigger(`jdsBiFadeInBottom`, [
  transition(
    `:enter`,
    [
      style({ transform: `translateY(10px)`, opacity: 0 }),
      animate(TRANSITION, style({ transform: `translateY(0)`, opacity: 1 })),
    ],
    DURATION,
  ),
  transition(
    `:leave`,
    [
      style({ transform: `translateY(0)`, opacity: 1 }),
      animate(TRANSITION, style({ transform: `translateY(10px)`, opacity: 0 })),
    ],
    DURATION,
  ),
]);

export const jdsBiDropdownAnimation = trigger(`jdsBiDropdownAnimation`, [
  transition(
    `* => ${JdsBiDropdownAnimation.FadeInTop}`,
    [
      style({ transform: `translateY(-10px)`, opacity: 0 }),
      animate(TRANSITION, style({ transform: `translateY(0)`, opacity: 1 })),
    ],
    DURATION,
  ),
  transition(
    `* => ${JdsBiDropdownAnimation.FadeInBottom}`,
    [
      style({ transform: `translateY(10px)`, opacity: 0 }),
      animate(TRANSITION, style({ transform: `translateY(0)`, opacity: 1 })),
    ],
    DURATION,
  ),
  transition(
    `${JdsBiDropdownAnimation.FadeInBottom} => *`,
    [
      style({ transform: `translateY(0)`, opacity: 1 }),
      animate(TRANSITION, style({ transform: `translateY(10px)`, opacity: 0 })),
    ],
    DURATION,
  ),
  transition(
    `${JdsBiDropdownAnimation.FadeInTop} => *`,
    [
      style({ transform: `translateY(0)`, opacity: 1 }),
      animate(
        TRANSITION,
        style({ transform: `translateY(-10px)`, opacity: 0 }),
      ),
    ],
    DURATION,
  ),
]);

export const jdsBiScaleIn = trigger(`jdsBiScaleIn`, [
  transition(
    `:enter`,
    [
      style({ transform: `scale(0)` }),
      animate(TRANSITION, style({ transform: `scale(1)` })),
    ],
    DURATION,
  ),
  transition(
    `:leave`,
    [
      style({ transform: `scale(1)` }),
      animate(TRANSITION, style({ transform: `scale(0)` })),
    ],
    DURATION,
  ),
]);

export const jdsBiScaleInList = trigger(`jdsBiScaleInList`, [
  transition(
    `* => *`,
    [
      query(
        `:enter`,
        [
          style({ transform: `scale(0)` }),
          stagger(STAGGER, [
            animate(TRANSITION, style({ transform: `scale(1)` })),
          ]),
        ],
        { optional: true },
      ),
      query(
        `:leave`,
        [
          style({ transform: `scale(1)` }),
          stagger(STAGGER, [
            animate(TRANSITION, style({ transform: `scale(0)` })),
          ]),
        ],
        { optional: true },
      ),
    ],
    DURATION,
  ),
]);

export const jdsBiSlideIn = trigger(`jdsBiSlideIn`, [
  transition(
    `* => left`,
    [
      style({ transform: `translateX(-100%)` }),
      animate(TRANSITION, style({ transform: `translateX(0)` })),
    ],
    DURATION,
  ),
  transition(
    `left => *`,
    [
      style({ transform: `translateX(0)` }),
      animate(TRANSITION, style({ transform: `translateX(-100%)` })),
    ],
    DURATION,
  ),
  transition(
    `* => right`,
    [
      style({ transform: `translateX(100%)` }),
      animate(TRANSITION, style({ transform: `translateX(0)` })),
    ],
    DURATION,
  ),
  transition(
    `right => *`,
    [
      style({ transform: `translateX(0)` }),
      animate(TRANSITION, style({ transform: `translateX(100%)` })),
    ],
    DURATION,
  ),
]);

export const jdsBiSlideInLeft = trigger(`jdsBiSlideInLeft`, [
  transition(
    `:enter`,
    [
      style({ transform: `translateX(-100%)` }),
      animate(TRANSITION, style({ transform: `translateX(0)` })),
    ],
    DURATION,
  ),
  transition(
    `:leave`,
    [
      style({ transform: `translateX(0)` }),
      animate(TRANSITION, style({ transform: `translateX(-100%)` })),
    ],
    DURATION,
  ),
]);

export const jdsBiSlideInLeftList = trigger(`jdsBiSlideInLeftList`, [
  transition(
    `* => *`,
    [
      query(
        `:enter`,
        [
          style({ transform: `translateX(-100%)` }),
          stagger(STAGGER, [
            animate(TRANSITION, style({ transform: `translateX(0)` })),
          ]),
        ],
        { optional: true },
      ),
      query(
        `:leave`,
        [
          style({ transform: `translateX(0)` }),
          stagger(STAGGER, [
            animate(TRANSITION, style({ transform: `translateX(-100%)` })),
          ]),
        ],
        { optional: true },
      ),
    ],
    DURATION,
  ),
]);

export const jdsBiSlideInRight = trigger(`jdsBiSlideInRight`, [
  transition(
    `:enter`,
    [
      style({ transform: `translateX(100%)` }),
      animate(TRANSITION, style({ transform: `translateX(0)` })),
    ],
    DURATION,
  ),
  transition(
    `:leave`,
    [
      style({ transform: `translateX(0)` }),
      animate(TRANSITION, style({ transform: `translateX(100%)` })),
    ],
    DURATION,
  ),
]);

export const jdsBiSlideInRightList = trigger(`jdsBiSlideInRightList`, [
  transition(
    `* => *`,
    [
      query(
        `:enter`,
        [
          style({ transform: `translateX(100%)` }),
          stagger(STAGGER, [
            animate(TRANSITION, style({ transform: `translateX(0)` })),
          ]),
        ],
        { optional: true },
      ),
      query(
        `:leave`,
        [
          style({ transform: `translateX(0)` }),
          stagger(STAGGER, [
            animate(TRANSITION, style({ transform: `translateX(100%)` })),
          ]),
        ],
        { optional: true },
      ),
    ],
    DURATION,
  ),
]);

export const jdsBiSlideInTop = trigger(`jdsBiSlideInTop`, [
  transition(
    `:enter`,
    [
      style({ transform: `translate3d(0,{{start}},0)` }),
      animate(TRANSITION, style({ transform: `translate3d(0,{{end}},0)` })),
    ],
    { params: { end: 0, start: `100%`, duration: 300 } },
  ),
  transition(
    `:leave`,
    [
      style({ transform: `translate3d(0,{{end}},0)` }),
      animate(TRANSITION, style({ transform: `translate3d(0,{{start}},0)` })),
    ],
    { params: { end: 0, start: `100%`, duration: 300 } },
  ),
]);

export const jdsBiSlideInTopList = trigger(`jdsBiSlideInTopList`, [
  transition(
    `* => *`,
    [
      query(
        `:enter`,
        [
          style({ transform: `translateY(100%)` }),
          stagger(STAGGER, [
            animate(TRANSITION, style({ transform: `translateY(0)` })),
          ]),
        ],
        { optional: true },
      ),
      query(
        `:leave`,
        [
          style({ transform: `translateY(0)` }),
          stagger(STAGGER, [
            animate(TRANSITION, style({ transform: `translateY(100%)` })),
          ]),
        ],
        { optional: true },
      ),
    ],
    DURATION,
  ),
]);

export const jdsBiSlideInBottom = trigger(`jdsBiSlideInBottom`, [
  transition(
    `:enter`,
    [
      style({ transform: `translateY(-100%)` }),
      animate(TRANSITION, style({ transform: `translateY(0)` })),
    ],
    DURATION,
  ),
  transition(
    `:leave`,
    [
      style({ transform: `translateY(0)` }),
      animate(TRANSITION, style({ transform: `translateY(-100%)` })),
    ],
    DURATION,
  ),
]);

export const jdsBiSlideInBottomList = trigger(`jdsBiSlideInBottomList`, [
  transition(
    `* => *`,
    [
      query(
        `:enter`,
        [
          style({ transform: `translateY(-100%)` }),
          stagger(STAGGER, [
            animate(TRANSITION, style({ transform: `translateY(0)` })),
          ]),
        ],
        { optional: true },
      ),
      query(
        `:leave`,
        [
          style({ transform: `translateY(0)` }),
          stagger(STAGGER, [
            animate(TRANSITION, style({ transform: `translateY(-100%)` })),
          ]),
        ],
        { optional: true },
      ),
    ],
    DURATION,
  ),
]);
