export const jdsBiEditingKeys: readonly string[] = [
  `Spacebar`,
  `Backspace`,
  `Delete`,
  `ArrowLeft`,
  `ArrowRight`,
  `Left`,
  `Right`,
  `End`,
  `Home`,
];
