/**
 * An event indicating that async data for expand has finished loading.
 * Dispatch to finish loading states for {@link JdsBiExpandComponent}.
 */
export const JDS_BI_EXPAND_LOADED = `jds-bi-expand-loaded`;

/**
 * An event for scrolling an element into view within {@link JdsBiScrollbarComponent}.
 */
export const JDS_BI_SCROLL_INTO_VIEW = `jds-bi-scroll-into-view`;

/**
 * An event to notify {@link JdsBiScrollbarComponent} that
 * it should control a nested element.
 */
export const JDS_BI_SCROLLABLE = `jds-bi-scrollable`;

/**
 * An event indicating and error during icon loading in {@link JdsBiSvgComponent}.
 */
export const JDS_BI_ICON_ERROR = `jds-bi-icon-error`;
