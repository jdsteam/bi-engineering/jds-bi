import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { JDS_BI_PARENT_ANIMATION } from '@jds-bi/cdk';
import { JdsBiPortalItem } from '@jds-bi/core/interfaces';
import { JdsBiHintService } from '@jds-bi/core/services';
import { Observable } from 'rxjs';

@Component({
  selector: 'jds-bi-hints-host',
  templateUrl: './hints-host.template.html',
  styleUrls: ['./hints-host.style.scss'],
  // So that we do not force OnPush on custom hints
  // eslint-disable-next-line @angular-eslint/prefer-on-push-component-change-detection
  changeDetection: ChangeDetectionStrategy.Default,
  animations: [JDS_BI_PARENT_ANIMATION],
  // eslint-disable-next-line @angular-eslint/no-host-metadata-property
  host: {
    'aria-live': 'polite',
  },
})
export class JdsBiHintsHostComponent {
  constructor(
    @Inject(JdsBiHintService)
    readonly hints$: Observable<readonly JdsBiPortalItem[]>,
  ) {}
}
