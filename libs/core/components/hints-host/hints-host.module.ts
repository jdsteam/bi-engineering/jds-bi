import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { JdsBiActiveZoneModule } from '@jds-bi/cdk';
import { PolymorpheusModule } from '@tinkoff/ng-polymorpheus';

import { JdsBiHintsHostComponent } from './hints-host.component';

@NgModule({
  imports: [CommonModule, PolymorpheusModule, JdsBiActiveZoneModule],
  declarations: [JdsBiHintsHostComponent],
  exports: [JdsBiHintsHostComponent],
})
export class JdsBiHintsHostModule {}
