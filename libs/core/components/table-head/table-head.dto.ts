export interface JdsBiTableHeadInputs {
  colorScheme: string;
  colorCustom: boolean;
}

export interface JdsBiTableHeadStyles {
  tableHead: string;
}
