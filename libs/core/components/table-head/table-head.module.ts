import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { JdsBiTableHeadComponent } from './table-head.component';

@NgModule({
  declarations: [JdsBiTableHeadComponent],
  imports: [CommonModule],
  exports: [JdsBiTableHeadComponent],
})
export class JdsBiTableHeadModule {}
