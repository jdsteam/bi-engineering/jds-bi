import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type {
  JdsBiTableHeadInputs,
  JdsBiTableHeadStyles,
} from './table-head.dto';
import { JdsBiTableHeadStyle } from './table-head.style';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: '[jdsBiTableHead]',
  templateUrl: './table-head.component.html',
  styleUrls: ['./table-head.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [JdsBiTableHeadStyle],
})
export class JdsBiTableHeadComponent implements OnInit, OnChanges {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor | string = 'green';

  @Input()
  @jdsBiDefaultProp()
  colorCustom = false;

  @Input()
  @jdsBiDefaultProp()
  column = '';

  @Input()
  @jdsBiDefaultProp()
  currentSort: any = {};

  @Input()
  @jdsBiDefaultProp()
  isSortable = false;

  @Input()
  @jdsBiDefaultProp()
  isFilterable = false;

  @Output() filterSort = new EventEmitter();

  // Variable
  className!: JdsBiTableHeadStyles;

  constructor(
    private _elRef: ElementRef,
    private jdsBiTableHeadStyle: JdsBiTableHeadStyle,
  ) {}

  ngOnInit(): void {
    this.getClassnames({
      colorScheme: this.colorScheme,
      colorCustom: this.colorCustom,
    });
  }

  ngOnChanges(): void {
    this.getClassnames({
      colorScheme: this.colorScheme,
      colorCustom: this.colorCustom,
    });
  }

  getClassnames(input: JdsBiTableHeadInputs): void {
    if (this.className) {
      this._elRef.nativeElement.classList.remove(this.className.tableHead);
    }

    this.className = this.jdsBiTableHeadStyle.getStyle(input);
    this._elRef.nativeElement.classList.add(this.className.tableHead);
  }

  sortClassname(column: string): string {
    if (this.currentSort.sort !== column) {
      return '';
    }
    return this.currentSort.direction;
  }

  changeSort(event: string): void {
    this.filterSort.emit(event);
  }
}
