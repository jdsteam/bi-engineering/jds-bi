import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import {
  JdsBiPaginationInputs,
  JdsBiPaginationItems,
  JdsBiPaginationStyles,
} from './pagination.dto';
import { JdsBiPaginationStyle } from './pagination.style';

import { isEmpty, isNil } from 'lodash';

@Component({
  selector: 'jds-bi-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [JdsBiPaginationStyle],
})
export class JdsBiPaginationComponent implements OnInit, OnChanges {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  perPageItems: JdsBiPaginationItems[] = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];

  @Input()
  @jdsBiDefaultProp()
  pageItems: JdsBiPaginationItems[] = [];

  @Input()
  @jdsBiDefaultProp()
  perPage = 10;

  @Input()
  @jdsBiDefaultProp()
  page = 1;

  @Input()
  get pagination(): any {
    return this._pagination;
  }
  set pagination(pagination: any) {
    this._pagination = pagination;

    const totalPages = isNil(pagination) ? 0 : pagination.total_pages;
    const options: JdsBiPaginationItems[] = [];
    for (let index = 1; index <= totalPages; index += 1) {
      options.push({ value: index, label: index.toString() });
    }
    this.pageItems = options;
  }

  @Output() filterPerPage = new EventEmitter();
  @Output() filterPage = new EventEmitter();

  private _pagination = { empty: true };

  // Variable
  className!: JdsBiPaginationStyles;

  constructor(
    private _elRef: ElementRef,
    private jdsBiPaginationStyle: JdsBiPaginationStyle,
  ) {}

  ngOnInit(): void {
    this.getClassnames({
      colorScheme: this.colorScheme,
    });
  }

  ngOnChanges(): void {
    this.getClassnames({
      colorScheme: this.colorScheme,
    });
  }

  getClassnames(input: JdsBiPaginationInputs): void {
    if (this.className) {
      this._elRef.nativeElement.classList.remove(this.className.pagination);
    }

    this.className = this.jdsBiPaginationStyle.getStyle(input);
    this._elRef.nativeElement.classList.add(this.className.pagination);
  }

  changePerPage(event: JdsBiPaginationItems): void {
    const value = isEmpty(event) === false ? event.value : null;
    this.filterPerPage.emit(value);
  }

  changePage(event: JdsBiPaginationItems): void {
    const value = isEmpty(event) === false ? event.value : null;
    this.filterPage.emit(value);
  }
}
