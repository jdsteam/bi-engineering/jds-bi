import { Injectable } from '@angular/core';
import { jdsBiColorScheme } from '@jds-bi/cdk';
import { css } from '@emotion/css';

import type {
  JdsBiPaginationInputs,
  JdsBiPaginationStyles,
} from './pagination.dto';

@Injectable()
export class JdsBiPaginationStyle {
  getStyle(inputs: JdsBiPaginationInputs): JdsBiPaginationStyles {
    const colorScheme = jdsBiColorScheme(inputs.colorScheme);
    const { primary, hexSVG } = colorScheme;

    const down = `url("data:image/svg+xml;utf8, <svg width='16' height='16' viewBox='0 0 512 512' xmlns='http://www.w3.org/2000/svg'><path fill='${hexSVG}' d='M233.4 406.6c12.5 12.5 32.8 12.5 45.3 0l192-192c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L256 338.7 86.6 169.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l192 192z'/></svg>")`;
    const up = `url("data:image/svg+xml;utf8, <svg width='16' height='16' viewBox='0 0 512 512' xmlns='http://www.w3.org/2000/svg'><path fill='${hexSVG}' d='M233.4 105.4c12.5-12.5 32.8-12.5 45.3 0l192 192c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L256 173.3 86.6 342.6c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3l192-192z'/></svg>")`;

    const pagination = css`
      .t-pagination {
        border-top-color: ${primary};

        .pgntn-navigation {
          .pgntn-previous button,
          .pgntn-next button {
            color: ${primary};

            &:focus {
              box-shadow: 0 0 0 1px ${primary}, inset 0 0 0 1px #ffc800;
            }
          }
        }

        .pgntn-select {
          .ng-select-container {
            .ng-arrow-wrapper {
              .ng-arrow {
                &::before {
                  background-image: ${down};
                }
              }
            }
          }

          &.ng-select-opened {
            .ng-select-container {
              .ng-arrow-wrapper {
                .ng-arrow {
                  &::before {
                    background-image: ${up};
                  }
                }
              }
            }
          }

          &.ng-select-focused:not(.ng-select-opened) > .ng-select-container {
            border-color: ${primary};
          }
        }
      }
    `;

    return { pagination };
  }
}
