import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { JdsBiPaginationComponent } from './pagination.component';

@NgModule({
  declarations: [JdsBiPaginationComponent],
  imports: [CommonModule, FormsModule, NgSelectModule],
  exports: [JdsBiPaginationComponent],
})
export class JdsBiPaginationModule {}
