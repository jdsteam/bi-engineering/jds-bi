export interface JdsBiPaginationItems {
  value: number | string;
  label?: string;
  checked?: boolean;
  selected?: boolean;
}

export interface JdsBiPaginationInputs {
  colorScheme: string;
}

export interface JdsBiPaginationStyles {
  pagination: string;
}
