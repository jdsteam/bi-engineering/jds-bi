import {
  AfterContentChecked,
  ContentChildren,
  Directive,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Inject,
  Input,
  OnInit,
  Output,
  QueryList,
} from '@angular/core';

import { jdsBiIsDefined } from '@jds-bi/core/utils/miscellaneous';

import { JdsBiNavDirective } from './nav.directive';
import { JdsBiNavContentDirective } from './nav-content.directive';
import { JDS_BI_NAV_DIRECTIVE } from './nav.providers';

const isValidNavId = (id: any) => jdsBiIsDefined(id) && id !== '';
let navCounter = 0;

@Directive({
  selector: '[jdsBiNavItem]',
  exportAs: 'jdsBiNavItem',
  providers: [
    {
      provide: JDS_BI_NAV_DIRECTIVE,
      useExisting: forwardRef(() => JdsBiNavDirective),
    },
  ],
})
export class JdsBiNavItemDirective implements AfterContentChecked, OnInit {
  @Input() destroyOnHide?: boolean;
  @Input() disabled = false;
  @Input() domId?: string;
  @Input('jdsBiNavItem') _id: any;
  @Output() shown = new EventEmitter<void>();
  @Output() hidden = new EventEmitter<void>();

  contentTpl!: JdsBiNavContentDirective | null;

  @ContentChildren(JdsBiNavContentDirective, { descendants: false })
  contentTpls!: QueryList<JdsBiNavContentDirective>;

  constructor(
    @Inject(JDS_BI_NAV_DIRECTIVE) private _nav: any,
    public elementRef: ElementRef<any>,
  ) {}

  @HostBinding('class.t-nav-item')
  get hostNavItem() {
    return true;
  }

  ngAfterContentChecked() {
    this.contentTpl = this.contentTpls.first;
  }

  ngOnInit() {
    if (!jdsBiIsDefined(this.domId)) {
      // eslint-disable-next-line no-plusplus
      this.domId = `jds-bi-nav-${navCounter++}`;
    }
  }

  get active() {
    return this._nav.activeId === this.id;
  }

  get id() {
    return isValidNavId(this._id) ? this._id : this.domId;
  }

  get panelDomId() {
    return `${this.domId}-panel`;
  }

  isPanelInDom() {
    return (
      (jdsBiIsDefined(this.destroyOnHide)
        ? !this.destroyOnHide
        : !this._nav.destroyOnHide) || this.active
    );
  }
}
