import { InjectionToken, Type } from '@angular/core';

// TODO: find the best way for prevent cycle
import { JdsBiNavDirective } from './nav.directive';
import { JdsBiNavItemDirective } from './nav-item.directive';

export const JDS_BI_NAV_DIRECTIVE = new InjectionToken<Type<any>>(
  `[JDS_BI_NAV_DIRECTIVE] A directive to display a nav`,
  {
    factory: () => JdsBiNavDirective,
  },
);

export const JDS_BI_NAV_ITEM_DIRECTIVE = new InjectionToken<Type<any>>(
  `[JDS_BI_NAV_ITEM_DIRECTIVE] A directive to display a nav item`,
  {
    factory: () => JdsBiNavItemDirective,
  },
);
