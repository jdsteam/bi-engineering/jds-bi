import {
  Attribute,
  Directive,
  ElementRef,
  forwardRef,
  HostBinding,
  HostListener,
  Inject,
} from '@angular/core';

import { JdsBiNavDirective } from './nav.directive';
import { JdsBiNavItemDirective } from './nav-item.directive';
import { JDS_BI_NAV_DIRECTIVE } from './nav.providers';

@Directive({
  selector: 'a[jdsBiNavLink]',
  providers: [
    {
      provide: JDS_BI_NAV_DIRECTIVE,
      useExisting: forwardRef(() => JdsBiNavDirective),
    },
  ],
})
export class JdsBiNavLinkDirective {
  constructor(
    @Attribute('role') public role: string,
    public navItem: JdsBiNavItemDirective,
    @Inject(JDS_BI_NAV_DIRECTIVE) public nav: any,
    public elRef: ElementRef,
  ) {}

  @HostBinding('id') get hostOrientation() {
    return this.navItem.domId;
  }
  @HostBinding('class.t-nav-link')
  get hostNavLink() {
    return true;
  }
  @HostBinding('class.t-nav-item')
  get hostNavItem() {
    return this.hasNavItemClass();
  }
  @HostBinding('attr.role')
  get hostRole() {
    if (this.role) {
      return this.role;
    }

    return this.nav.roles ? 'tab' : undefined;
  }
  @HostBinding('href') get hostHref() {
    return '';
  }
  @HostBinding('class.active') get hostActive() {
    return this.navItem.active;
  }
  @HostBinding('class.disabled') get hostDisabled() {
    return this.navItem.disabled;
  }
  @HostBinding('attr.tabindex') get hostTabIndex() {
    return this.navItem.disabled ? -1 : undefined;
  }
  @HostBinding('attr.aria-controls') get hostAriaControls() {
    return this.navItem.isPanelInDom() ? this.navItem.panelDomId : null;
  }
  @HostBinding('attr.aria-selected') get hostAriaSelected() {
    return this.navItem.active;
  }
  @HostBinding('attr.aria-disabled') get hostAriaDisabled() {
    return this.navItem.disabled;
  }

  @HostListener('click', ['$event']) onClick(event: any) {
    this.nav.click(this.navItem);
    event.preventDefault();
  }

  hasNavItemClass() {
    return this.navItem.elementRef.nativeElement.nodeType === Node.COMMENT_NODE;
  }
}
