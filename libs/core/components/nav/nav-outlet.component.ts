import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  NgZone,
  QueryList,
  ViewChildren,
  ViewEncapsulation,
} from '@angular/core';
import {
  distinctUntilChanged,
  skip,
  startWith,
  takeUntil,
} from 'rxjs/operators';

import { JdsBiNavDirective } from './nav.directive';
import { JdsBiNavItemDirective } from './nav-item.directive';
import { JdsBiNavPaneDirective } from './nav-pane.directive';

import { NavFadeInTransition, NavFadeOutTransition } from './nav-transition';
import { RunTransition, TransitionOptions } from '@jds-bi/core/utils/dom';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: '[jdsBiNavOutlet]',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ng-template
      [ngForOf]="nav.items"
      ngFor
      let-item
    >
      <div
        *ngIf="item.isPanelInDom() || isPanelTransitioning(item)"
        [item]="item"
        [nav]="nav"
        [role]="paneRole"
        jdsBiNavPane
      >
        <ng-template
          [ngTemplateOutlet]="item.contentTpl?.templateRef || null"
          [ngTemplateOutletContext]="{
            $implicit: item.active || isPanelTransitioning(item)
          }"
        ></ng-template>
      </div>
    </ng-template>
  `,
})
export class JdsBiNavOutletComponent implements AfterViewInit {
  private _activePane: JdsBiNavPaneDirective | null = null;

  @ViewChildren(JdsBiNavPaneDirective)
  private _panes!: QueryList<JdsBiNavPaneDirective>;

  @Input() paneRole!: string;
  @Input('jdsBiNavOutlet') nav!: JdsBiNavDirective;

  constructor(private _cd: ChangeDetectorRef, private _ngZone: NgZone) {}

  @HostBinding('class.t-tab-content')
  get hostTabContent() {
    return true;
  }

  isPanelTransitioning(item: JdsBiNavItemDirective) {
    return this._activePane?.item === item;
  }

  ngAfterViewInit() {
    // initial display
    this._updateActivePane();

    // this will be emitted for all 3 types of nav changes: .select(), [activeId] or (click)
    this.nav.navItemChange$
      .pipe(
        takeUntil(this.nav.destroy$),
        startWith(this._activePane?.item || null),
        distinctUntilChanged(),
        skip(1),
      )
      .subscribe((nextItem) => {
        const options: TransitionOptions<undefined> = {
          animation: true,
          runningTransition: 'stop',
        };

        this._cd.detectChanges();

        // fading out
        if (this._activePane) {
          RunTransition(
            this._ngZone,
            this._activePane.elRef.nativeElement,
            NavFadeOutTransition,
            options,
          ).subscribe(() => {
            const activeItem = this._activePane?.item;
            this._activePane = this._getPaneForItem(nextItem);

            this._cd.markForCheck();

            // fading in
            if (this._activePane) {
              this._activePane.elRef.nativeElement.classList.add('active');
              RunTransition(
                this._ngZone,
                this._activePane.elRef.nativeElement,
                NavFadeInTransition,
                options,
              ).subscribe(() => {
                if (nextItem) {
                  nextItem.shown.emit();
                  this.nav.shown.emit(nextItem.id);
                }
              });
            }

            if (activeItem) {
              activeItem.hidden.emit();
              this.nav.hidden.emit(activeItem.id);
            }
          });
        } else {
          this._updateActivePane();
        }
      });
  }

  private _updateActivePane() {
    this._activePane = this._getActivePane();
    this._activePane?.elRef.nativeElement.classList.add('show');
    this._activePane?.elRef.nativeElement.classList.add('active');
  }

  private _getPaneForItem(item: JdsBiNavItemDirective | null) {
    return (
      (this._panes && this._panes.find((pane) => pane.item === item)) || null
    );
  }

  private _getActivePane(): JdsBiNavPaneDirective | null {
    return (
      (this._panes && this._panes.find((pane) => pane.item.active)) || null
    );
  }
}
