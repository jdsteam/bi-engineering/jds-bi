/* eslint-disable @typescript-eslint/brace-style */
import {
  AfterContentInit,
  Attribute,
  ChangeDetectorRef,
  ContentChildren,
  Directive,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import type { JdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiActiveZoneDirective,
  JdsBiContextWithImplicit,
  jdsBiDefaultProp,
} from '@jds-bi/cdk';
import { PolymorpheusContent } from '@tinkoff/ng-polymorpheus';

import { jdsBiIsDefined } from '@jds-bi/core/utils';

import {
  JdsBiNavChangeEvent,
  JdsBiNavInputs,
  JdsBiNavOrientation,
  JdsBiNavStyles,
  JdsBiNavVariant,
} from './nav.dto';
import { JdsBiNavItemDirective } from './nav-item.directive';
import { JdsBiNavLinkDirective } from './nav-link.directive';
import { JdsBiNavStyle } from './nav.style';

const isValidNavId = (id: any) => jdsBiIsDefined(id) && id !== '';

@Directive({
  selector: '[jdsBiNav]',
  exportAs: 'jdsBiNav',
  providers: [JdsBiNavStyle],
})
export class JdsBiNavDirective
  implements OnInit, AfterContentInit, OnChanges, OnDestroy
{
  static ngAcceptInputType_orientation: string;
  static ngAcceptInputType_roles: boolean | string;

  @Input() activeId: any;

  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiNavVariant = 'outline';

  @Input()
  @jdsBiDefaultProp()
  orientation: JdsBiNavOrientation = 'horizontal';

  @Input()
  @jdsBiDefaultProp()
  destroyOnHide = true;

  @Input()
  @jdsBiDefaultProp()
  roles: 'tablist' | false = 'tablist';

  @Input('jdsBiNav')
  @jdsBiDefaultProp()
  content: PolymorpheusContent<
    JdsBiContextWithImplicit<JdsBiActiveZoneDirective>
  > = '';

  @Output() activeIdChange = new EventEmitter<any>();
  @Output() shown = new EventEmitter<any>();
  @Output() hidden = new EventEmitter<any>();
  @Output() navChange = new EventEmitter<JdsBiNavChangeEvent>();

  @ContentChildren(forwardRef(() => JdsBiNavItemDirective), {
    descendants: true,
  })
  items!: QueryList<JdsBiNavItemDirective>;
  @ContentChildren(forwardRef(() => JdsBiNavLinkDirective), {
    descendants: true,
  })
  links!: QueryList<JdsBiNavLinkDirective>;

  destroy$ = new Subject<void>();
  navItemChange$ = new Subject<JdsBiNavItemDirective | null>();

  // Variable
  className!: JdsBiNavStyles;

  constructor(
    @Attribute('role') public role: string,
    private _cd: ChangeDetectorRef,
    private _elRef: ElementRef,
    private jdsBiNavStyle: JdsBiNavStyle,
  ) {}

  @HostBinding('class.t-nav')
  get hostNav() {
    return true;
  }

  @HostBinding('attr.aria-orientation')
  get hostOrientation() {
    return this.orientation === 'vertical' && this.roles === 'tablist'
      ? 'vertical'
      : undefined;
  }

  @HostBinding('attr.role') get hostRole() {
    if (this.role) {
      return this.role;
    }

    return this.roles ? 'tablist' : undefined;
  }

  ngOnInit(): void {
    this.getClassnames({
      colorScheme: this.colorScheme,
      variant: this.variant,
    });
  }

  click(item: JdsBiNavItemDirective) {
    if (!item.disabled) {
      this._updateActiveId(item.id);
    }
  }

  select(id: any) {
    this._updateActiveId(id, false);
  }

  ngAfterContentInit() {
    if (!jdsBiIsDefined(this.activeId)) {
      const nextId = this.items.first ? this.items.first.id : null;
      if (isValidNavId(nextId)) {
        this._updateActiveId(nextId, false);
        this._cd.detectChanges();
      }
    }

    this.items.changes
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this._notifyItemChanged(this.activeId));
  }

  ngOnChanges({ activeId }: SimpleChanges): void {
    if (activeId && !activeId.firstChange) {
      this._notifyItemChanged(activeId.currentValue);
    }

    if (!this.content) {
      this.getClassnames({
        colorScheme: this.colorScheme,
        variant: this.variant,
      });
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  getClassnames(input: JdsBiNavInputs): void {
    if (this.className) {
      this._elRef.nativeElement.classList.remove(this.className.nav);
    }

    this.className = this.jdsBiNavStyle.getStyle(input);
    this._elRef.nativeElement.classList.add(this.className.nav);
  }

  private _updateActiveId(nextId: any, emitNavChange = true) {
    if (this.activeId !== nextId) {
      let defaultPrevented = false;

      if (emitNavChange) {
        this.navChange.emit({
          activeId: this.activeId,
          nextId,
          preventDefault: () => {
            defaultPrevented = true;
          },
        });
      }

      if (!defaultPrevented) {
        this.activeId = nextId;
        this.activeIdChange.emit(nextId);
        this._notifyItemChanged(nextId);
      }
    }
  }

  private _notifyItemChanged(nextItemId: any) {
    this.navItemChange$.next(this._getItemById(nextItemId));
  }

  private _getItemById(itemId: any): JdsBiNavItemDirective | null {
    return (
      (this.items && this.items.find((item) => item.id === itemId)) || null
    );
  }
}
