import { Injectable } from '@angular/core';
import { jdsBiColor, jdsBiColorScheme } from '@jds-bi/cdk';
import { css } from '@emotion/css';

import type {
  JdsBiNavInputs,
  JdsBiNavPaneStyles,
  JdsBiNavStyles,
} from './nav.dto';

@Injectable()
export class JdsBiNavStyle {
  getStyle(inputs: JdsBiNavInputs): JdsBiNavStyles {
    const colorScheme = jdsBiColorScheme(inputs.colorScheme);
    const { color: schemeColor } = colorScheme;

    const gray500 = jdsBiColor.gray['500'];

    let color: string;
    let backgroundColor: string;
    let borderColor: string;
    let borderBottom: string;

    switch (inputs.variant) {
      case 'outline':
        color = schemeColor['700'];
        backgroundColor = '#FFF';
        borderColor = schemeColor['700'];
        borderBottom = `2px solid ${borderColor}`;
        break;
      case 'ghost':
        color = schemeColor['700'];
        backgroundColor = 'transparent';
        borderColor = schemeColor['700'];
        borderBottom = `2px solid ${gray500}`;
        break;
      default:
        color = schemeColor['700'];
        backgroundColor = '#FFF';
        borderColor = schemeColor['700'];
        borderBottom = `2px solid ${borderColor}`;
        break;
    }

    const nav = css`
      &.t-nav {
        display: flex;
        flex-wrap: wrap;
        padding-left: 0;
        margin-top: 0;
        margin-bottom: 0;
        list-style: none;
        border-bottom: ${borderBottom};

        .t-nav-item {
          margin-left: 8px;

          .t-nav-link {
            display: block;
            background: none;
            font-size: 16px;
            font-weight: 500;
            line-height: 26px;
            padding: 8px 14px;
            color: ${gray500};
            text-decoration: none;
            border: 2px solid transparent;
            border-top-left-radius: 8px;
            border-top-right-radius: 8px;
            margin-bottom: -2px;

            ${inputs.variant === 'outline' &&
            css`
              border-bottom: 0;
            `}

            ${inputs.variant === 'ghost' &&
            css`
              border-top: 0;
              border-left: 0;
              border-right: 0;
            `}

            &.active {
              font-weight: 700;
              color: ${color};
              background-color: ${backgroundColor};
              border-color: ${borderColor};
            }
          }
        }
      }
    `;

    return { nav };
  }

  getStylePane(): JdsBiNavPaneStyles {
    const pane = css`
      &.t-tab-pane {
        display: none;

        &.active {
          display: block;
        }

        &.fade {
          transition: opacity 0.15s linear;
        }
      }
    `;

    return { pane };
  }
}
