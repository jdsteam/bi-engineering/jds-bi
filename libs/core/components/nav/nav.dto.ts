export interface JdsBiNavContentContext {
  $implicit: boolean;
}

export interface JdsBiNavChangeEvent<T = any> {
  activeId: T;
  nextId: T;
  preventDefault: () => void;
}

export const JdsBiNavVariant = ['outline', 'ghost'] as const;

export type JdsBiNavVariant = typeof JdsBiNavVariant[number];

export const JdsBiNavOrientation = ['horizontal', 'vertical'] as const;

export type JdsBiNavOrientation = typeof JdsBiNavOrientation[number];

export interface JdsBiNavInputs {
  colorScheme: string;
  variant: string;
  orientation?: string;
}

export interface JdsBiNavStyles {
  nav: string;
}

export interface JdsBiNavPaneStyles {
  pane: string;
}
