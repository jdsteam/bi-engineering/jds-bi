import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[jdsBiNavContent]',
})
export class JdsBiNavContentDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}
