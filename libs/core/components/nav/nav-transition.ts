import { TransitionStartFn } from '@jds-bi/core/utils';

function reflow(element: HTMLElement) {
  return (element || document.body).getBoundingClientRect();
}

export const NavFadeOutTransition: TransitionStartFn = ({ classList }) => {
  classList.remove('show');
  return () => classList.remove('active');
};

export const NavFadeInTransition: TransitionStartFn = (
  element: HTMLElement,
  animation: boolean,
) => {
  if (animation) {
    reflow(element);
  }
  element.classList.add('show');
};
