import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { JdsBiNavDirective } from './nav.directive';
import { JdsBiNavItemDirective } from './nav-item.directive';
import { JdsBiNavLinkDirective } from './nav-link.directive';
import { JdsBiNavContentDirective } from './nav-content.directive';
import { JdsBiNavPaneDirective } from './nav-pane.directive';
import { JdsBiNavOutletComponent } from './nav-outlet.component';

@NgModule({
  declarations: [
    JdsBiNavContentDirective,
    JdsBiNavDirective,
    JdsBiNavItemDirective,
    JdsBiNavLinkDirective,
    JdsBiNavOutletComponent,
    JdsBiNavPaneDirective,
  ],
  imports: [CommonModule],
  exports: [
    JdsBiNavContentDirective,
    JdsBiNavDirective,
    JdsBiNavItemDirective,
    JdsBiNavLinkDirective,
    JdsBiNavOutletComponent,
    JdsBiNavPaneDirective,
  ],
})
export class JdsBiNavModule {}
