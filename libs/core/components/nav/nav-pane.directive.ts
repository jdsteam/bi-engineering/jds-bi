import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
} from '@angular/core';

import { JdsBiNavPaneStyles } from './nav.dto';
import { JdsBiNavDirective } from './nav.directive';
import { JdsBiNavItemDirective } from './nav-item.directive';
import { JdsBiNavStyle } from './nav.style';

@Directive({
  selector: '[jdsBiNavPane]',
  providers: [JdsBiNavStyle],
})
export class JdsBiNavPaneDirective implements OnInit {
  @Input() item!: JdsBiNavItemDirective;
  @Input() nav!: JdsBiNavDirective;
  @Input() role?: string;

  // Variable
  className!: JdsBiNavPaneStyles;

  constructor(
    public elRef: ElementRef<HTMLElement>,
    private jdsBiNavStyle: JdsBiNavStyle,
  ) {}

  @HostBinding('id') get hostOrientation() {
    return this.item.panelDomId;
  }

  @HostBinding('class.t-tab-pane')
  get hostTabPane() {
    return true;
  }

  @HostBinding('class.fade')
  get hostFade() {
    return true;
  }

  @HostBinding('attr.role') get hostRole() {
    if (this.role) {
      return this.role;
    }

    return this.nav.roles ? 'tabpanel' : undefined;
  }

  @HostBinding('attr.aria-labelledby')
  get hostAriaLabeldby() {
    return this.item.domId;
  }

  ngOnInit(): void {
    this.className = this.jdsBiNavStyle.getStylePane();
    this.elRef.nativeElement.classList.add(this.className.pane);
  }
}
