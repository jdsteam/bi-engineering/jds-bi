import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Inject,
  Input,
} from '@angular/core';
import { CSS as CSS_TOKEN, USER_AGENT } from '@ng-web-apis/common';
import {
  JDS_BI_IS_IOS,
  jdsBiDefaultProp,
  jdsBiGetElementOffset,
  JdsBiInjectionTokenType,
  jdsBiIsFirefox,
} from '@jds-bi/cdk';
import {
  JDS_BI_SCROLL_INTO_VIEW,
  JDS_BI_SCROLLABLE,
} from '@jds-bi/core/constants';
import { JDS_BI_SCROLL_REF } from '@jds-bi/core/tokens';

@Component({
  selector: 'jds-bi-scrollbar',
  templateUrl: './scrollbar.template.html',
  styleUrls: ['./scrollbar.style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: JDS_BI_SCROLL_REF,
      deps: [JdsBiScrollbarComponent],
      useFactory: ({
        browserScrollRef,
      }: JdsBiScrollbarComponent): ElementRef<HTMLElement> => browserScrollRef,
    },
  ],
})
export class JdsBiScrollbarComponent {
  private delegated = false;

  private readonly isLegacy: boolean =
    !this.cssRef.supports('position', 'sticky') ||
    (jdsBiIsFirefox(this.userAgent) &&
      !this.cssRef.supports('scrollbar-width', 'none'));

  @Input()
  @jdsBiDefaultProp()
  hidden = false;

  readonly browserScrollRef = new ElementRef(this.elementRef.nativeElement);

  constructor(
    @Inject(CSS_TOKEN)
    private readonly cssRef: JdsBiInjectionTokenType<typeof CSS_TOKEN>,
    @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>,
    @Inject(USER_AGENT) private readonly userAgent: string,
    @Inject(JDS_BI_IS_IOS) private readonly isIos: boolean,
  ) {}

  get showScrollbars(): boolean {
    return !this.hidden && !this.isIos && (!this.isLegacy || this.delegated);
  }

  @HostBinding('class._legacy')
  get showNative(): boolean {
    return this.isLegacy && !this.hidden && !this.delegated;
  }

  @HostListener(`${JDS_BI_SCROLLABLE}.stop`, ['$event.detail'])
  onScrollable(element: HTMLElement): void {
    this.delegated = true;
    this.browserScrollRef.nativeElement = element;
  }

  @HostListener(`${JDS_BI_SCROLL_INTO_VIEW}.stop`, ['$event.detail'])
  scrollIntoView(detail: HTMLElement): void {
    if (this.delegated) {
      return;
    }

    const { nativeElement } = this.browserScrollRef;
    const { offsetTop, offsetLeft } = jdsBiGetElementOffset(
      nativeElement,
      detail,
    );
    const { clientHeight, clientWidth } = nativeElement;
    const { offsetHeight, offsetWidth } = detail;
    const scrollTop = offsetTop + offsetHeight / 2 - clientHeight / 2;
    const scrollLeft = offsetLeft + offsetWidth / 2 - clientWidth / 2;

    // ?. for our clients on Windows XP and Chrome 49
    nativeElement.scrollTo?.(scrollLeft, scrollTop);
  }
}
