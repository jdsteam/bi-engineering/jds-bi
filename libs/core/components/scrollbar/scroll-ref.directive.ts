import { Directive, ElementRef } from '@angular/core';
import { JDS_BI_SCROLL_REF } from '@jds-bi/core/tokens';

export const SCROLL_REF_SELECTOR = '[jdsBiScrollRef]';

@Directive({
  selector: SCROLL_REF_SELECTOR,
  providers: [
    {
      provide: JDS_BI_SCROLL_REF,
      useExisting: ElementRef,
    },
  ],
})
export class JdsBiScrollRefDirective {}
