import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { JdsBiScrollControlsModule } from '@jds-bi/core/components/scroll-controls';

import { JdsBiScrollRefDirective } from './scroll-ref.directive';
import { JdsBiScrollableDirective } from './scrollable.directive';
import { JdsBiScrollbarComponent } from './scrollbar.component';

@NgModule({
  imports: [CommonModule, JdsBiScrollControlsModule],
  declarations: [
    JdsBiScrollbarComponent,
    JdsBiScrollRefDirective,
    JdsBiScrollableDirective,
  ],
  exports: [
    JdsBiScrollbarComponent,
    JdsBiScrollRefDirective,
    JdsBiScrollableDirective,
  ],
})
export class JdsBiScrollbarModule {}
