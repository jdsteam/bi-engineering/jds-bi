import { Directive, ElementRef, Inject, OnInit } from '@angular/core';
import { JDS_BI_SCROLLABLE } from '@jds-bi/core/constants';

@Directive({
  selector: '[jdsBiScrollable]',
})
export class JdsBiScrollableDirective implements OnInit {
  constructor(
    @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>,
  ) {}

  ngOnInit(): void {
    this.elementRef.nativeElement.dispatchEvent(
      new CustomEvent(JDS_BI_SCROLLABLE, {
        bubbles: true,
        detail: this.elementRef.nativeElement,
      }),
    );
  }
}
