import { AnimationOptions } from '@angular/animations';
import { DOCUMENT } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  NgZone,
  Optional,
} from '@angular/core';
import { ANIMATION_FRAME } from '@ng-web-apis/common';
import { jdsBiZoneOptimized } from '@jds-bi/cdk';
import { jdsBiFadeIn } from '@jds-bi/core/animations';
import { MODE_PROVIDER } from '@jds-bi/core/providers';
import {
  JDS_BI_ANIMATION_OPTIONS,
  JDS_BI_MODE,
  JDS_BI_SCROLL_REF,
} from '@jds-bi/core/tokens';
import { JdsBiBrightness } from '@jds-bi/core/types';
import { Observable } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  startWith,
  throttleTime,
} from 'rxjs/operators';

@Component({
  selector: 'jds-bi-scroll-controls',
  templateUrl: './scroll-controls.template.html',
  styleUrls: ['./scroll-controls.style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [jdsBiFadeIn],
  providers: [MODE_PROVIDER],
  // eslint-disable-next-line @angular-eslint/no-host-metadata-property
  host: {
    '($.data-mode.attr)': 'mode$',
  },
})
export class JdsBiScrollControlsComponent {
  readonly refresh$ = this.animationFrame$.pipe(
    throttleTime(300),
    map(() => this.scrollbars),
    startWith([false, false]),
    distinctUntilChanged((a, b) => a[0] === b[0] && a[1] === b[1]),
    jdsBiZoneOptimized(this.ngZone),
  );

  readonly animation = {
    value: '',
    ...this.options,
  } as const;

  constructor(
    @Inject(JDS_BI_ANIMATION_OPTIONS)
    private readonly options: AnimationOptions,
    @Inject(NgZone) private readonly ngZone: NgZone,
    @Inject(DOCUMENT) private readonly documentRef: Document,
    @Optional()
    @Inject(JDS_BI_SCROLL_REF)
    private readonly scrollRef: ElementRef<HTMLElement> | null,
    @Inject(ANIMATION_FRAME)
    private readonly animationFrame$: Observable<number>,
    @Inject(JDS_BI_MODE) readonly mode$: Observable<JdsBiBrightness | null>,
  ) {}

  private get scrollbars(): [boolean, boolean] {
    const { clientHeight, scrollHeight, clientWidth, scrollWidth } = this
      .scrollRef
      ? this.scrollRef.nativeElement
      : this.documentRef.documentElement;

    return [
      Math.ceil((clientHeight / scrollHeight) * 100) < 100,
      Math.ceil((clientWidth / scrollWidth) * 100) < 100,
    ];
  }
}
