import { Directive, ElementRef } from '@angular/core';
import { JDS_BI_ELEMENT_REF } from '@jds-bi/core/tokens';

@Directive({
  selector: '[jdsBiScrollbarWrapper]',
  providers: [
    {
      provide: JDS_BI_ELEMENT_REF,
      useExisting: ElementRef,
    },
  ],
})
export class JdsBiScrollbarWrapperDirective {}
