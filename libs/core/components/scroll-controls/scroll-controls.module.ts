import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { JdsBiLetModule } from '@jds-bi/cdk';

import { JdsBiScrollControlsComponent } from './scroll-controls.component';
import { JdsBiScrollbarDirective } from './scrollbar.directive';
import { JdsBiScrollbarWrapperDirective } from './scrollbar-wrapper.directive';

@NgModule({
  imports: [CommonModule, JdsBiLetModule],
  declarations: [
    JdsBiScrollbarDirective,
    JdsBiScrollbarWrapperDirective,
    JdsBiScrollControlsComponent,
  ],
  exports: [JdsBiScrollControlsComponent],
})
export class JdsBiScrollControlsModule {}
