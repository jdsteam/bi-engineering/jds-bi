export const JdsBiInputSize = ['sm', 'md', 'lg', 'xl'] as const;

export type JdsBiInputSize = typeof JdsBiInputSize[number];
