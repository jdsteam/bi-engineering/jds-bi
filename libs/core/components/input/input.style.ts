import { Injectable } from '@angular/core';
import { jdsBiColor, jdsBiColorScheme } from '@jds-bi/cdk';
import { css } from '@emotion/css';

import type {
  JdsBiInputInputs,
  JdsBiInputGroupInputs,
  JdsBiInputStyles,
  JdsBiInputGroupStyles,
} from './input.dto';

@Injectable()
export class JdsBiInputStyle {
  getSize(size: string): any {
    let fontSize: string;
    let padding: string;
    let paddingMin: string;
    let marginEnd: string;
    let marginEl: string;
    let width: string;
    let height: string;

    switch (size) {
      case 'sm':
        fontSize = '12px';
        padding = '8px 6px';
        paddingMin = '-8px -6px';
        marginEnd = '6px';
        marginEl = '0px';
        width = '34px';
        height = '34px';
        break;
      case 'md':
        fontSize = '14px';
        padding = '10px 8px';
        paddingMin = '-10px -8px';
        marginEnd = '8px';
        marginEl = '0px';
        width = '38px';
        height = '38px';
        break;
      case 'lg':
        fontSize = '16px';
        padding = '12px 10px';
        paddingMin = '-12px -10px';
        marginEnd = '10px';
        marginEl = '3px';
        width = '44px';
        height = '44px';
        break;
      case 'xl':
        fontSize = '16px';
        padding = '14px 12px';
        paddingMin = '-14px -12px';
        marginEnd = '12px';
        marginEl = '6px';
        width = '48px';
        height = '48px';
        break;
      default:
        fontSize = '14px';
        padding = '10px 8px';
        paddingMin = '-10px -8px';
        marginEnd = '8px';
        marginEl = '0px';
        width = '38px';
        height = '38px';
        break;
    }

    return {
      fontSize,
      padding,
      paddingMin,
      marginEnd,
      marginEl,
      width,
      height,
    };
  }

  getStyle(inputs: JdsBiInputInputs): JdsBiInputStyles {
    const colorScheme = jdsBiColorScheme(inputs.colorScheme);
    const { color: schemeColor } = colorScheme;

    const primary = schemeColor['700'];
    const gray100 = jdsBiColor.gray['100'];
    const gray200 = jdsBiColor.gray['200'];
    const gray300 = jdsBiColor.gray['300'];
    const gray500 = jdsBiColor.gray['500'];
    const gray600 = jdsBiColor.gray['600'];
    const gray800 = jdsBiColor.gray['800'];
    const green700 = jdsBiColor.green['700'];
    const red700 = jdsBiColor.red['700'];
    const yellow500 = jdsBiColor.yellow['500'];

    const { fontSize, padding, paddingMin, marginEnd } = this.getSize(
      inputs.size,
    );

    const input = css`
      &.t-input {
        display: block;
        width: 100%;
        padding: ${padding};
        font-size: ${fontSize};
        line-height: 16px;
        color: ${gray800};
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid ${gray500};
        border-radius: 8px;
        appearance: none;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

        &[type='file'] {
          overflow: hidden;

          &:not(:disabled):not([readonly]) {
            cursor: pointer;
          }
        }

        &:focus {
          color: ${gray800};
          background-color: #fff;
          border-color: ${primary};
          outline: 0;
        }

        &:focus-within {
          box-shadow: inset 0 0 0 1px ${yellow500};
        }

        &::placeholder {
          color: ${gray600};
          opacity: 1;
        }

        &:disabled,
        &[readonly] {
          background-color: ${gray300};
          border-color: ${gray500};
          opacity: 1;
        }

        &::file-selector-button {
          padding: ${padding};
          margin: ${paddingMin};
          margin-inline-end: ${marginEnd};
          color: ${gray800};
          background-color: ${gray100};
          pointer-events: none;
          border-color: inherit;
          border-style: solid;
          border-width: 0;
          border-inline-end-width: 1px;
          border-radius: 0;
        }

        &:hover:not(:disabled):not([readonly])::-webkit-file-upload-button {
          background-color: ${gray200};
        }

        &.t-valid {
          border: 1px solid ${green700};
        }

        &.t-invalid {
          border: 1px solid ${red700};
        }
      }
    `;

    return { input };
  }

  getStyleGroup(inputs: JdsBiInputGroupInputs): JdsBiInputGroupStyles {
    const gray100 = jdsBiColor.gray['100'];
    const gray500 = jdsBiColor.gray['500'];
    const gray800 = jdsBiColor.gray['800'];

    const { fontSize, padding, marginEl, width, height } = this.getSize(
      inputs.size,
    );

    const group = css`
      &.t-input-group {
        position: relative;
        display: flex;
        flex-wrap: wrap;
        align-items: stretch;
        width: 100%;

        > .t-input {
          position: relative;
          flex: 1 1 auto;
          width: 1%;
          min-width: 0;
        }

        ${inputs.variant === 'addon' &&
        css`
          > :not(:first-child) {
            margin-left: -1px;
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
          }

          > :not(:last-child) {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
          }

          .t-input-left-addon,
          .t-input-right-addon {
            display: flex;
            align-items: center;
            padding: ${padding};
            font-size: ${fontSize};
            font-weight: 400;
            line-height: 16px;
            color: ${gray800};
            background-color: ${gray100};
            border: 1px solid ${gray500};
            border-radius: 8px;
            text-align: center;
            white-space: nowrap;
          }

          .t-input-left-addon {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
          }

          .t-input-right-addon {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
          }
        `}

        ${inputs.variant === 'element' &&
        css`
          &:has(.t-input-left-element) {
            .t-input {
              padding-left: ${width};
            }
          }

          &:has(.t-input-right-element) {
            .t-input {
              padding-right: ${width};
            }
          }

          .t-input-left-element,
          .t-input-right-element {
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            top: 0;
            z-index: 2;
            min-width: ${width};
            height: ${height};
          }

          .t-input-left-element {
            left: ${marginEl};
          }

          .t-input-right-element {
            right: ${marginEl};
          }
        `}
      }
    `;

    return { group };
  }
}
