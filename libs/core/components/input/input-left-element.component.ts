import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'jds-bi-input-left-element',
  exportAs: 'jdsBiInputLeftElement',
  template: '<ng-content></ng-content>',
})
export class JdsBiInputLeftElementComponent {
  @HostBinding('class.t-input-left-element')
  get hostInputGroup() {
    return true;
  }
}
