import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'jds-bi-input-right-element',
  exportAs: 'jdsBiInputRightElement',
  template: '<ng-content></ng-content>',
})
export class JdsBiInputRightElementComponent {
  @HostBinding('class.t-input-right-element')
  get hostInputGroup() {
    return true;
  }
}
