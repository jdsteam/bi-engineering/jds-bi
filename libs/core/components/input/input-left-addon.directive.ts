import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: 'span[jdsBiInputLeftAddon]',
  exportAs: 'jdsBiInputLeftAddon',
})
export class JdsBiInputLeftAddonDirective {
  @HostBinding('class.t-input-left-addon')
  get hostInputLeftAddon() {
    return true;
  }
}
