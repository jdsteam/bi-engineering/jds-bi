import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import type { JdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiActiveZoneDirective,
  JdsBiContextWithImplicit,
  jdsBiDefaultProp,
} from '@jds-bi/cdk';
import { PolymorpheusContent } from '@tinkoff/ng-polymorpheus';

import type {
  JdsBiInputSize,
  JdsBiInputInputs,
  JdsBiInputStyles,
} from './input.dto';
import { JdsBiInputStyle } from './input.style';

@Directive({
  selector: '[jdsBiInput]',
  exportAs: 'jdsBiInput',
  providers: [JdsBiInputStyle],
})
export class JdsBiInputDirective implements OnInit, OnChanges {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  size: JdsBiInputSize = 'md';

  @Input()
  @jdsBiDefaultProp()
  valid = false;

  @Input()
  @jdsBiDefaultProp()
  invalid = false;

  @Input('jdsBiInput')
  @jdsBiDefaultProp()
  content: PolymorpheusContent<
    JdsBiContextWithImplicit<JdsBiActiveZoneDirective>
  > = '';

  // Variable
  className!: JdsBiInputStyles;

  constructor(
    private elRef: ElementRef,
    private jdsBiInputStyle: JdsBiInputStyle,
  ) {}

  @HostBinding('class.t-input')
  get hostInput() {
    return true;
  }

  @HostBinding('class.t-valid')
  get hostInputValid() {
    return this.valid;
  }

  @HostBinding('class.t-invalid')
  get hostInputInvalid() {
    return this.invalid;
  }

  ngOnInit(): void {
    this.getClassnames({
      colorScheme: this.colorScheme,
      size: this.size,
      valid: this.valid,
      invalid: this.invalid,
    });
  }

  ngOnChanges(): void {
    if (!this.content) {
      this.getClassnames({
        colorScheme: this.colorScheme,
        size: this.size,
        valid: this.valid,
        invalid: this.invalid,
      });
    }
  }

  getClassnames(input: JdsBiInputInputs): void {
    if (this.className) {
      this.elRef.nativeElement.classList.remove(this.className.input);
    }

    this.className = this.jdsBiInputStyle.getStyle(input);
    this.elRef.nativeElement.classList.add(this.className.input);
  }
}
