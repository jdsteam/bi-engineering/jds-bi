export const JdsBiInputSize = ['sm', 'md', 'lg', 'xl'] as const;

export type JdsBiInputSize = typeof JdsBiInputSize[number];

export const JdsBiInputGroupVariant = ['addon', 'element'] as const;

export type JdsBiInputGroupVariant = typeof JdsBiInputGroupVariant[number];

export interface JdsBiInputInputs {
  colorScheme: string;
  size: string;
  valid?: boolean;
  invalid?: boolean;
}

export interface JdsBiInputGroupInputs {
  size: string;
  variant: string;
}

export interface JdsBiInputStyles {
  input: string;
}

export interface JdsBiInputGroupStyles {
  group: string;
}
