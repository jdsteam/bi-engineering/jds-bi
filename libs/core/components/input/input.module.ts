import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { JdsBiInputDirective } from './input.directive';
import { JdsBiInputGroupDirective } from './input-group.directive';
import { JdsBiInputLeftAddonDirective } from './input-left-addon.directive';
import { JdsBiInputRightAddonDirective } from './input-right-addon.directive';
import { JdsBiInputLeftElementComponent } from './input-left-element.component';
import { JdsBiInputRightElementComponent } from './input-right-element.component';

@NgModule({
  declarations: [
    JdsBiInputDirective,
    JdsBiInputGroupDirective,
    JdsBiInputLeftAddonDirective,
    JdsBiInputRightAddonDirective,
    JdsBiInputLeftElementComponent,
    JdsBiInputRightElementComponent,
  ],
  imports: [CommonModule],
  exports: [
    JdsBiInputDirective,
    JdsBiInputGroupDirective,
    JdsBiInputLeftAddonDirective,
    JdsBiInputRightAddonDirective,
    JdsBiInputLeftElementComponent,
    JdsBiInputRightElementComponent,
  ],
})
export class JdsBiInputModule {}
