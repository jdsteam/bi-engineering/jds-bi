import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: 'span[jdsBiInputRightAddon]',
  exportAs: 'jdsBiInputRightAddon',
})
export class JdsBiInputRightAddonDirective {
  @HostBinding('class.t-input-right-addon')
  get hostInputRightAddon() {
    return true;
  }
}
