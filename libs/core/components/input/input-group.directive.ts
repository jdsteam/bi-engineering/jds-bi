import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import {
  JdsBiActiveZoneDirective,
  JdsBiContextWithImplicit,
  jdsBiDefaultProp,
} from '@jds-bi/cdk';
import { PolymorpheusContent } from '@tinkoff/ng-polymorpheus';

import type {
  JdsBiInputSize,
  JdsBiInputGroupVariant,
  JdsBiInputGroupInputs,
  JdsBiInputGroupStyles,
} from './input.dto';
import { JdsBiInputStyle } from './input.style';

@Directive({
  selector: '[jdsBiInputGroup]',
  exportAs: 'jdsBiInputGroup',
  providers: [JdsBiInputStyle],
})
export class JdsBiInputGroupDirective implements OnInit, OnChanges {
  @Input()
  @jdsBiDefaultProp()
  size: JdsBiInputSize = 'md';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiInputGroupVariant = 'addon';

  @Input('jdsBiInputGroup')
  @jdsBiDefaultProp()
  content: PolymorpheusContent<
    JdsBiContextWithImplicit<JdsBiActiveZoneDirective>
  > = '';

  // Variable
  className!: JdsBiInputGroupStyles;

  constructor(
    private _elRef: ElementRef,
    private jdsBiInputStyle: JdsBiInputStyle,
  ) {}

  @HostBinding('class.t-input-group')
  get hostInputGroup() {
    return true;
  }

  ngOnInit(): void {
    this.getClassnames({
      size: this.size,
      variant: this.variant,
    });
  }

  ngOnChanges(): void {
    if (!this.content) {
      this.getClassnames({
        size: this.size,
        variant: this.variant,
      });
    }
  }

  getClassnames(input: JdsBiInputGroupInputs): void {
    if (this.className) {
      this._elRef.nativeElement.classList.remove(this.className.group);
    }

    this.className = this.jdsBiInputStyle.getStyleGroup(input);
    this._elRef.nativeElement.classList.add(this.className.group);
  }
}
