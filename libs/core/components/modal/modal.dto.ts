export const JdsBiModalSize = ['sm', 'md', 'lg', 'xl', '2xl'] as const;

export type JdsBiModalSize = typeof JdsBiModalSize[number];

export const JdsBiModalPosition = ['start', 'center'] as const;

export type JdsBiModalPosition = typeof JdsBiModalPosition[number];

export interface JdsBiModalInputs {
  size: string;
  position: string;
  showHeader: boolean;
  showFooter: boolean;
}

export interface JdsBiModalStyles {
  modal: string;
  dialog: string;
  content: string;
  header: string;
  body: string;
  footer: string;
  background: string;
}
