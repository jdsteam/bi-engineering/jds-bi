import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JdsBiModalComponent } from './modal.component';

@NgModule({
  declarations: [JdsBiModalComponent],
  imports: [CommonModule],
  exports: [JdsBiModalComponent],
})
export class JdsBiModalModule {}
