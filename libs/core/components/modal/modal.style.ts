import { Injectable } from '@angular/core';
import { css } from '@emotion/css';

import type { JdsBiModalInputs, JdsBiModalStyles } from './modal.dto';

@Injectable()
export class JdsBiModalStyle {
  getStyle(inputs: JdsBiModalInputs): JdsBiModalStyles {
    const modal = css`
      position: fixed;
      top: 0;
      left: 0;
      z-index: 1000;
      width: 100%;
      height: 100%;
      overflow-x: hidden;
      overflow-y: auto;
      outline: 0;
    `;

    const dialog = css`
      position: relative;
      width: auto;
      margin: 5px;
      pointer-events: none;

      ${inputs.position === 'center' &&
      css`
        display: flex;
        align-items: center;
      `}

      @media (min-width: 576px) {
        margin: 28px auto;

        ${inputs.size === 'sm' &&
        css`
          max-width: 300px;
        `}

        ${inputs.size === 'md' &&
        css`
          max-width: 510px;
        `}

        ${inputs.size === 'lg' &&
        css`
          max-width: 766px;
        `}

        ${inputs.size === 'xl' &&
        css`
          max-width: 900px;
        `}

        ${inputs.size === '2xl' &&
        css`
          max-width: 1140px;
        `}

        ${inputs.position === 'center' &&
        css`
          min-height: calc(100% - 3.5rem);
        `}
      }
    `;

    const content = css`
      position: relative;
      display: flex;
      flex-direction: column;
      width: 100%;
      pointer-events: auto;
      background-color: #fff;
      background-clip: padding-box;
      border-radius: 8px;
      outline: 0;
    `;

    const header = css`
      flex-shrink: 0;
      align-items: center;
      justify-content: space-between;
      padding: 8px 24px;
      border-top-right-radius: 8px;
      border-top-left-radius: 8px;

      ${inputs.showHeader &&
      css`
        display: flex;
      `}

      ${!inputs.showHeader &&
      css`
        display: none;
      `}

      h1, h2, h3, h4, h5, h6 {
        margin-top: 0;
        margin-bottom: 0;
      }

      .t-button-close {
        cursor: pointer;
        padding: 5px;
        display: flex;
        background: transparent;
        box-sizing: content-box;
        color: #000;
        border: 0;
        border-radius: 2.5px;
      }
    `;

    const body = css`
      position: relative;
      flex: 1 1 auto;
      padding: 12px 24px;
    `;

    const footer = css`
      flex-shrink: 0;
      align-items: center;
      justify-content: flex-end;
      padding: 16px 24px;
      background-color: #fafafa;
      border-bottom-right-radius: 8px;
      border-bottom-left-radius: 8px;

      ${inputs.showFooter &&
      css`
        display: flex;
      `}

      ${!inputs.showFooter &&
      css`
        display: none;
      `}
    `;

    const background = css`
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background-color: #000;
      opacity: 0.5;
      z-index: 900;
    `;

    return {
      modal,
      dialog,
      content,
      header,
      body,
      footer,
      background,
    };
  }
}
