import {
  Injectable,
  ComponentFactoryResolver,
  ComponentFactory,
  ApplicationRef,
  Type,
} from '@angular/core';

// MODEL
import { JdsBiModal } from './modal.model';
import { JdsBiModalRef } from './modal-ref.model';

// COMPONENT
import { JdsBiModalContainerComponent } from './modal-container.component';

@Injectable({ providedIn: 'root' })
export class JdsBiModalService {
  private modalContainer!: HTMLElement;
  private modalContainerFactory!: ComponentFactory<JdsBiModalContainerComponent>;

  private modals: any[] = [];

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
  ) {
    this.setupModalContainerFactory();
  }

  add(modal: any) {
    this.modals.push(modal);
  }

  remove(id: string) {
    this.modals = this.modals.filter((x) => x.id !== id);
  }

  open(id: string) {
    const modal = this.modals.find((x) => x.id === id);
    modal.open();
  }

  close(id: string) {
    const modal = this.modals.find((x) => x.id === id);
    modal.close();
  }

  openComponent<T extends JdsBiModal>(
    component: Type<T>,
    inputs?: any,
  ): JdsBiModalRef {
    this.setupModalContainerDiv();

    const modalContainerRef = this.appRef.bootstrap(
      this.modalContainerFactory,
      this.modalContainer,
    );
    const modalComponentRef = modalContainerRef.instance.createModal(component);

    if (inputs) {
      modalComponentRef.instance.onInjectInputs(inputs);
    }

    const modalRef = new JdsBiModalRef(modalContainerRef, modalComponentRef);

    return modalRef;
  }

  private setupModalContainerDiv(): void {
    this.modalContainer = document.createElement('div');
    document.getElementsByTagName('body')[0].appendChild(this.modalContainer);
  }

  private setupModalContainerFactory(): void {
    this.modalContainerFactory =
      this.componentFactoryResolver.resolveComponentFactory(
        JdsBiModalContainerComponent,
      );
  }
}
