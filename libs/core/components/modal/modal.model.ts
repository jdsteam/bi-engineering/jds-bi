import { JdsBiModalRef } from './modal-ref.model';

export abstract class JdsBiModal {
  modalInstance!: JdsBiModalRef;

  abstract onInjectInputs(inputs: any): void;

  close(output?: any): void {
    this.modalInstance.close(output);
  }

  dismiss(output?: any): void {
    this.modalInstance.dismiss(output);
  }
}
