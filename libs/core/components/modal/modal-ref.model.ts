import { ComponentRef } from '@angular/core';
import { Observable, Subject } from 'rxjs';

export class JdsBiModalRef {
  private result$ = new Subject<any>();

  constructor(
    private modalContainer: ComponentRef<any>,
    private modal: ComponentRef<any>,
  ) {
    this.modal.instance.modalInstance = this;
  }

  close(output: any): void {
    this.result$.next(output);
    this.destroy$();
  }

  dismiss(output: any): void {
    this.result$.error(output);
    this.destroy$();
  }

  onResult(): Observable<any> {
    return this.result$.asObservable();
  }

  private destroy$(): void {
    this.modal.destroy();
    this.modalContainer.destroy();
    this.result$.complete();
  }
}
