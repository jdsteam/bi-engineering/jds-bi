import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewEncapsulation,
  ElementRef,
  HostBinding,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

import { jdsBiDefaultProp } from '@jds-bi/cdk';
import type {
  JdsBiModalSize,
  JdsBiModalPosition,
  JdsBiModalStyles,
  JdsBiModalInputs,
} from './modal.dto';

import { JdsBiModalService } from './modal.service';
import { JdsBiModalStyle } from './modal.style';

@Component({
  selector: 'jds-bi-modal',
  templateUrl: 'modal.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [JdsBiModalStyle],
})
export class JdsBiModalComponent
  implements OnInit, OnChanges, OnDestroy, AfterViewInit
{
  @HostBinding('style') style: SafeStyle;

  @ViewChild('modalHeader') modalHeader: any;
  @ViewChild('modalFooter') modalFooter: any;
  showHeader = false;
  showFooter = false;

  @Input()
  @jdsBiDefaultProp()
  id = 'modal';

  @Input()
  @jdsBiDefaultProp()
  size: JdsBiModalSize = 'md';

  @Input()
  @jdsBiDefaultProp()
  position: JdsBiModalPosition = 'start';

  @Input()
  @jdsBiDefaultProp()
  isClose = true;

  // Variable
  className!: JdsBiModalStyles;

  private element: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private jdsBiModalService: JdsBiModalService,
    private el: ElementRef,
    private jdsBiModalStyle: JdsBiModalStyle,
    sanitizer: DomSanitizer,
  ) {
    this.element = el.nativeElement;
    this.style = sanitizer.bypassSecurityTrustStyle('display: none;');
  }

  ngOnInit(): void {
    this.getClassnames({
      size: this.size,
      position: this.position,
      showHeader: this.showHeader,
      showFooter: this.showFooter,
    });

    if (!this.id) {
      console.error('modal must have an id');
      return;
    }

    document.body.appendChild(this.element);

    this.element.addEventListener('click', (el: any) => {
      if (this.isClose && el.target.className === this.className.modal) {
        this.close();
      }
    });

    this.jdsBiModalService.add(this);
  }

  ngOnChanges(): void {
    this.getClassnames({
      size: this.size,
      position: this.position,
      showHeader: this.showHeader,
      showFooter: this.showFooter,
    });
  }

  ngOnDestroy(): void {
    this.jdsBiModalService.remove(this.id);
    this.element.remove();
  }

  ngAfterViewInit() {
    this.showHeader =
      this.modalHeader.nativeElement &&
      this.modalHeader.nativeElement.children.length > 0;
    this.showFooter =
      this.modalFooter.nativeElement &&
      this.modalFooter.nativeElement.children.length > 0;

    const show = {
      showHeader: this.showHeader,
      showFooter: this.showFooter,
    };

    this.getClassnames({
      size: this.size,
      position: this.position,
      ...show,
    });

    this.cdRef.detectChanges();
  }

  getClassnames(input: JdsBiModalInputs): void {
    this.className = this.jdsBiModalStyle.getStyle(input);
  }

  open(): void {
    this.element.style.display = 'block';
    document.body.classList.add('t-modal-open');
  }

  close(): void {
    this.element.style.display = 'none';
    document.body.classList.remove('t-modal-open');
  }
}
