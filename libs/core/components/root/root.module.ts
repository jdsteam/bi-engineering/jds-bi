import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { JdsBiDropdownHostModule } from '@jds-bi/cdk';
import { JdsBiScrollControlsModule } from '@jds-bi/core/components/scroll-controls';
import { EventPluginsModule } from '@tinkoff/ng-event-plugins';

import { JdsBiRootComponent } from './root.component';

@NgModule({
  imports: [
    CommonModule,
    EventPluginsModule,
    JdsBiDropdownHostModule,
    JdsBiScrollControlsModule,
  ],
  declarations: [JdsBiRootComponent],
  exports: [JdsBiRootComponent],
})
export class JdsBiRootModule {}
