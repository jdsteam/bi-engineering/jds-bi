import { DOCUMENT } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import {
  JDS_BI_DIALOGS,
  JDS_BI_IS_MOBILE,
  JDS_BI_VERSION,
  jdsBiAssert,
} from '@jds-bi/cdk';
import { JDS_BI_IS_MOBILE_RES_PROVIDER } from '@jds-bi/core/providers';
import {
  JDS_BI_ANIMATIONS_DURATION,
  JDS_BI_ASSERT_ENABLED,
  JDS_BI_IS_MOBILE_RES,
  JDS_BI_THEME,
} from '@jds-bi/core/tokens';
import { merge, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'jds-bi-root',
  templateUrl: './root.template.html',
  styleUrls: ['./root.style.scss'],
  // So that we do not force OnPush on custom dialogs
  // eslint-disable-next-line @angular-eslint/prefer-on-push-component-change-detection
  changeDetection: ChangeDetectionStrategy.Default,
  providers: [JDS_BI_IS_MOBILE_RES_PROVIDER],
  encapsulation: ViewEncapsulation.None,
  // eslint-disable-next-line @angular-eslint/no-host-metadata-property
  host: {
    'data-jds-bi-version': JDS_BI_VERSION,
    '($.class._mobile)': 'isMobileRes$',
  },
})
export class JdsBiRootComponent {
  readonly scrollbars$: Observable<boolean> =
    this.dialogs.length && !this.isMobile
      ? merge(...this.dialogs).pipe(map(({ length }) => !length))
      : of(!this.isMobile);

  constructor(
    @Inject(JDS_BI_ANIMATIONS_DURATION) readonly duration: number,
    @Inject(ElementRef) readonly elementRef: ElementRef<HTMLElement>,
    @Inject(JDS_BI_DIALOGS)
    readonly dialogs: ReadonlyArray<Observable<readonly unknown[]>>,
    @Inject(JDS_BI_IS_MOBILE) private readonly isMobile: boolean,
    @Inject(JDS_BI_ASSERT_ENABLED) enabled: boolean,
    @Inject(JDS_BI_IS_MOBILE_RES) readonly isMobileRes$: Observable<boolean>,
    @Inject(DOCUMENT) { body }: Document,
    @Inject(JDS_BI_THEME) theme: string,
  ) {
    jdsBiAssert.enabled = enabled;
    body.setAttribute('data-jds-bi-theme', theme.toLowerCase());
  }
}
