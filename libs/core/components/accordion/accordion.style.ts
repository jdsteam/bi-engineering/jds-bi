import { Injectable } from '@angular/core';
import { jdsBiColor, jdsBiColorScheme } from '@jds-bi/cdk';
import { css } from '@emotion/css';

import type {
  JdsBiAccordionInputs,
  JdsBiAccordionStyles,
} from './accordion.dto';

@Injectable()
export class JdsBiAccordionStyle {
  getColorScheme(value: string) {
    const colorScheme = jdsBiColorScheme(value);
    const { color: schemeColor, hexSVG } = colorScheme;

    return { schemeColor, hexSVG };
  }

  getStyle(inputs: JdsBiAccordionInputs): JdsBiAccordionStyles {
    const { schemeColor, hexSVG } = this.getColorScheme(inputs.colorScheme);

    const hexGray800 = jdsBiColor.gray['800'].replace('#', '%23');

    const accordion = css`
      &.t-accordion {
        .t-accordion-item {
          color: ${jdsBiColor.gray['800']};
          background-color: #fff;
          border: 1px solid ${schemeColor['700']};

          &:first-of-type,
          &:first-of-type .t-accordion-button,
          &:first-of-type .t-accordion-button:focus-within {
            border-top-left-radius: 8px;
            border-top-right-radius: 8px;
          }

          &:not(:first-of-type) {
            border-top: 0;
          }

          &:last-of-type,
          &:last-of-type .t-accordion-button,
          &:last-of-type .t-accordion-button.t-collapsed:focus-within {
            border-bottom-left-radius: 8px;
            border-bottom-right-radius: 8px;
          }

          .t-accordion-header {
            margin-bottom: 0;

            button {
              cursor: pointer;

              &.t-accordion-button {
                width: 100%;

                &::after {
                  flex-shrink: 0;
                  width: 20px;
                  height: 20px;
                  margin-left: auto;
                  content: '';
                  background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 18 18' fill='${hexGray800}'%3e%3cpath d='M13.5666 8.45372L7.67509 2.22366C7.53883 2.07945 7.35693 2 7.16297 2C6.96902 2 6.78712 2.07945 6.65086 2.22366L6.21699 2.68235C5.93467 2.98124 5.93467 3.46703 6.21699 3.76547L11.1642 8.9971L6.2115 14.2345C6.07524 14.3787 6 14.571 6 14.776C6 14.9812 6.07524 15.1734 6.2115 15.3178L6.64537 15.7763C6.78174 15.9206 6.96353 16 7.15749 16C7.35144 16 7.53334 15.9206 7.6696 15.7763L13.5666 9.54059C13.7032 9.39592 13.7782 9.20277 13.7778 8.99744C13.7782 8.79131 13.7032 8.59827 13.5666 8.45372Z'/%3e%3c/svg%3e");
                  background-repeat: no-repeat;
                  background-size: 20px;
                  transition: transform 0.2s ease-in-out;
                }

                &:not(.t-collapsed):after {
                  background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 18 18' fill='${hexSVG}'%3e%3cpath d='M13.5666 8.45372L7.67509 2.22366C7.53883 2.07945 7.35693 2 7.16297 2C6.96902 2 6.78712 2.07945 6.65086 2.22366L6.21699 2.68235C5.93467 2.98124 5.93467 3.46703 6.21699 3.76547L11.1642 8.9971L6.2115 14.2345C6.07524 14.3787 6 14.571 6 14.776C6 14.9812 6.07524 15.1734 6.2115 15.3178L6.64537 15.7763C6.78174 15.9206 6.96353 16 7.15749 16C7.35144 16 7.53334 15.9206 7.6696 15.7763L13.5666 9.54059C13.7032 9.39592 13.7782 9.20277 13.7778 8.99744C13.7782 8.79131 13.7032 8.59827 13.5666 8.45372Z'/%3e%3c/svg%3e");
                  transform: rotate(90deg);
                }
              }
            }

            .t-accordion-button {
              position: relative;
              display: flex;
              align-items: center;
              padding: 12px 16px;
              font-size: 16px;
              font-weight: 700;
              line-height: 26px;
              color: ${jdsBiColor.gray['900']};
              text-align: left;
              background-color: transparent;
              border: 0;
              border-radius: 0;
              overflow-anchor: none;
              transition: color 0.15s ease-in-out,
                background-color 0.15s ease-in-out,
                border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out,
                border-radius 0.15s ease;

              &:focus-within {
                box-shadow: inset 0 0 0 1px ${jdsBiColor.yellow['500']};
              }

              &:disabled {
                opacity: 0.5;
                cursor: not-allowed;
              }

              &:not(.t-collapsed) {
                color: ${schemeColor['700']};
              }
            }
          }

          .t-accordion-body {
            padding: 8px 16px;
            border-top: 1px solid ${schemeColor['700']};
          }
        }

        .t-collapsing {
          height: 0;
          overflow: hidden;
          transition: height 0.35s ease;
        }

        .t-collapse:not(.t-show) {
          display: none;
        }
      }
    `;

    return { accordion };
  }
}
