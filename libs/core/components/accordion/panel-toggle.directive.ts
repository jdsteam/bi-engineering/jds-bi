import {
  Directive,
  Host,
  HostBinding,
  HostListener,
  Input,
  Optional,
} from '@angular/core';

import { JdsBiAccordionComponent } from './accordion.component';
import { JdsBiPanelDirective } from './panel.directive';

@Directive({
  selector: 'button[jdsBiPanelToggle]',
})
export class JdsBiPanelToggleDirective {
  static ngAcceptInputType_jdsBiPanelToggle: JdsBiPanelDirective | '';

  @Input()
  set jdsBiPanelToggle(panel: JdsBiPanelDirective) {
    if (panel) {
      this.panel = panel;
    }
  }

  constructor(
    public accordion: JdsBiAccordionComponent,
    @Optional() @Host() public panel: JdsBiPanelDirective,
  ) {}

  @HostBinding('type')
  get hostType() {
    return 'button';
  }

  @HostBinding('disabled')
  get hostDisabled() {
    return this.panel.disabled;
  }

  @HostBinding('class.t-collapsed')
  get hostCollapsed() {
    return !this.panel.isOpen;
  }

  @HostBinding('attr.aria-expanded')
  get hostAriaExpanded() {
    return this.panel.isOpen;
  }

  @HostBinding('attr.aria-controls')
  get hostAriaControls() {
    return this.panel.id;
  }

  @HostListener('click', ['$event']) onClick() {
    this.accordion.toggle(this.panel.id);
  }
}
