import {
  Directive,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';

@Directive({
  selector: '[jdsBiRef]',
})
export class JdsBiRefDirective implements OnInit, OnDestroy {
  @Output() jdsBiRef = new EventEmitter<HTMLElement | null>();
  constructor(private _El: ElementRef) {}

  ngOnInit() {
    this.jdsBiRef.emit(this._El.nativeElement);
  }

  ngOnDestroy() {
    this.jdsBiRef.emit(null);
  }
}
