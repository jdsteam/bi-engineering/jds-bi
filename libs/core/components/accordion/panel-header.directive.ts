import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: 'ng-template[jdsBiPanelHeader]',
})
export class JdsBiPanelHeaderDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}
