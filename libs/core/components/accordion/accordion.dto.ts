export interface JdsBiPanelHeaderContext {
  opened: boolean;
}

export interface JdsBiPanelChangeEvent {
  panelId: string;
  nextState: boolean;
  preventDefault: () => void;
}

export interface JdsBiAccordionInputs {
  colorScheme: string;
}

export interface JdsBiAccordionStyles {
  accordion: string;
}
