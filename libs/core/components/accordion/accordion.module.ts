import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { JdsBiPanelDirective } from './panel.directive';
import { JdsBiPanelHeaderDirective } from './panel-header.directive';
import { JdsBiPanelTitleDirective } from './panel-title.directive';
import { JdsBiPanelContentDirective } from './panel-content.directive';
import {
  JdsBiAccordionComponent,
  JdsBiPanelToggleDirective,
} from './accordion.component';
import { JdsBiRefDirective } from './ref.directive';

@NgModule({
  declarations: [
    JdsBiAccordionComponent,
    JdsBiPanelDirective,
    JdsBiPanelHeaderDirective,
    JdsBiPanelTitleDirective,
    JdsBiPanelContentDirective,
    JdsBiPanelToggleDirective,
    JdsBiRefDirective,
  ],
  imports: [CommonModule],
  exports: [
    JdsBiAccordionComponent,
    JdsBiPanelDirective,
    JdsBiPanelHeaderDirective,
    JdsBiPanelTitleDirective,
    JdsBiPanelContentDirective,
    JdsBiPanelToggleDirective,
  ],
})
export class JdsBiAccordionModule {}
