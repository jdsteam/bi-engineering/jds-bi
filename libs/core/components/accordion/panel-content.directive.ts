import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: 'ng-template[jdsBiPanelContent]',
})
export class JdsBiPanelContentDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}
