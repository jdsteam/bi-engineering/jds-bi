/* eslint-disable @typescript-eslint/brace-style */
import {
  AfterContentChecked,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  Directive,
  ElementRef,
  EventEmitter,
  Host,
  HostBinding,
  HostListener,
  Input,
  NgZone,
  OnChanges,
  OnInit,
  Optional,
  Output,
  QueryList,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { take } from 'rxjs/operators';
import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import { JdsBiPanelDirective } from './panel.directive';
import { JdsBiAccordionStyle } from './accordion.style';
import type {
  JdsBiPanelChangeEvent,
  JdsBiAccordionInputs,
  JdsBiAccordionStyles,
} from './accordion.dto';

import { RunTransition, CollapsingTransition } from '@jds-bi/core/utils/dom';

function isString(value: any): value is string {
  return typeof value === 'string';
}

@Component({
  selector: 'jds-bi-accordion',
  exportAs: 'jdsBiAccordion',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './accordion.component.html',
  providers: [JdsBiAccordionStyle],
})
export class JdsBiAccordionComponent implements OnChanges, AfterContentChecked {
  @ContentChildren(JdsBiPanelDirective) panels!: QueryList<JdsBiPanelDirective>;

  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  activeIds: string | readonly string[] = [];

  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('closeOthers')
  @jdsBiDefaultProp()
  closeOtherPanels = false;

  @Input()
  @jdsBiDefaultProp()
  destroyOnHide = true;

  @Output()
  readonly panelChange = new EventEmitter<JdsBiPanelChangeEvent>();

  @Output()
  readonly shown = new EventEmitter<string>();

  @Output()
  readonly hidden = new EventEmitter<string>();

  // Variable
  className!: JdsBiAccordionStyles;

  constructor(
    private _ngZone: NgZone,
    private _changeDetector: ChangeDetectorRef,
    private _elRef: ElementRef,
    private jdsBiAccordionStyle: JdsBiAccordionStyle,
  ) {}

  @HostBinding('class.t-accordion')
  get hostAccordion() {
    return true;
  }

  @HostBinding('attr.role')
  get hostRole() {
    return 'tablist';
  }

  @HostBinding('attr.aria-multiselectable')
  get hostAriaMultiselecable() {
    return !this.closeOtherPanels;
  }

  ngOnChanges(): void {
    this.getClassnames({
      colorScheme: this.colorScheme,
    });
  }

  getClassnames(input: JdsBiAccordionInputs): void {
    if (this.className) {
      this._elRef.nativeElement.classList.remove(this.className.accordion);
    }

    this.className = this.jdsBiAccordionStyle.getStyle(input);
    this._elRef.nativeElement.classList.add(this.className.accordion);
  }

  isExpanded(panelId: string): boolean {
    return this.activeIds.indexOf(panelId) > -1;
  }

  expand(panelId: string): void {
    this._changeOpenState(this._findPanelById(panelId), true);
  }

  expandAll(): void {
    if (this.closeOtherPanels) {
      if (this.activeIds.length === 0 && this.panels.length) {
        this._changeOpenState(this.panels.first, true);
      }
    } else {
      this.panels.forEach((panel) => this._changeOpenState(panel, true));
    }
  }

  collapse(panelId: string) {
    this._changeOpenState(this._findPanelById(panelId), false);
  }

  collapseAll() {
    this.panels.forEach((panel) => {
      this._changeOpenState(panel, false);
    });
  }

  toggle(panelId: string) {
    const panel = this._findPanelById(panelId);
    if (panel) {
      this._changeOpenState(panel, !panel.isOpen);
    }
  }

  ngAfterContentChecked() {
    // active id updates
    if (isString(this.activeIds)) {
      this.activeIds = this.activeIds.split(/\s*,\s*/);
    }

    // update panels open states
    this.panels.forEach((panel) => {
      panel.isOpen = !panel.disabled && this.activeIds.indexOf(panel.id) > -1;
    });

    // closeOthers updates
    if (this.activeIds.length > 1 && this.closeOtherPanels) {
      this._closeOthers(this.activeIds[0], false);
      this._updateActiveIds();
    }

    // Setup the initial classes here
    this._ngZone.onStable.pipe(take(1)).subscribe(() => {
      this.panels.forEach((panel) => {
        const panelElement = panel.panelDiv;
        if (panelElement) {
          if (!panel.initClassDone) {
            panel.initClassDone = true;
            RunTransition(this._ngZone, panelElement, CollapsingTransition, {
              animation: false,
              runningTransition: 'continue',
              context: {
                direction: panel.isOpen ? 'show' : 'hide',
                dimension: 'height',
              },
            });
          }
        } else {
          // Classes must be initialized next time it will be in the dom
          panel.initClassDone = false;
        }
      });
    });
  }

  private _changeOpenState(
    panel: JdsBiPanelDirective | null,
    nextState: boolean,
  ) {
    if (panel != null && !panel.disabled && panel.isOpen !== nextState) {
      let defaultPrevented = false;

      this.panelChange.emit({
        panelId: panel.id,
        nextState,
        preventDefault: () => {
          defaultPrevented = true;
        },
      });

      if (!defaultPrevented) {
        panel.isOpen = nextState;
        panel.transitionRunning = true;

        if (nextState && this.closeOtherPanels) {
          this._closeOthers(panel.id);
        }
        this._updateActiveIds();
        this._runTransitions();
      }
    }
  }

  private _closeOthers(panelId: string, enableTransition = true) {
    this.panels.forEach((panel) => {
      if (panel.id !== panelId && panel.isOpen) {
        panel.isOpen = false;
        panel.transitionRunning = enableTransition;
      }
    });
  }

  private _findPanelById(panelId: string): JdsBiPanelDirective | null {
    return this.panels.find((p) => p.id === panelId) || null;
  }

  private _updateActiveIds() {
    this.activeIds = this.panels
      .filter((panel) => panel.isOpen && !panel.disabled)
      .map((panel) => panel.id);
  }

  private _runTransitions() {
    this._changeDetector.detectChanges();

    this.panels.forEach((panel) => {
      // When panel.transitionRunning is true, the transition needs to be started OR reversed,
      // The direction (show or hide) is choosen by each panel.isOpen state
      if (panel.transitionRunning) {
        const panelElement = panel.panelDiv;
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        RunTransition(this._ngZone, panelElement!, CollapsingTransition, {
          animation: true,
          runningTransition: 'stop',
          context: {
            direction: panel.isOpen ? 'show' : 'hide',
            dimension: 'height',
          },
        }).subscribe(() => {
          panel.transitionRunning = false;
          const { id } = panel;
          if (panel.isOpen) {
            panel.shown.emit();
            this.shown.emit(id);
          } else {
            panel.hidden.emit();
            this.hidden.emit(id);
          }
        });
      }
    });
  }
}

@Directive({
  selector: 'button[jdsBiPanelToggle]',
})
export class JdsBiPanelToggleDirective {
  static ngAcceptInputType_jdsBiPanelToggle: JdsBiPanelDirective | '';

  @Input()
  set jdsBiPanelToggle(panel: JdsBiPanelDirective) {
    if (panel) {
      this.panel = panel;
    }
  }

  constructor(
    public accordion: JdsBiAccordionComponent,
    @Optional() @Host() public panel: JdsBiPanelDirective,
  ) {}

  @HostBinding('type')
  get hostType() {
    return 'button';
  }

  @HostBinding('disabled')
  get hostDisabled() {
    return this.panel.disabled;
  }

  @HostBinding('class.t-collapsed')
  get hostCollapsed() {
    return !this.panel.isOpen;
  }

  @HostBinding('attr.aria-expanded')
  get hostAriaExpanded() {
    return this.panel.isOpen;
  }

  @HostBinding('attr.aria-controls')
  get hostAriaControls() {
    return this.panel.id;
  }

  @HostListener('click', ['$event']) onClick() {
    this.accordion.toggle(this.panel.id);
  }
}
