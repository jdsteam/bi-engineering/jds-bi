import {
  AfterContentChecked,
  ContentChildren,
  Directive,
  EventEmitter,
  Input,
  Output,
  QueryList,
} from '@angular/core';

import { JdsBiPanelHeaderDirective } from './panel-header.directive';
import { JdsBiPanelTitleDirective } from './panel-title.directive';
import { JdsBiPanelContentDirective } from './panel-content.directive';

let nextId = 0;

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'jds-bi-panel',
})
export class JdsBiPanelDirective implements AfterContentChecked {
  @Input() disabled = false;
  // eslint-disable-next-line no-plusplus
  @Input() id = `jds-bi-panel-${nextId++}`;

  isOpen = false;
  initClassDone = false;
  transitionRunning = false;

  @Input() title?: string;
  @Input() type?: string;
  @Input() cardClass?: string;
  @Output() shown = new EventEmitter<void>();
  @Output() hidden = new EventEmitter<void>();

  titleTpl?: JdsBiPanelTitleDirective;
  headerTpl?: JdsBiPanelHeaderDirective;
  contentTpl?: JdsBiPanelContentDirective;
  panelDiv!: HTMLElement | null;

  @ContentChildren(JdsBiPanelTitleDirective, { descendants: false })
  titleTpls!: QueryList<JdsBiPanelTitleDirective>;
  @ContentChildren(JdsBiPanelHeaderDirective, { descendants: false })
  headerTpls!: QueryList<JdsBiPanelHeaderDirective>;
  @ContentChildren(JdsBiPanelContentDirective, { descendants: false })
  contentTpls!: QueryList<JdsBiPanelContentDirective>;

  ngAfterContentChecked() {
    this.titleTpl = this.titleTpls.first;
    this.headerTpl = this.headerTpls.first;
    this.contentTpl = this.contentTpls.first;
  }
}
