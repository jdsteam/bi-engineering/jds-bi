import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: 'ng-template[jdsBiPanelTitle]',
})
export class JdsBiPanelTitleDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}
