export const JdsBiButtonVariant = [
  'solid',
  'outline',
  'ghost',
  'link',
  'input',
] as const;

export type JdsBiButtonVariant = typeof JdsBiButtonVariant[number];

export const JdsBiButtonSize = ['sm', 'md', 'lg', 'xl'] as const;

export type JdsBiButtonSize = typeof JdsBiButtonSize[number];

export interface JdsBiButtonInputs {
  colorScheme: string;
  variant: string;
  size: string;
  isLoading?: boolean;
  isIconOnly: boolean;
}

export interface JdsBiButtonStyles {
  button: string;
}
