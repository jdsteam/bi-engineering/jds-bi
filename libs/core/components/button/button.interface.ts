export const JdsBiButtonVariant = [
  'solid',
  'outline',
  'ghost',
  'link',
  'input',
] as const;

export type JdsBiButtonVariant = typeof JdsBiButtonVariant[number];
