import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import type { JdsBiColor } from '@jds-bi/cdk';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import type {
  JdsBiButtonVariant,
  JdsBiButtonSize,
  JdsBiButtonInputs,
  JdsBiButtonStyles,
} from './button.dto';
import { JdsBiButtonStyle } from './button.style';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: '[jdsBiButton]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [JdsBiButtonStyle],
})
export class JdsBiButtonComponent implements OnInit, OnChanges {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiButtonVariant = 'solid';

  @Input()
  @jdsBiDefaultProp()
  size: JdsBiButtonSize = 'md';

  @Input()
  @jdsBiDefaultProp()
  isLoading = false;

  @Input()
  @jdsBiDefaultProp()
  isIconOnly = false;

  @Input()
  @jdsBiDefaultProp()
  disabled = false;

  // Variable
  className!: JdsBiButtonStyles;

  constructor(
    private elRef: ElementRef,
    private jdsBiButtonStyle: JdsBiButtonStyle,
  ) {}

  @HostBinding('disabled')
  get hostDisabled() {
    return this.disabled || this.isLoading;
  }

  ngOnInit(): void {
    this.getClassnames({
      colorScheme: this.colorScheme,
      variant: this.variant,
      size: this.size,
      isIconOnly: this.isIconOnly,
    });
  }

  ngOnChanges(): void {
    this.getClassnames({
      colorScheme: this.colorScheme,
      variant: this.variant,
      size: this.size,
      isIconOnly: this.isIconOnly,
    });
  }

  getClassnames(input: JdsBiButtonInputs): void {
    if (this.className) {
      this.elRef.nativeElement.classList.remove(this.className.button);
    }

    this.className = this.jdsBiButtonStyle.getStyle(input);
    this.elRef.nativeElement.classList.add(this.className.button);
  }
}
