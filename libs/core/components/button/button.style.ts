import { Injectable } from '@angular/core';
import { jdsBiColor, jdsBiColorScheme } from '@jds-bi/cdk';
import { css } from '@emotion/css';

import type { JdsBiButtonInputs, JdsBiButtonStyles } from './button.dto';

@Injectable()
export class JdsBiButtonStyle {
  getStyle(inputs: JdsBiButtonInputs): JdsBiButtonStyles {
    const colorScheme = jdsBiColorScheme(inputs.colorScheme);
    const { color: schemeColor } = colorScheme;
    const yellow500 = jdsBiColor.yellow['500'];

    let fontSize: string;
    let padding: string | number;
    let borderWidth: string | number;
    let borderColor: string;
    let textDecoration: string;
    let backgroundColor: string;
    let color: string;
    let backgroundColorHover: string;

    switch (inputs.size) {
      case 'sm':
        fontSize = '12px';
        padding = '8px 14px';

        if (inputs.isIconOnly) {
          padding = '8px 10px';
        }

        break;
      case 'md':
        fontSize = '14px';
        padding = '10px 16px';

        if (inputs.isIconOnly) {
          padding = '10px 12px';
        }

        break;
      case 'lg':
        fontSize = '16px';
        padding = '12px 18px';

        if (inputs.isIconOnly) {
          padding = '8px 14px';
        }

        break;
      case 'xl':
        fontSize = '18px';
        padding = '14px 20px';

        if (inputs.isIconOnly) {
          padding = '14px 16px';
        }

        break;
      default:
        fontSize = '14px';
        padding = '10px 16px';

        if (inputs.isIconOnly) {
          padding = '10px 12px';
        }

        break;
    }

    switch (inputs.variant) {
      case 'solid':
      case 'outline':
      case 'ghost':
        borderWidth = '1px';
        textDecoration = 'none';
        break;
      case 'link':
        padding = 0;
        borderWidth = 0;
        textDecoration = 'underline';
        break;
      default:
        borderWidth = '1px';
        textDecoration = 'none';
        break;
    }

    switch (inputs.variant) {
      case 'solid':
        borderColor = schemeColor['700'];
        backgroundColor = schemeColor['700'];
        color = 'white';
        backgroundColorHover = schemeColor['800'];
        break;
      case 'outline':
        borderColor = schemeColor['700'];
        backgroundColor = 'transparent';
        color = schemeColor['700'];
        backgroundColorHover = schemeColor['50'];
        break;
      case 'ghost':
        borderColor = 'transparent';
        backgroundColor = 'transparent';
        color = schemeColor['700'];
        backgroundColorHover = schemeColor['50'];
        break;
      case 'link':
        borderColor = 'transparent';
        backgroundColor = 'transparent';
        color = schemeColor['700'];
        backgroundColorHover = 'transparent';
        break;
      case 'input':
        borderColor = schemeColor['500'];
        backgroundColor = schemeColor['100'];
        color = schemeColor['800'];
        backgroundColorHover = schemeColor['200'];
        break;
      default:
        borderColor = schemeColor['700'];
        backgroundColor = schemeColor['700'];
        color = 'white';
        backgroundColorHover = schemeColor['800'];
        break;
    }

    const button = css`
      font-size: ${fontSize} !important;
      font-weight: 400 !important;
      padding: ${padding};
      border-width: ${borderWidth};
      border-color: ${borderColor};
      background-color: ${backgroundColor};
      color: ${color};

      &:hover {
        color: ${color};
        text-decoration: ${textDecoration};
        background-color: ${backgroundColorHover};
      }

      &:hover:disabled {
        background-color: ${backgroundColor};
      }

      ${inputs.variant !== 'link' &&
      css`
        &:focus-within {
          box-shadow: inset 0 0 0 1px ${yellow500};
        }
      `}

      ${inputs.variant === 'link' &&
      css`
        &:focus-within {
          box-shadow: none;
        }
      `}

      .t-button-loading {
        color: ${color};
        margin-right: ${!inputs.isIconOnly ? '8px' : '0'};
      }
    `;

    return { button };
  }
}
