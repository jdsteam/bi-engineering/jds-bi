import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { JdsBiButtonComponent } from './button.component';

@NgModule({
  declarations: [JdsBiButtonComponent],
  imports: [CommonModule],
  exports: [JdsBiButtonComponent],
})
export class JdsBiButtonModule {}
