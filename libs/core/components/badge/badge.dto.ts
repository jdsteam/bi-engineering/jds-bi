export const JdsBiBadgeVariant = ['solid', 'outline', 'subtle'] as const;

export type JdsBiBadgeVariant = typeof JdsBiBadgeVariant[number];

export interface JdsBiBadgeInputs {
  colorScheme: string;
  variant: string;
  isRounded: boolean;
}

export interface JdsBiBadgeStyles {
  badge: string;
}
