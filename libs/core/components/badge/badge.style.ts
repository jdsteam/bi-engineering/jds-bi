import { Injectable } from '@angular/core';
import { jdsBiColorScheme } from '@jds-bi/cdk';
import { css } from '@emotion/css';

import type { JdsBiBadgeInputs, JdsBiBadgeStyles } from './badge.dto';

@Injectable()
export class JdsBiBadgeStyle {
  getColorScheme(value: string) {
    const colorScheme = jdsBiColorScheme(value);
    const { color: schemeColor } = colorScheme;

    return { schemeColor };
  }

  getStyle(inputs: JdsBiBadgeInputs): JdsBiBadgeStyles {
    const { schemeColor } = this.getColorScheme(inputs.colorScheme);

    let backgroundColor: string;
    let color: string;
    let borderColor: string;
    let borderRadius: string;

    switch (inputs.variant) {
      case 'solid':
        backgroundColor = schemeColor['700'];
        color = 'white';
        borderColor = schemeColor['700'];
        break;
      case 'outline':
        backgroundColor = 'white';
        color = schemeColor['700'];
        borderColor = schemeColor['700'];
        break;
      case 'subtle':
        backgroundColor = schemeColor['50'];
        color = schemeColor['700'];
        borderColor = schemeColor['50'];
        break;
      default:
        backgroundColor = schemeColor['700'];
        color = 'white';
        borderColor = schemeColor['700'];
        break;
    }

    if (inputs.isRounded) {
      borderRadius = '50rem';
    } else {
      borderRadius = '8px';
    }

    const badge = css`
      &.t-badge {
        display: inline-flex;
        white-space: nowrap;
        vertical-align: middle;
        border-width: 1px;
        border-style: solid;
        border-color: ${borderColor};
        border-radius: ${borderRadius};
        font-size: 12px;
        font-weight: 500;
        color: ${color};
        background-color: ${backgroundColor};
        line-height: 18px;
        padding: 4px 8px;
      }
    `;

    return { badge };
  }
}
