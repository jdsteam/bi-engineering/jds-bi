import { NgModule } from '@angular/core';

import { JdsBiBadgeDirective } from './badge.directive';

@NgModule({
  declarations: [JdsBiBadgeDirective],
  exports: [JdsBiBadgeDirective],
})
export class JdsBiBadgeModule {}
