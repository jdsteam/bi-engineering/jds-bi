import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import type { JdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiActiveZoneDirective,
  JdsBiContextWithImplicit,
  jdsBiDefaultProp,
} from '@jds-bi/cdk';
import { PolymorpheusContent } from '@tinkoff/ng-polymorpheus';

import type {
  JdsBiBadgeVariant,
  JdsBiBadgeInputs,
  JdsBiBadgeStyles,
} from './badge.dto';
import { JdsBiBadgeStyle } from './badge.style';

@Directive({
  selector: '[jdsBiBadge]',
  providers: [JdsBiBadgeStyle],
})
export class JdsBiBadgeDirective implements OnInit, OnChanges {
  @Input()
  @jdsBiDefaultProp()
  colorScheme: JdsBiColor = 'green';

  @Input()
  @jdsBiDefaultProp()
  variant: JdsBiBadgeVariant = 'solid';

  @Input()
  @jdsBiDefaultProp()
  isRounded = false;

  @Input('jdsBiBadge')
  @jdsBiDefaultProp()
  content: PolymorpheusContent<
    JdsBiContextWithImplicit<JdsBiActiveZoneDirective>
  > = '';

  // Variable
  className!: JdsBiBadgeStyles;

  constructor(
    private _elRef: ElementRef,
    private jdsBiBadgeStyle: JdsBiBadgeStyle,
  ) {}

  @HostBinding('class.t-badge')
  get hostBadge() {
    return true;
  }

  ngOnInit(): void {
    this.getClassnames({
      colorScheme: this.colorScheme,
      variant: this.variant,
      isRounded: this.isRounded,
    });
  }

  ngOnChanges(): void {
    if (!this.content) {
      this.getClassnames({
        colorScheme: this.colorScheme,
        variant: this.variant,
        isRounded: this.isRounded,
      });
    }
  }

  getClassnames(input: JdsBiBadgeInputs): void {
    if (this.className) {
      this._elRef.nativeElement.classList.remove(this.className.badge);
    }

    this.className = this.jdsBiBadgeStyle.getStyle(input);
    this._elRef.nativeElement.classList.add(this.className.badge);
  }
}
