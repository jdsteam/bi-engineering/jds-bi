import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Inject,
  Input,
  Optional,
  Output,
  ViewChild,
} from '@angular/core';
import {
  JdsBiActiveZoneDirective,
  jdsBiAsFocusableItemAccessor,
  JdsBiContextWithImplicit,
  jdsBiDefaultProp,
  JdsBiFocusableElementAccessor,
  jdsBiGetClosestFocusable,
  jdsBiIsElement,
  jdsBiIsElementEditable,
  jdsBiIsHTMLElement,
  jdsBiIsNativeFocusedIn,
  jdsBiIsNativeKeyboardFocusable,
  JdsBiNativeFocusableElement,
} from '@jds-bi/cdk';
import {
  JdsBiDropdownDirective,
  JdsBiDropdownHoverDirective,
} from '@jds-bi/core/directives/dropdown';
import { jdsBiIsEditingKey } from '@jds-bi/core/utils/miscellaneous';
import { PolymorpheusContent } from '@tinkoff/ng-polymorpheus';
import { asyncScheduler, BehaviorSubject, EMPTY, scheduled } from 'rxjs';
import { distinctUntilChanged, mergeAll, skip } from 'rxjs/operators';

import { JdsBiHostedDropdownConnectorDirective } from './hosted-dropdown-connector.directive';

export interface JdsBiHostedDropdownContext
  extends JdsBiContextWithImplicit<JdsBiActiveZoneDirective> {
  close(): void;
}

/* eslint-disable @typescript-eslint/member-ordering */
@Component({
  selector: 'jds-bi-hosted-dropdown',
  templateUrl: './hosted-dropdown.template.html',
  styleUrls: ['./hosted-dropdown.style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [jdsBiAsFocusableItemAccessor(JdsBiHostedDropdownComponent)],
})
export class JdsBiHostedDropdownComponent
  implements JdsBiFocusableElementAccessor
{
  @ContentChild(JdsBiHostedDropdownConnectorDirective, { read: ElementRef })
  private readonly dropdownHost?: ElementRef<HTMLElement>;

  @ViewChild('wrapper', { read: ElementRef })
  private readonly wrapper?: ElementRef<HTMLDivElement>;

  @ViewChild(JdsBiDropdownDirective)
  private readonly dropdownDirective?: JdsBiDropdownDirective;

  // /** TODO: rename in 4.0 */
  readonly openChange = new BehaviorSubject(false);

  @ViewChild(JdsBiActiveZoneDirective)
  readonly activeZone!: JdsBiActiveZoneDirective;

  @Input()
  @jdsBiDefaultProp()
  content: PolymorpheusContent<JdsBiHostedDropdownContext> = '';

  @Input()
  @jdsBiDefaultProp()
  sided = false;

  @Input()
  @jdsBiDefaultProp()
  canOpen = true;

  // eslint-disable-next-line @angular-eslint/no-output-rename
  @Output('openChange')
  readonly open$ = scheduled(
    [this.openChange, this.hover$ || EMPTY],
    asyncScheduler,
  ).pipe(mergeAll(), skip(1), distinctUntilChanged());

  @Output()
  readonly focusedChange = new EventEmitter<boolean>();

  readonly context!: JdsBiContextWithImplicit<JdsBiActiveZoneDirective>;

  constructor(
    @Optional()
    @Inject(JdsBiDropdownHoverDirective)
    private readonly hover$: JdsBiDropdownHoverDirective | null,
    @Inject(ElementRef) private readonly elementRef: ElementRef,
  ) {}

  @Input()
  @jdsBiDefaultProp()
  set open(open: boolean) {
    this.openChange.next(open);
  }

  get open(): boolean {
    return this.openChange.value;
  }

  get host(): HTMLElement {
    return this.dropdownHost?.nativeElement || this.elementRef.nativeElement;
  }

  get computedHost(): HTMLElement {
    return (
      this.dropdownHost?.nativeElement ||
      this.nativeFocusableElement ||
      this.elementRef.nativeElement
    );
  }

  get dropdown(): HTMLElement | undefined {
    return this.dropdownDirective?.dropdownBoxRef?.location.nativeElement;
  }

  get nativeFocusableElement(): JdsBiNativeFocusableElement | null {
    return jdsBiIsNativeKeyboardFocusable(this.host)
      ? this.host
      : jdsBiGetClosestFocusable({
          initial: this.host,
          root: this.elementRef.nativeElement,
        });
  }

  @HostBinding('class._hosted_dropdown_focused')
  get focused(): boolean {
    return (
      jdsBiIsNativeFocusedIn(this.host) ||
      (this.open &&
        !!this.wrapper &&
        jdsBiIsNativeFocusedIn(this.wrapper.nativeElement))
    );
  }

  @HostListener('focusin', ['$event.target'])
  onFocusIn(target: HTMLElement): void {
    if (!this.computedHost.contains(target)) {
      this.updateOpen(false);
    }
  }

  @HostListener('click', ['$event.target'])
  onClick(target: HTMLElement): void {
    if (
      !this.hostEditable &&
      this.computedHost.contains(target) &&
      !this.hover$?.hovered
    ) {
      this.updateOpen(!this.open);
    }
  }

  @HostListener('keydown.esc', ['$event'])
  onKeyDownEsc(event: Event): void {
    if (!this.canOpen || !this.open) {
      return;
    }

    event.stopPropagation();
    this.closeDropdown();
  }

  @HostListener('keydown.arrowDown', ['$event', 'true'])
  @HostListener('keydown.arrowUp', ['$event', 'false'])
  onArrow(event: KeyboardEvent, down: boolean): void {
    this.focusDropdown(event, down);
  }

  onKeydown({ key, target, defaultPrevented }: KeyboardEvent): void {
    if (
      !defaultPrevented &&
      jdsBiIsEditingKey(key) &&
      this.hostEditable &&
      jdsBiIsHTMLElement(target) &&
      !jdsBiIsElementEditable(target)
    ) {
      this.focusHost();
    }
  }

  onActiveZone(active: boolean): void {
    this.updateFocused(active);

    if (!active) {
      this.updateOpen(false);
    }
  }

  onHostObscured(obscured: boolean): void {
    if (obscured) {
      this.closeDropdown();
    }
  }

  updateOpen(open: boolean): void {
    if (!open || this.canOpen) {
      this.open = open;
    }
  }

  readonly close = (): void => this.updateOpen(false);

  private get hostEditable(): boolean {
    return jdsBiIsElementEditable(this.computedHost);
  }

  private focusDropdown(event: KeyboardEvent, first: boolean): void {
    const host = this.nativeFocusableElement;

    if (
      !host ||
      !jdsBiIsHTMLElement(host) ||
      !jdsBiIsElement(event.target) ||
      !host.contains(event.target)
    ) {
      return;
    }

    if (
      !this.wrapper ||
      !this.open ||
      !this.dropdown ||
      !jdsBiIsHTMLElement(this.wrapper.nativeElement.nextElementSibling)
    ) {
      this.updateOpen(true);

      if (!jdsBiIsElementEditable(host)) {
        event.preventDefault();
      }

      return;
    }

    const initial = first
      ? this.wrapper.nativeElement
      : this.wrapper.nativeElement.nextElementSibling;
    const focusable = jdsBiGetClosestFocusable({
      initial,
      root: this.wrapper.nativeElement,
      previous: !first,
    });

    if (!focusable) {
      return;
    }

    focusable.focus();
    event.preventDefault();
  }

  private closeDropdown(): void {
    if (this.focused) {
      this.focusHost();
    }

    this.updateOpen(false);
  }

  private focusHost(): void {
    const host = this.nativeFocusableElement;

    if (host) {
      host.focus({ preventScroll: true });
    }
  }

  private updateFocused(focused: boolean): void {
    this.focusedChange.emit(focused);
  }
}
