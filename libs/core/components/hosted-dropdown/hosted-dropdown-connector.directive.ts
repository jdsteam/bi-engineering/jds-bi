import { Directive } from '@angular/core';

@Directive({
  selector: '[jdsBiHostedDropdownHost]',
})
export class JdsBiHostedDropdownConnectorDirective {}
