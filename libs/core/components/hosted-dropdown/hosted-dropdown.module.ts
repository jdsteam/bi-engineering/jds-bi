import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  JdsBiActiveZoneModule,
  JdsBiLetModule,
  JdsBiObscuredModule,
} from '@jds-bi/cdk';
import {
  JdsBiDropdownModule,
  JdsBiDropdownOptionsDirective,
} from '@jds-bi/core/directives/dropdown';
import { PolymorpheusModule } from '@tinkoff/ng-polymorpheus';

import { JdsBiHostedDropdownComponent } from './hosted-dropdown.component';
import { JdsBiHostedDropdownConnectorDirective } from './hosted-dropdown-connector.directive';

@NgModule({
  imports: [
    CommonModule,
    PolymorpheusModule,
    JdsBiLetModule,
    JdsBiObscuredModule,
    JdsBiActiveZoneModule,
    JdsBiDropdownModule,
  ],
  declarations: [
    JdsBiHostedDropdownComponent,
    JdsBiHostedDropdownConnectorDirective,
  ],
  exports: [
    JdsBiHostedDropdownComponent,
    JdsBiHostedDropdownConnectorDirective,
    JdsBiDropdownOptionsDirective,
  ],
})
export class JdsBiHostedDropdownModule {}
