export type JdsBiHorizontalDirection = 'left' | 'right';

export type JdsBiVerticalDirection = 'bottom' | 'top';
