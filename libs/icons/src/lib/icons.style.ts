import { Injectable } from '@angular/core';
import { css } from '@emotion/css';

import type { JdsBiIconsInputs, JdsBiIconsStyles } from './icons.interface';

@Injectable()
export class JdsBiIconsStyle {
  getStyle(inputs: JdsBiIconsInputs): JdsBiIconsStyles {
    const icon = css`
      svg.icon {
        width: ${inputs.size}px;
        height: ${inputs.size}px;
        fill: ${inputs.color};
      }
    `;

    return { icon };
  }
}
