import { Injectable } from '@angular/core';
import { JdsBiIcons } from './icons.interface';

@Injectable({
  providedIn: 'root',
})
export class JdsBiIconsService {
  private registry = new Map<string, string>();

  public registerIcons(icons: JdsBiIcons[]): void {
    icons.forEach((icon: JdsBiIcons) =>
      this.registry.set(icon.name, icon.data),
    );
  }

  public getIcon(iconName: string): string | undefined {
    if (!this.registry.has(iconName)) {
      console.warn(
        `We could not find the JDS Icon with the name ${iconName}, did you add it to the Icon registry?`,
      );
    }
    return this.registry.get(iconName);
  }
}
