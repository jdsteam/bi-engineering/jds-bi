import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { JdsBiIconsComponent } from './icons.component';

@NgModule({
  declarations: [JdsBiIconsComponent],
  imports: [CommonModule],
  exports: [JdsBiIconsComponent],
})
export class JdsBiIconsModule {}
