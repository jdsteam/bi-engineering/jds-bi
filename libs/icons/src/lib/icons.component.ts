import {
  Component,
  ElementRef,
  Input,
  Inject,
  Optional,
  ChangeDetectionStrategy,
  OnInit,
  OnChanges,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import { JdsBiIconsInputs, JdsBiIconsStyles } from './icons.interface';
import { JdsBiIconsService } from './icons.service';
import { JdsBiIconsStyle } from './icons.style';

@Component({
  selector: 'jds-bi-icon',
  template: '<ng-content></ng-content>',
  styleUrls: ['./icons.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [JdsBiIconsStyle],
})
export class JdsBiIconsComponent implements OnInit, OnChanges {
  @Input()
  set name(iconName: string) {
    if (this.svgIcon) {
      this._elRef.nativeElement.removeChild(this.svgIcon);
    }
    const svgData = this.jdsBiIconsService.getIcon(iconName);
    this.svgIcon = this.svgElementFromString(svgData || '');
    this._elRef.nativeElement.appendChild(this.svgIcon);
  }

  @Input()
  @jdsBiDefaultProp()
  size = 14;

  @Input()
  @jdsBiDefaultProp()
  color = 'currentColor';

  private svgIcon!: SVGElement;

  // Variable
  className!: JdsBiIconsStyles;

  constructor(
    private _elRef: ElementRef,
    private jdsBiIconsService: JdsBiIconsService,
    private jdsBiIconsStyle: JdsBiIconsStyle,
    @Optional() @Inject(DOCUMENT) private document: any,
  ) {}

  ngOnInit(): void {
    this.getClassnames({
      size: this.size,
      color: this.color,
    });
  }

  ngOnChanges(): void {
    this.getClassnames({
      size: this.size,
      color: this.color,
    });
  }

  getClassnames(input: JdsBiIconsInputs): void {
    if (this.className) {
      this._elRef.nativeElement.classList.remove(this.className.icon);
    }

    this.className = this.jdsBiIconsStyle.getStyle(input);
    this._elRef.nativeElement.classList.add(this.className.icon);
  }

  private svgElementFromString(svgContent: string): SVGElement {
    const div = this.document.createElement('DIV');
    div.innerHTML = svgContent;
    return (
      div.querySelector('svg') ||
      this.document.createElementNS('http://www.w3.org/2000/svg', 'path')
    );
  }
}
