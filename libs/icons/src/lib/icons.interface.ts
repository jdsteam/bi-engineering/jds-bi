export const JdsBiIconsName = [
  'arrow-right',
  'arrow-up',
  'arrow-up-right-from-square',
  'ban',
  'bars',
  'bell',
  'book',
  'box-archive',
  'building',
  'building-columns',
  'briefcase',
  'calendar',
  'cabinet-filing',
  'chart-simple',
  'check',
  'check-double',
  'chevron-down',
  'chevron-left',
  'chevron-right',
  'circle-check',
  'circle-down',
  'circle-info',
  'circle-plus',
  'circle-question',
  'circle-xmark',
  'clock-rotate-left',
  'clock-three',
  'cloud-arrow-down',
  'comments',
  'database',
  'download',
  'ellipsis',
  'envelope',
  'eye',
  'eye-slash',
  'file-timer',
  'globe',
  'location-dot',
  'magnifying-glass',
  'map-location',
  'pencil',
  'phone',
  'plus',
  'star',
  'trash',
  'user-lock',
  'whatsapp',
  'xmark',
] as const;

export type JdsBiIconsName = typeof JdsBiIconsName[number];

export interface JdsBiIcons {
  name: JdsBiIconsName;
  data: string;
}

export interface JdsBiIconsInputs {
  size: number;
  color: string;
}

export interface JdsBiIconsStyles {
  icon: string;
}
