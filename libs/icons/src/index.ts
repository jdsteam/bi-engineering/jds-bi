export * from './lib/icons';
export * from './lib/icons.component';
export * from './lib/icons.interface';
export * from './lib/icons.module';
export * from './lib/icons.service';
