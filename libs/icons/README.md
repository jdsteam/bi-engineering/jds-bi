# Angular Icons

## Versions

| Angular  | Components |
| -------- | :--------: |
| >=18.0.0 |    v5.x    |
| >=17.0.0 |    v4.x    |
| >=16.0.0 |    v3.x    |
| >=15.0.0 |    v2.x    |
