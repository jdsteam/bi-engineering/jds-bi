export const jdsBiColor = {
  gray: {
    50: '#fafafa',
    100: '#f5f5f5',
    200: '#eee',
    300: '#e0e0e0',
    400: '#bdbdbd',
    500: '#9e9e9e',
    600: '#757575',
    700: '#616161',
    800: '#424242',
    900: '#212121',
  },
  yellow: {
    50: '#fff9e1',
    100: '#ffeeb4',
    200: '#ffe483',
    300: '#ffda4f',
    400: '#ffd026',
    500: '#ffc800',
    600: '#ffb900',
    700: '#ffa600',
    800: '#ff9500',
    900: '#ff7500',
  },
  green: {
    50: '#e6f6ec',
    100: '#c3e9d0',
    200: '#9bdbb3',
    300: '#70cd94',
    400: '#4dc27e',
    500: '#1fb767',
    600: '#16a75c',
    700: '#069550',
    800: '#008444',
    900: '#006430',
  },
  blue: {
    50: '#e3f2fd',
    100: '#bbdefb',
    200: '#90caf9',
    300: '#64b5f6',
    400: '#42a5f5',
    500: '#2196f3',
    600: '#1e88e5',
    700: '#1976d2',
    800: '#1565c0',
    900: '#0d47a1',
  },
  pink: {
    50: '#ffe6ec',
    100: '#ffbfcf',
    200: '#ff96af',
    300: '#ff6c8f',
    400: '#fd355f',
    500: '#fd355f',
    600: '#ec305d',
    700: '#d62a59',
    800: '#c12357',
    900: '#9d1951',
  },
  red: {
    50: '#ffebee',
    100: '#ffcdd2',
    200: '#ef9a9a',
    300: '#e57373',
    400: '#ef5350',
    500: '#f44336',
    600: '#e53935',
    700: '#d32f2f',
    800: '#c62828',
    900: '#b71b1c',
  },
  purple: {
    50: '#f3e5f5',
    100: '#e1bee7',
    200: '#ce93d8',
    300: '#ba68c8',
    400: '#ab47bc',
    500: '#9b27b0',
    600: '#8d24aa',
    700: '#7a1fa2',
    800: '#691b9a',
    900: '#49148c',
  },
  'blue-gray': {
    50: '#e3e7ed',
    100: '#b9c3d3',
    200: '#8d9db5',
    300: '#627798',
    400: '#415c84',
    500: '#1a4373',
    600: '#133c6b',
    700: '#083461',
    800: '#022b55',
    900: '#001b3d',
  },
};
