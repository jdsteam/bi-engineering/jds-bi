import {
  animateChild,
  query,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const JDS_BI_PARENT_ANIMATION = trigger(`jdsBiParentAnimation`, [
  transition(`* => void`, [
    style({ overflow: `hidden` }),
    query(`:scope > *`, [animateChild()], { optional: true }),
  ]),
]);
