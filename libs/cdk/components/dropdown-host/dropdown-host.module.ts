import { NgModule } from '@angular/core';

import { JdsBiDropdownHostComponent } from './dropdown-host.component';

@NgModule({
  declarations: [JdsBiDropdownHostComponent],
  exports: [JdsBiDropdownHostComponent],
})
export class JdsBiDropdownHostModule {}
