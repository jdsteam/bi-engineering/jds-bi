import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  ViewChild,
} from '@angular/core';
import {
  AbstractJdsBiPortalHostComponent,
  AbstractJdsBiPortalService,
} from '@jds-bi/cdk/abstract';
import { EMPTY_CLIENT_RECT } from '@jds-bi/cdk/constants';

import { JdsBiDropdownPortalService } from './dropdown-portal.service';

/**
 * Host element for dynamically created portals, for example using {@link JdsBiDropdownDirective}.
 */
@Component({
  selector: 'jds-bi-dropdown-host',
  templateUrl: './dropdown-host.template.html',
  styleUrls: ['./dropdown-host.style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: AbstractJdsBiPortalService,
      useExisting: JdsBiDropdownPortalService,
    },
    {
      provide: AbstractJdsBiPortalHostComponent,
      useExisting: JdsBiDropdownHostComponent,
    },
  ],
})
export class JdsBiDropdownHostComponent extends AbstractJdsBiPortalHostComponent {
  @ViewChild('positionFixedOffset')
  private readonly positionFixedOffsetRef?: ElementRef<HTMLDivElement>;

  fixedPositionOffset(): ClientRect {
    return (
      this.positionFixedOffsetRef?.nativeElement.getBoundingClientRect() ||
      EMPTY_CLIENT_RECT
    );
  }
}
