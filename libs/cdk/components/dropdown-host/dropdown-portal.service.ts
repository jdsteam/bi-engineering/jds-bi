import { Injectable } from '@angular/core';
import { AbstractJdsBiPortalService } from '@jds-bi/cdk/abstract';

/**
 * Service for displaying dropdown portals
 */
@Injectable({
  providedIn: `root`,
})
export class JdsBiDropdownPortalService extends AbstractJdsBiPortalService {}
