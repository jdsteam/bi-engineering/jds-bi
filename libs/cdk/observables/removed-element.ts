import { ɵAnimationEngine as AnimationEngine } from '@angular/animations/browser';
import { inject, InjectFlags, InjectionToken } from '@angular/core';
import { BehaviorSubject, Observable, timer } from 'rxjs';
import { map, share, startWith, switchMap } from 'rxjs/operators';

export const JDS_BI_REMOVED_ELEMENT = new InjectionToken<
  Observable<Element | null>
>(
  `[JDS_BI_REMOVED_ELEMENT]: Element currently being removed by AnimationEngine`,
  {
    factory: () => {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      const stub = { onRemovalComplete: () => {} };
      const element$ = new BehaviorSubject<Element | null>(null);
      const engine = inject(AnimationEngine, InjectFlags.Optional) || stub;
      const { onRemovalComplete = stub.onRemovalComplete } = engine;

      engine.onRemovalComplete = (element, context) => {
        element$.next(element);
        onRemovalComplete(element, context);
      };

      return element$.pipe(
        switchMap((element) =>
          timer(0).pipe(
            map(() => null),
            startWith(element),
          ),
        ),
        share(),
      );
    },
  },
);
