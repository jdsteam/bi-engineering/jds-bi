import { JdsBiEventWith, JdsBiTypedEventTarget } from '@jds-bi/cdk/types';
import { fromEvent, Observable } from 'rxjs';

export function jdsBiTypedFromEvent<E extends keyof WindowEventMap>(
  target: Window,
  event: E,
  options?: AddEventListenerOptions,
): Observable<JdsBiEventWith<WindowEventMap[E], typeof target>>;

export function jdsBiTypedFromEvent<E extends keyof DocumentEventMap>(
  target: Document,
  event: E,
  options?: AddEventListenerOptions,
): Observable<JdsBiEventWith<DocumentEventMap[E], typeof target>>;

export function jdsBiTypedFromEvent<
  T extends Element,
  E extends keyof HTMLElementEventMap,
>(
  target: T,
  event: E,
  options?: AddEventListenerOptions,
): Observable<JdsBiEventWith<HTMLElementEventMap[E], typeof target>>;

export function jdsBiTypedFromEvent<
  E extends Event,
  T extends JdsBiTypedEventTarget<JdsBiEventWith<E, T>>,
>(
  target: T,
  event: string,
  options?: AddEventListenerOptions,
): Observable<JdsBiEventWith<E, T>>;

export function jdsBiTypedFromEvent<E extends Event>(
  target: JdsBiTypedEventTarget<E>,
  event: string,
  options?: AddEventListenerOptions,
): Observable<E>;

export function jdsBiTypedFromEvent<E extends Event>(
  target: JdsBiTypedEventTarget<E>,
  event: string,
  options: AddEventListenerOptions = {},
): Observable<E> {
  /**
   * @note:
   * in RxJS 7 type signature `JdsBiTypedEventTarget<E>` !== `HasEventTargetAddRemove<E>`
   */
  return fromEvent<E>(
    target as any,
    event,
    options,
  ) as unknown as Observable<E>;
}
