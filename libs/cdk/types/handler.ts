export type JdsBiHandler<T, G> = (item: T) => G;
export type JdsBiBooleanHandler<T> = JdsBiHandler<T, boolean>;
export type JdsBiStringHandler<T> = JdsBiHandler<T, string>;
export type JdsBiNumberHandler<T> = JdsBiHandler<T, number>;
