import type { InjectionToken } from '@angular/core';

export type JdsBiInjectionTokenType<Token> = Token extends InjectionToken<
  infer T
>
  ? T
  : never;
