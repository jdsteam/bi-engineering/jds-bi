export const JdsBiColor = [
  'gray',
  'yellow',
  'green',
  'blue',
  'pink',
  'red',
  'purple',
  'blue-gray',
] as const;

export type JdsBiColor = typeof JdsBiColor[number];
