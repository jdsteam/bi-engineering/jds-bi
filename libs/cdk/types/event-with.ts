export interface JdsBiTypedEventTarget<E> {
  addEventListener(
    type: string,
    listener: ((evt: E) => void) | null,
    options?: AddEventListenerOptions | boolean,
  ): void;
  removeEventListener(
    type: string,
    listener?: ((evt: E) => void) | null,
    options?: EventListenerOptions | boolean,
  ): void;
}

/**
 * Wrapper around {@link Event} to add typings to target and currentTarget.
 */
export type JdsBiEventWith<
  G extends Event,
  T extends JdsBiTypedEventTarget<G>,
> = G & {
  readonly currentTarget: T;
};
