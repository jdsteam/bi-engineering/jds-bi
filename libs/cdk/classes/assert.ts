import { EMPTY_FUNCTION } from '@jds-bi/cdk/constants';

export const jdsBiAssert = {
  enabled: false,
  get assert(): (assertion: boolean, ...args: unknown[]) => void {
    return this.enabled
      ? Function.prototype.bind.call(console.assert, console)
      : EMPTY_FUNCTION;
  },
};
