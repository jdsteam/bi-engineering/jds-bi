export function jdsBiIsInput(element: Element): element is HTMLInputElement {
  return element.matches(`input`);
}

export function jdsBiIsTextarea(
  element: Element,
): element is HTMLTextAreaElement {
  return element.matches(`textarea`);
}

export function jdsBiIsTextfield(
  element: Element,
): element is HTMLInputElement | HTMLTextAreaElement {
  return jdsBiIsInput(element) || jdsBiIsTextarea(element);
}

export function jdsBiIsElement(
  node?: Element | EventTarget | Node | null,
): node is Element {
  return !!node && `nodeType` in node && node.nodeType === Node.ELEMENT_NODE;
}

export function jdsBiIsHTMLElement(node: unknown): node is HTMLElement {
  // TODO: iframe warning
  return node instanceof HTMLElement;
}

export function jdsBiIsTextNode(node: Node): node is Text {
  return node.nodeType === Node.TEXT_NODE;
}
