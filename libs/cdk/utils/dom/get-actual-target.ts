/**
 * Gets actual target from open Shadow DOM if event happened within it
 */
export function jdsBiGetActualTarget(event: Event): Node {
  return event.composedPath()[0] as Node;
}
