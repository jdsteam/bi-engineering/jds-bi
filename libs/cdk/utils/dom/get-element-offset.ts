import { jdsBiAssert } from '@jds-bi/cdk/classes';

import { jdsBiIsHTMLElement } from './element-checks';

/**
 * Calculates offset for an element relative to it's parent several levels above
 *
 * @param host parent element
 * @param element
 * @return object with offsetTop and offsetLeft number properties
 */
export function jdsBiGetElementOffset(
  host: Element,
  element: HTMLElement,
): { offsetTop: number; offsetLeft: number } {
  jdsBiAssert.assert(host.contains(element), `Host must contain element`);

  let { offsetTop, offsetLeft, offsetParent } = element;

  while (jdsBiIsHTMLElement(offsetParent) && offsetParent !== host) {
    offsetTop += offsetParent.offsetTop;
    offsetLeft += offsetParent.offsetLeft;
    offsetParent = offsetParent.offsetParent;
  }

  return { offsetTop, offsetLeft };
}
