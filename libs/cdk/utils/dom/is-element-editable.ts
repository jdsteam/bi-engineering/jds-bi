import { jdsBiIsTextfield } from './element-checks';

export function jdsBiIsElementEditable(element: HTMLElement): boolean {
  return (
    (jdsBiIsTextfield(element) && !element.readOnly) ||
    element.isContentEditable
  );
}
