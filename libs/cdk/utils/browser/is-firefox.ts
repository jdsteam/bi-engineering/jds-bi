export function jdsBiIsFirefox(userAgent: string): boolean {
  return userAgent.toLowerCase().includes(`firefox`);
}
