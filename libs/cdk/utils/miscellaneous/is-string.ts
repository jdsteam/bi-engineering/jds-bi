export function jdsBiIsString(value: unknown): value is string {
  return typeof value === `string`;
}
