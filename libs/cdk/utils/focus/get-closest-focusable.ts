import { svgNodeFilter } from '@jds-bi/cdk/constants';
import { jdsBiIsHTMLElement } from '@jds-bi/cdk/utils/dom';

import { jdsBiIsNativeKeyboardFocusable } from './is-native-keyboard-focusable';
import { jdsBiIsNativeMouseFocusable } from './is-native-mouse-focusable';

export interface JdsBiGetClosestFocusableOptions {
  /**
   * @description:
   * current HTML element
   */
  initial: Element;

  /**
   * @description:
   * top Node limiting the search area
   */
  root: Node;

  /**
   * @description:
   * should it look backwards instead (find item that will be focused with Shift + Tab)
   */
  previous?: boolean;

  /**
   * @description:
   * determine if only keyboard focus is of interest
   */
  keyboard?: boolean;
}

/**
 * @description:
 * Finds the closest element that can be focused with a keyboard or mouse in theory
 */
export function jdsBiGetClosestFocusable({
  initial,
  root,
  previous = false,
  keyboard = true,
}: JdsBiGetClosestFocusableOptions): HTMLElement | null {
  if (!root.ownerDocument) {
    return null;
  }

  const check = keyboard
    ? jdsBiIsNativeKeyboardFocusable
    : jdsBiIsNativeMouseFocusable;
  const treeWalker = root.ownerDocument.createTreeWalker(
    root,
    NodeFilter.SHOW_ELEMENT,
    svgNodeFilter,
  );

  treeWalker.currentNode = initial;

  while (previous ? treeWalker.previousNode() : treeWalker.nextNode()) {
    if (jdsBiIsHTMLElement(treeWalker.currentNode)) {
      initial = treeWalker.currentNode;
    }

    if (jdsBiIsHTMLElement(initial) && check(initial)) {
      return initial;
    }
  }

  return null;
}
