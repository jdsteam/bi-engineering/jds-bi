import { jdsBiIsNativeKeyboardFocusable } from './is-native-keyboard-focusable';

export function jdsBiIsNativeMouseFocusable(element: Element): boolean {
  return (
    !element.hasAttribute(`disabled`) &&
    (element.getAttribute(`tabIndex`) === `-1` ||
      jdsBiIsNativeKeyboardFocusable(element))
  );
}
