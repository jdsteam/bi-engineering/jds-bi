import { jdsBiColor } from '@jds-bi/cdk/constants';

export interface JdsBiColorScheme {
  primary: string;
  color: any;
  hexSVG: string;
}

export function jdsBiColorScheme(
  theme: string,
  isCustom = false,
): JdsBiColorScheme {
  let primary: string;
  let color: any;
  let hexSVG: string;

  if (isCustom) {
    primary = theme;
    color = {
      700: theme,
    };
    hexSVG = theme.replace('#', '%23');

    return {
      primary,
      color,
      hexSVG,
    };
  }

  switch (theme) {
    case 'green':
      primary = jdsBiColor.green['700'];
      color = {
        50: jdsBiColor.green['50'],
        100: jdsBiColor.green['100'],
        200: jdsBiColor.green['200'],
        300: jdsBiColor.green['300'],
        400: jdsBiColor.green['400'],
        500: jdsBiColor.green['500'],
        600: jdsBiColor.green['600'],
        700: jdsBiColor.green['700'],
        800: jdsBiColor.green['800'],
        900: jdsBiColor.green['900'],
      };
      hexSVG = jdsBiColor.green['700'].replace('#', '%23');
      break;
    case 'dark-green':
      primary = jdsBiColor.green['800'];
      color = {
        50: jdsBiColor.green['100'],
        100: jdsBiColor.green['200'],
        200: jdsBiColor.green['300'],
        300: jdsBiColor.green['400'],
        400: jdsBiColor.green['500'],
        500: jdsBiColor.green['600'],
        600: jdsBiColor.green['700'],
        700: jdsBiColor.green['800'],
        800: jdsBiColor.green['900'],
        900: jdsBiColor.green['900'],
      };
      hexSVG = jdsBiColor.green['800'].replace('#', '%23');
      break;
    case 'blue':
      primary = jdsBiColor.blue['700'];
      color = {
        50: jdsBiColor.blue['50'],
        100: jdsBiColor.blue['100'],
        200: jdsBiColor.blue['200'],
        300: jdsBiColor.blue['300'],
        400: jdsBiColor.blue['400'],
        500: jdsBiColor.blue['500'],
        600: jdsBiColor.blue['600'],
        700: jdsBiColor.blue['700'],
        800: jdsBiColor.blue['800'],
        900: jdsBiColor.blue['900'],
      };
      hexSVG = jdsBiColor.blue['700'].replace('#', '%23');
      break;
    case 'dark-blue':
      primary = jdsBiColor.blue['800'];
      color = {
        50: jdsBiColor.blue['100'],
        100: jdsBiColor.blue['200'],
        200: jdsBiColor.blue['300'],
        300: jdsBiColor.blue['400'],
        400: jdsBiColor.blue['500'],
        500: jdsBiColor.blue['600'],
        600: jdsBiColor.blue['700'],
        700: jdsBiColor.blue['800'],
        800: jdsBiColor.blue['900'],
        900: jdsBiColor.blue['900'],
      };
      hexSVG = jdsBiColor.blue['800'].replace('#', '%23');
      break;
    case 'purple':
      primary = jdsBiColor.purple['700'];
      color = {
        50: jdsBiColor.purple['50'],
        100: jdsBiColor.purple['100'],
        200: jdsBiColor.purple['200'],
        300: jdsBiColor.purple['300'],
        400: jdsBiColor.purple['400'],
        500: jdsBiColor.purple['500'],
        600: jdsBiColor.purple['600'],
        700: jdsBiColor.purple['700'],
        800: jdsBiColor.purple['800'],
        900: jdsBiColor.purple['900'],
      };
      hexSVG = jdsBiColor.purple['700'].replace('#', '%23');
      break;
    case 'dark-purple':
      primary = jdsBiColor.purple['800'];
      color = {
        50: jdsBiColor.purple['100'],
        100: jdsBiColor.purple['200'],
        200: jdsBiColor.purple['300'],
        300: jdsBiColor.purple['400'],
        400: jdsBiColor.purple['500'],
        500: jdsBiColor.purple['600'],
        600: jdsBiColor.purple['700'],
        700: jdsBiColor.purple['800'],
        800: jdsBiColor.purple['900'],
        900: jdsBiColor.purple['900'],
      };
      hexSVG = jdsBiColor.purple['800'].replace('#', '%23');
      break;
    case 'pink':
      primary = jdsBiColor.pink['700'];
      color = {
        50: jdsBiColor.pink['50'],
        100: jdsBiColor.pink['100'],
        200: jdsBiColor.pink['200'],
        300: jdsBiColor.pink['300'],
        400: jdsBiColor.pink['400'],
        500: jdsBiColor.pink['500'],
        600: jdsBiColor.pink['600'],
        700: jdsBiColor.pink['700'],
        800: jdsBiColor.pink['800'],
        900: jdsBiColor.pink['900'],
      };
      hexSVG = jdsBiColor.pink['700'].replace('#', '%23');
      break;
    case 'yellow':
      primary = jdsBiColor.yellow['700'];
      color = {
        50: jdsBiColor.yellow['50'],
        100: jdsBiColor.yellow['100'],
        200: jdsBiColor.yellow['200'],
        300: jdsBiColor.yellow['300'],
        400: jdsBiColor.yellow['400'],
        500: jdsBiColor.yellow['500'],
        600: jdsBiColor.yellow['600'],
        700: jdsBiColor.yellow['700'],
        800: jdsBiColor.yellow['800'],
        900: jdsBiColor.yellow['900'],
      };
      hexSVG = jdsBiColor.yellow['700'].replace('#', '%23');
      break;
    case 'red':
      primary = jdsBiColor.red['700'];
      color = {
        50: jdsBiColor.red['50'],
        100: jdsBiColor.red['100'],
        200: jdsBiColor.red['200'],
        300: jdsBiColor.red['300'],
        400: jdsBiColor.red['400'],
        500: jdsBiColor.red['500'],
        600: jdsBiColor.red['600'],
        700: jdsBiColor.red['700'],
        800: jdsBiColor.red['800'],
        900: jdsBiColor.red['900'],
      };
      hexSVG = jdsBiColor.red['700'].replace('#', '%23');
      break;
    case 'blue-gray':
      primary = jdsBiColor['blue-gray']['700'];
      color = {
        50: jdsBiColor['blue-gray']['50'],
        100: jdsBiColor['blue-gray']['100'],
        200: jdsBiColor['blue-gray']['200'],
        300: jdsBiColor['blue-gray']['300'],
        400: jdsBiColor['blue-gray']['400'],
        500: jdsBiColor['blue-gray']['500'],
        600: jdsBiColor['blue-gray']['600'],
        700: jdsBiColor['blue-gray']['700'],
        800: jdsBiColor['blue-gray']['800'],
        900: jdsBiColor['blue-gray']['900'],
      };
      hexSVG = jdsBiColor['blue-gray']['700'].replace('#', '%23');
      break;
    case 'gray':
      primary = jdsBiColor.gray['700'];
      color = {
        50: jdsBiColor.gray['50'],
        100: jdsBiColor.gray['100'],
        200: jdsBiColor.gray['200'],
        300: jdsBiColor.gray['300'],
        400: jdsBiColor.gray['400'],
        500: jdsBiColor.gray['500'],
        600: jdsBiColor.gray['600'],
        700: jdsBiColor.gray['700'],
        800: jdsBiColor.gray['800'],
        900: jdsBiColor.gray['900'],
      };
      hexSVG = jdsBiColor.gray['700'].replace('#', '%23');
      break;
    default:
      primary = jdsBiColor.green['700'];
      color = {
        50: jdsBiColor.green['50'],
        100: jdsBiColor.green['100'],
        200: jdsBiColor.green['200'],
        300: jdsBiColor.green['300'],
        400: jdsBiColor.green['400'],
        500: jdsBiColor.green['500'],
        600: jdsBiColor.green['600'],
        700: jdsBiColor.green['700'],
        800: jdsBiColor.green['800'],
        900: jdsBiColor.green['900'],
      };
      hexSVG = jdsBiColor.green['700'].replace('#', '%23');
      break;
  }

  return {
    primary,
    color,
    hexSVG,
  };
}
