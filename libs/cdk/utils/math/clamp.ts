import { jdsBiAssert } from '@jds-bi/cdk/classes';

/**
 * Clamps a value between two inclusive limits
 *
 * @param value
 * @param min lower limit
 * @param max upper limit
 */
export function jdsBiClamp(value: number, min: number, max: number): number {
  jdsBiAssert.assert(!Number.isNaN(value));
  jdsBiAssert.assert(!Number.isNaN(min));
  jdsBiAssert.assert(!Number.isNaN(max));
  jdsBiAssert.assert(max >= min);

  return Math.min(max, Math.max(min, value));
}
