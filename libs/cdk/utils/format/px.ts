import { jdsBiAssert } from '@jds-bi/cdk/classes';

/**
 * Adds 'px' to the number and turns it into a string
 */
export function jdsBiPx(value: number): string {
  jdsBiAssert.assert(Number.isFinite(value), `Value must be finite number`);

  return `${value}px`;
}
