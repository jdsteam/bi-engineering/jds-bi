import { jdsBiIsApplePlatform } from './is-apple-platform';

const IOS_REG_EXP = /ipad|iphone|ipod/;

export function jdsBiIsIos(navigator: Navigator): boolean {
  return (
    IOS_REG_EXP.test(navigator.userAgent.toLowerCase()) ||
    (jdsBiIsApplePlatform(navigator) && navigator.maxTouchPoints > 1)
  );
}
