import { ElementRef, Inject, Injectable, NgZone, Self } from '@angular/core';
import { ANIMATION_FRAME, WINDOW } from '@ng-web-apis/common';
import { POLLING_TIME } from '@jds-bi/cdk/constants';
import { jdsBiZoneOptimized } from '@jds-bi/cdk/observables';
import { jdsBiGetElementObscures } from '@jds-bi/cdk/utils/dom';
import { fromEvent, merge, Observable } from 'rxjs';
import {
  delay,
  distinctUntilChanged,
  map,
  startWith,
  takeUntil,
  throttleTime,
} from 'rxjs/operators';

import { JdsBiDestroyService } from './destroy.service';
import { JdsBiParentsScrollService } from './parents-scroll.service';

// @bad TODO: Consider Intersection Observer with fallback to current implementation
/**
 * Service that monitors element visibility by subscribing to scrolls
 * and polling with set interval, returns either null or an array
 * of elements that overlap given element edges
 */
@Injectable()
export class JdsBiObscuredService extends Observable<
  readonly Element[] | null
> {
  private readonly obscured$: Observable<readonly Element[] | null>;

  constructor(
    @Inject(JdsBiParentsScrollService)
    @Self()
    parentsScroll$: JdsBiParentsScrollService,
    @Inject(ElementRef) { nativeElement }: ElementRef<Element>,
    @Inject(NgZone) ngZone: NgZone,
    @Inject(WINDOW) windowRef: Window,
    @Self() @Inject(JdsBiDestroyService) destroy$: Observable<void>,
    @Inject(ANIMATION_FRAME) animationFrame$: Observable<number>,
  ) {
    super((subscriber) => this.obscured$.subscribe(subscriber));

    this.obscured$ = merge(
      // delay is added so it will not interfere with other listeners
      merge(parentsScroll$, fromEvent(windowRef, `resize`)).pipe(delay(0)),
      animationFrame$.pipe(throttleTime(POLLING_TIME)),
    ).pipe(
      map(() => jdsBiGetElementObscures(nativeElement)),
      startWith(null),
      distinctUntilChanged(),
      jdsBiZoneOptimized(ngZone),
      takeUntil(destroy$),
    );
  }
}
