import {
  Directive,
  Inject,
  Input,
  Optional,
  Output,
  Self,
} from '@angular/core';
import { jdsBiRequiredSetter } from '@jds-bi/cdk/decorators';
import { JdsBiActiveZoneDirective } from '@jds-bi/cdk/directives/active-zone';
import {
  JdsBiDestroyService,
  JdsBiObscuredService,
  JdsBiParentsScrollService,
} from '@jds-bi/cdk/services';
import { EMPTY, Observable, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

/**
 * Directive that monitors element visibility
 */
@Directive({
  selector: '[jdsBiObscured]',
  providers: [
    JdsBiObscuredService,
    JdsBiParentsScrollService,
    JdsBiDestroyService,
  ],
})
export class JdsBiObscuredDirective {
  private readonly enabled$ = new Subject<boolean>();

  @Input()
  @jdsBiRequiredSetter()
  set jdsBiObscuredEnabled(enabled: boolean) {
    this.enabled$.next(enabled);
  }

  @Output()
  readonly jdsBiObscured: Observable<boolean>;

  constructor(
    @Optional()
    @Inject(JdsBiActiveZoneDirective)
    activeZone: JdsBiActiveZoneDirective | null,
    @Self()
    @Inject(JdsBiObscuredService)
    obscured$: JdsBiObscuredService,
  ) {
    const mapped$ = obscured$.pipe(
      map(
        (obscuredBy) =>
          !!obscuredBy &&
          (!activeZone ||
            !obscuredBy.length ||
            obscuredBy.every((element) => !activeZone.contains(element))),
      ),
    );

    this.jdsBiObscured = this.enabled$.pipe(
      switchMap((enabled) => (enabled ? mapped$ : EMPTY)),
    );
  }
}
