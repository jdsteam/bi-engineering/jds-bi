import { NgModule } from '@angular/core';

import { JdsBiObscuredDirective } from './obscured.directive';

@NgModule({
  declarations: [JdsBiObscuredDirective],
  exports: [JdsBiObscuredDirective],
})
export class JdsBiObscuredModule {}
