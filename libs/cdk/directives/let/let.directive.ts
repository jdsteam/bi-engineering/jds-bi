import {
  Directive,
  Inject,
  Input,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

import { JdsBiLetContext } from './let-context';

/**
 * Works like *ngIf but does not have a condition — use it to declare
 * the result of pipes calculation (i.e. async pipe)
 */
@Directive({
  selector: '[jdsBiLet]',
})
export class JdsBiLetDirective<T> {
  @Input()
  jdsBiLet!: T;

  constructor(
    @Inject(ViewContainerRef) viewContainer: ViewContainerRef,
    @Inject(TemplateRef) templateRef: TemplateRef<JdsBiLetContext<T>>,
  ) {
    viewContainer.createEmbeddedView(templateRef, new JdsBiLetContext<T>(this));
  }

  /**
   * Asserts the correct type of the context for the template that `JdsBiLet` will render.
   *
   * The presence of this method is a signal to the Ivy template type-check compiler that the
   * `JdsBiLet` structural directive renders its template with a specific context type.
   */
  static ngTemplateContextGuard<T>(
    _dir: JdsBiLetDirective<T>,
    _ctx: unknown,
  ): _ctx is JdsBiLetContext<T> {
    return true;
  }
}
