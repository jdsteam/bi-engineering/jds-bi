import { NgModule } from '@angular/core';

import { JdsBiLetDirective } from './let.directive';

@NgModule({
  declarations: [JdsBiLetDirective],
  exports: [JdsBiLetDirective],
})
export class JdsBiLetModule {}
