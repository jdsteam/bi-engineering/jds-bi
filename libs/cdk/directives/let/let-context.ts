import { JdsBiContextWithImplicit } from '@jds-bi/cdk/interfaces';

import type { JdsBiLetDirective } from './let.directive';

/**
 * @internal
 */
export class JdsBiLetContext<T> implements JdsBiContextWithImplicit<T> {
  constructor(
    private readonly internalDirectiveInstance: JdsBiLetDirective<T>,
  ) {}

  get $implicit(): T {
    return this.internalDirectiveInstance.jdsBiLet;
  }

  get jdsBiLet(): T {
    return this.internalDirectiveInstance.jdsBiLet;
  }
}
