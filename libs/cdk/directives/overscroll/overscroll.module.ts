import { NgModule } from '@angular/core';

import { JdsBiOverscrollDirective } from './overscroll.directive';

@NgModule({
  declarations: [JdsBiOverscrollDirective],
  exports: [JdsBiOverscrollDirective],
})
export class JdsBiOverscrollModule {}
