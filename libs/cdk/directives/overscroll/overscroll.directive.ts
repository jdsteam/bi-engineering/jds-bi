import {
  Directive,
  ElementRef,
  HostBinding,
  Inject,
  Input,
  NgZone,
  Self,
} from '@angular/core';
import { jdsBiTypedFromEvent, jdsBiZonefree } from '@jds-bi/cdk/observables';
import { JdsBiDestroyService } from '@jds-bi/cdk/services';
import { JdsBiEventWith, JdsBiOverscrollMode } from '@jds-bi/cdk/types';
import {
  jdsBiCanScroll,
  jdsBiGetScrollParent,
  jdsBiIsElement,
} from '@jds-bi/cdk/utils/dom';
import { Observable } from 'rxjs';
import { filter, switchMap, takeUntil, tap } from 'rxjs/operators';

/**
 * Directive to isolate scrolling, i.e. prevent body scroll behind modal dialog
 */
@Directive({
  selector: '[jdsBiOverscroll]',
  providers: [JdsBiDestroyService],
})
export class JdsBiOverscrollDirective {
  @Input('jdsBiOverscroll')
  mode: JdsBiOverscrollMode | '' = 'scroll';

  constructor(
    @Inject(ElementRef) { nativeElement }: ElementRef<HTMLElement>,
    @Inject(NgZone) ngZone: NgZone,
    @Self() @Inject(JdsBiDestroyService) destroy$: Observable<void>,
  ) {
    jdsBiTypedFromEvent(nativeElement, 'wheel', { passive: false })
      .pipe(
        filter(() => this.enabled),
        jdsBiZonefree(ngZone),
        takeUntil(destroy$),
      )
      .subscribe((event) => {
        this.processEvent(
          event,
          !!event.deltaY,
          event.deltaY ? event.deltaY < 0 : event.deltaX < 0,
        );
      });

    jdsBiTypedFromEvent(nativeElement, 'touchstart', { passive: true })
      .pipe(
        switchMap(({ touches }) => {
          let { clientX, clientY } = touches[0];
          let deltaX = 0;
          let deltaY = 0;
          let vertical: boolean;

          return jdsBiTypedFromEvent(nativeElement, 'touchmove', {
            passive: false,
          }).pipe(
            filter(() => this.enabled),
            tap((event) => {
              // We have to have it in tap instead of subscribe due to variables in closure
              const changedTouch = event.changedTouches[0];

              deltaX = clientX - changedTouch.clientX;
              deltaY = clientY - changedTouch.clientY;
              clientX = changedTouch.clientX;
              clientY = changedTouch.clientY;

              if (vertical === undefined) {
                vertical = Math.abs(deltaY) > Math.abs(deltaX);
              }

              this.processEvent(
                event,
                vertical,
                vertical ? deltaY < 0 : deltaX < 0,
              );
            }),
          );
        }),
        jdsBiZonefree(ngZone),
        takeUntil(destroy$),
      )
      .subscribe();
  }

  get enabled(): boolean {
    return this.mode !== 'none';
  }

  @HostBinding('style.overscrollBehavior')
  get overscrollBehavior(): 'contain' | null {
    return this.enabled ? 'contain' : null;
  }

  private processEvent(
    event: JdsBiEventWith<Event, HTMLElement>,
    vertical: boolean,
    negative: boolean,
  ): void {
    const { target, currentTarget, cancelable } = event;

    if (
      !cancelable ||
      !jdsBiIsElement(target) ||
      (target as HTMLInputElement)?.type === 'range'
    ) {
      return;
    }

    // This is all what's needed in Chrome/Firefox thanks to CSS overscroll-behavior
    if (
      this.mode === 'all' &&
      ((vertical && !currentTarget.contains(jdsBiGetScrollParent(target))) ||
        (!vertical &&
          !currentTarget.contains(jdsBiGetScrollParent(target, false))))
    ) {
      event.preventDefault();

      return;
    }

    // This is Safari/IE/Edge fallback
    if (
      vertical &&
      ((negative && !jdsBiCanScroll(target, currentTarget, true, false)) ||
        (!negative && !jdsBiCanScroll(target, currentTarget, true, true)))
    ) {
      event.preventDefault();

      return;
    }

    if (
      !vertical &&
      ((negative && !jdsBiCanScroll(target, currentTarget, false, false)) ||
        (!negative && !jdsBiCanScroll(target, currentTarget, false, true)))
    ) {
      event.preventDefault();
    }
  }
}
