import { ElementRef, Inject, Injectable, NgZone } from '@angular/core';
import {
  ALWAYS_FALSE_HANDLER,
  ALWAYS_TRUE_HANDLER,
} from '@jds-bi/cdk/constants';
import {
  jdsBiTypedFromEvent,
  jdsBiZoneOptimized,
} from '@jds-bi/cdk/observables';
import { jdsBiIsElement } from '@jds-bi/cdk/utils';
import { merge, Observable } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

function movedOut({ currentTarget, relatedTarget }: MouseEvent): boolean {
  return (
    !jdsBiIsElement(relatedTarget) ||
    !jdsBiIsElement(currentTarget) ||
    !currentTarget.contains(relatedTarget)
  );
}

@Injectable()
export class JdsBiHoveredService extends Observable<boolean> {
  private readonly stream$ = merge(
    jdsBiTypedFromEvent(this.elementRef.nativeElement, `mouseenter`).pipe(
      map(ALWAYS_TRUE_HANDLER),
    ),
    jdsBiTypedFromEvent(this.elementRef.nativeElement, `mouseleave`).pipe(
      map(ALWAYS_FALSE_HANDLER),
    ),
    // Hello, Safari
    jdsBiTypedFromEvent(this.elementRef.nativeElement, `mouseout`).pipe(
      filter(movedOut),
      map(ALWAYS_FALSE_HANDLER),
    ),
  ).pipe(distinctUntilChanged(), jdsBiZoneOptimized(this.ngZone));

  constructor(
    @Inject(ElementRef) private readonly elementRef: ElementRef<Element>,
    @Inject(NgZone) private readonly ngZone: NgZone,
  ) {
    super((subscriber) => this.stream$.subscribe(subscriber));
  }
}
