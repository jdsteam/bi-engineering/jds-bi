import { NgModule } from '@angular/core';

import { JdsBiHoveredDirective } from './hovered.directive';

@NgModule({
  declarations: [JdsBiHoveredDirective],
  exports: [JdsBiHoveredDirective],
})
export class JdsBiHoveredModule {}
