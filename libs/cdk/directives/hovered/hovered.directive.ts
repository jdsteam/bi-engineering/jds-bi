import { Directive, Inject } from '@angular/core';
import { Observable } from 'rxjs';

import { JdsBiHoveredService } from './hovered.service';

@Directive({
  selector: '[jdsBiHoveredChange]',
  // eslint-disable-next-line @angular-eslint/no-outputs-metadata-property
  outputs: ['jdsBiHoveredChange'],
  providers: [JdsBiHoveredService],
})
export class JdsBiHoveredDirective {
  constructor(
    @Inject(JdsBiHoveredService)
    readonly jdsBiHoveredChange: Observable<boolean>,
  ) {}
}
