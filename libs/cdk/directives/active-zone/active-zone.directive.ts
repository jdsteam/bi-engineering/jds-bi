import {
  Directive,
  ElementRef,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  Optional,
  Output,
  SkipSelf,
} from '@angular/core';
import { jdsBiDefaultProp, jdsBiPure } from '@jds-bi/cdk/decorators';
import { jdsBiZoneOptimized } from '@jds-bi/cdk/observables';
import { JDS_BI_ACTIVE_ELEMENT } from '@jds-bi/cdk/tokens';
import { jdsBiArrayRemove } from '@jds-bi/cdk/utils';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skip, startWith } from 'rxjs/operators';

@Directive({
  selector:
    '[jdsBiActiveZone]:not(ng-container), [jdsBiActiveZoneChange]:not(ng-container), [jdsBiActiveZoneParent]:not(ng-container)',
  exportAs: 'jdsBiActiveZone',
})
export class JdsBiActiveZoneDirective implements OnDestroy {
  private subActiveZones: readonly JdsBiActiveZoneDirective[] = [];

  private jdsBiActiveZoneParent: JdsBiActiveZoneDirective | null = null;

  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('jdsBiActiveZoneParent')
  @jdsBiDefaultProp()
  set jdsBiActiveZoneParentSetter(zone: JdsBiActiveZoneDirective | null) {
    this.setZone(zone);
  }

  @Output()
  readonly jdsBiActiveZoneChange = this.active$.pipe(
    map((element) => !!element && this.contains(element)),
    startWith(false),
    distinctUntilChanged(),
    skip(1),
    jdsBiZoneOptimized(this.ngZone),
  );

  constructor(
    @Inject(JDS_BI_ACTIVE_ELEMENT)
    private readonly active$: Observable<Element | null>,
    @Inject(NgZone) private readonly ngZone: NgZone,
    @Inject(ElementRef) private readonly elementRef: ElementRef<Element>,
    @Optional()
    @SkipSelf()
    @Inject(JdsBiActiveZoneDirective)
    private readonly directParentActiveZone: JdsBiActiveZoneDirective | null,
  ) {
    if (this.directParentActiveZone) {
      this.directParentActiveZone.addSubActiveZone(this);
    }
  }

  ngOnDestroy(): void {
    if (this.directParentActiveZone) {
      this.directParentActiveZone.removeSubActiveZone(this);
    }

    if (this.jdsBiActiveZoneParent) {
      this.jdsBiActiveZoneParent.removeSubActiveZone(this);
    }
  }

  contains(node: Node): boolean {
    return (
      this.elementRef.nativeElement.contains(node) ||
      this.subActiveZones.some(
        (item, index, array) =>
          array.indexOf(item) === index && item.contains(node),
      )
    );
  }

  @jdsBiPure
  private setZone(zone: JdsBiActiveZoneDirective | null): void {
    if (this.jdsBiActiveZoneParent) {
      this.jdsBiActiveZoneParent.removeSubActiveZone(this);
    }

    if (zone) {
      zone.addSubActiveZone(this);
    }

    this.jdsBiActiveZoneParent = zone;
  }

  private addSubActiveZone(activeZone: JdsBiActiveZoneDirective): void {
    this.subActiveZones = [...this.subActiveZones, activeZone];
  }

  private removeSubActiveZone(activeZone: JdsBiActiveZoneDirective): void {
    this.subActiveZones = jdsBiArrayRemove(
      this.subActiveZones,
      this.subActiveZones.indexOf(activeZone),
    );
  }
}
