import { NgModule } from '@angular/core';

import { JdsBiActiveZoneDirective } from './active-zone.directive';

@NgModule({
  declarations: [JdsBiActiveZoneDirective],
  exports: [JdsBiActiveZoneDirective],
})
export class JdsBiActiveZoneModule {}
