export class JdsBiPureException extends Error {
  constructor() {
    super(`jdsBiPure can only be used with functions or getters`);
  }
}
