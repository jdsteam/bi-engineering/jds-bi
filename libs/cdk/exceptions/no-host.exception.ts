export class JdsBiNoHostException extends Error {
  constructor() {
    super(`Portals cannot be used without JdsBiPortalHostComponent`);
  }
}
