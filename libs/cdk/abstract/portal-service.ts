import {
  ComponentRef,
  EmbeddedViewRef,
  Injectable,
  TemplateRef,
} from '@angular/core';
import { JdsBiNoHostException } from '@jds-bi/cdk/exceptions';
import { PolymorpheusComponent } from '@tinkoff/ng-polymorpheus';

// TODO: find the best way for prevent cycle
import { AbstractJdsBiPortalHostComponent } from './portal-host';

/**
 * Abstract service for displaying portals
 */
@Injectable()
export abstract class AbstractJdsBiPortalService {
  protected host?: AbstractJdsBiPortalHostComponent;

  protected get safeHost(): AbstractJdsBiPortalHostComponent {
    if (!this.host) {
      throw new JdsBiNoHostException();
    }

    return this.host;
  }

  attach(host: AbstractJdsBiPortalHostComponent): void {
    this.host = host;
  }

  add<C>(component: PolymorpheusComponent<C, any>): ComponentRef<C> {
    return this.safeHost.addComponentChild(component);
  }

  remove<C>({ hostView }: ComponentRef<C>): void {
    hostView.destroy();
  }

  addTemplate<C>(templateRef: TemplateRef<C>, context?: C): EmbeddedViewRef<C> {
    return this.safeHost.addTemplateChild(templateRef, context);
  }

  removeTemplate<C>(viewRef: EmbeddedViewRef<C>): void {
    viewRef.destroy();
  }
}
