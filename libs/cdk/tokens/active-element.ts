import { DOCUMENT } from '@angular/common';
import { inject, InjectionToken } from '@angular/core';
import { WINDOW } from '@ng-web-apis/common';
import { jdsBiTypedFromEvent } from '@jds-bi/cdk/observables';
import {
  jdsBiGetActualTarget,
  jdsBiGetDocumentOrShadowRoot,
} from '@jds-bi/cdk/utils';
import { merge, Observable, of, timer } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  repeat,
  share,
  startWith,
  switchMap,
  take,
  takeUntil,
  withLatestFrom,
} from 'rxjs/operators';

import { JDS_BI_REMOVED_ELEMENT } from '@jds-bi/cdk/observables';

export const JDS_BI_ACTIVE_ELEMENT = new InjectionToken<
  Observable<EventTarget | null>
>(`[JDS_BI_ACTIVE_ELEMENT]: Active element on the document for ActiveZone`, {
  factory: () => {
    const removedElement$ = inject(JDS_BI_REMOVED_ELEMENT);
    const windowRef = inject(WINDOW);
    const documentRef = inject(DOCUMENT);
    const focusout$ = jdsBiTypedFromEvent(windowRef, `focusout`);
    const focusin$ = jdsBiTypedFromEvent(windowRef, `focusin`);
    const blur$ = jdsBiTypedFromEvent(windowRef, `blur`);
    const mousedown$ = jdsBiTypedFromEvent(windowRef, `mousedown`);
    const mouseup$ = jdsBiTypedFromEvent(windowRef, `mouseup`);

    return merge(
      focusout$.pipe(
        takeUntil(mousedown$),
        repeat({ delay: () => mouseup$ }),
        withLatestFrom(removedElement$),
        filter(([event, removedElement]) =>
          isValidFocusout(jdsBiGetActualTarget(event), removedElement),
        ),
        map(([{ relatedTarget }]) => relatedTarget),
      ),
      blur$.pipe(
        map(() => documentRef.activeElement),
        filter((element) => !!element?.matches(`iframe`)),
      ),
      focusin$.pipe(
        switchMap((event) => {
          const target = jdsBiGetActualTarget(event);
          const root = jdsBiGetDocumentOrShadowRoot(target) as Document;

          return root === documentRef
            ? of(target)
            : shadowRootActiveElement(root).pipe(startWith(target));
        }),
      ),
      mousedown$.pipe(
        switchMap((event) => {
          const actualTargetInCurrentTime = jdsBiGetActualTarget(event);

          return !documentRef.activeElement ||
            documentRef.activeElement === documentRef.body
            ? of(actualTargetInCurrentTime)
            : focusout$.pipe(
                take(1),
                map(
                  /**
                   * Do not use `map(() => jdsBiGetActualTarget(event))`
                   * because we have different result in runtime
                   */
                  () => actualTargetInCurrentTime,
                ),
                takeUntil(timer(0)),
              );
        }),
      ),
    ).pipe(distinctUntilChanged(), share());
  },
});

// Checks if focusout event should be considered leaving active zone
function isValidFocusout(
  target: any,
  removedElement: Element | null = null,
): boolean {
  return (
    // Not due to switching tabs/going to DevTools
    jdsBiGetDocumentOrShadowRoot(target).activeElement !== target &&
    // Not due to button/input becoming disabled
    !target.disabled &&
    // Not due to element being removed from DOM
    !removedElement?.contains(target)
  );
}

function shadowRootActiveElement(
  root: Document,
): Observable<EventTarget | null> {
  return merge(
    jdsBiTypedFromEvent(root, `focusin`).pipe(map(({ target }) => target)),
    jdsBiTypedFromEvent(root, `focusout`).pipe(
      filter(
        ({ target, relatedTarget }) =>
          !!relatedTarget && isValidFocusout(target),
      ),
      map(({ relatedTarget }) => relatedTarget),
    ),
  );
}
