import { InjectionToken, Provider, Type } from '@angular/core';
import { JdsBiFocusableElementAccessor } from '@jds-bi/cdk/interfaces';

export const JDS_BI_FOCUSABLE_ITEM_ACCESSOR =
  new InjectionToken<JdsBiFocusableElementAccessor>(
    `[JDS_BI_FOCUSABLE_ITEM_ACCESSOR]: A component that can be focused`,
  );

export function jdsBiAsFocusableItemAccessor(
  useExisting: Type<JdsBiFocusableElementAccessor>,
): Provider {
  return {
    provide: JDS_BI_FOCUSABLE_ITEM_ACCESSOR,
    useExisting,
  };
}
