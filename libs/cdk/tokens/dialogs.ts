import { InjectionToken, Provider, Type } from '@angular/core';
import { JdsBiAriaDialogContext } from '@jds-bi/cdk/interfaces';
import { Observable } from 'rxjs';

export const JDS_BI_DIALOGS = new InjectionToken<
  ReadonlyArray<Observable<readonly JdsBiAriaDialogContext[]>>
>(`[JDS_BI_DIALOGS]: A stream of dialogs`, {
  factory: () => [],
});

export function jdsBiAsDialog(
  useExisting: Type<Observable<readonly JdsBiAriaDialogContext[]>>,
): Provider {
  return {
    provide: JDS_BI_DIALOGS,
    multi: true,
    useExisting,
  };
}
