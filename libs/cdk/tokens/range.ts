import { isPlatformBrowser } from '@angular/common';
import { inject, InjectionToken, PLATFORM_ID } from '@angular/core';

export const JDS_BI_RANGE = new InjectionToken<Range>(
  `[JDS_BI_RANGE] SSR safe default empty Range`,
  {
    factory: () =>
      isPlatformBrowser(inject(PLATFORM_ID))
        ? new Range()
        : ({} as unknown as Range),
  },
);
