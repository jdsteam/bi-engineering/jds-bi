import { inject, InjectionToken } from '@angular/core';
import { NAVIGATOR } from '@ng-web-apis/common';
import { jdsBiIsIos } from '@jds-bi/cdk/utils';

export const JDS_BI_IS_IOS = new InjectionToken<boolean>(
  `[JDS_BI_IS_IOS]: iOS browser detection`,
  {
    factory: () => jdsBiIsIos(inject(NAVIGATOR)),
  },
);
