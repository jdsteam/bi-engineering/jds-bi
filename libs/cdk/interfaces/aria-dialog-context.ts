import { PolymorpheusComponent } from '@tinkoff/ng-polymorpheus';

export interface JdsBiAriaDialogContext {
  readonly component: PolymorpheusComponent<any, any>;
  readonly id: string;
  readonly createdAt: number;
}
