export interface JdsBiContextWithImplicit<T> {
  $implicit: T;
}
