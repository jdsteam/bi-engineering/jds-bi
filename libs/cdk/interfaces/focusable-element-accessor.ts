import { Observable } from 'rxjs';

export interface JdsBiNativeFocusableElement
  extends Element,
    HTMLOrSVGElement {}

/**
 * Public interface for any focusable component or directive
 */
export interface JdsBiFocusableElementAccessor {
  nativeFocusableElement: JdsBiNativeFocusableElement | null;
  focused: boolean;
  readonly focusedChange: Observable<boolean>;
}
