import { dirname, join } from 'path';
import type { StorybookConfig } from '@storybook/core-common';

export const rootMain: StorybookConfig = {
  stories: [],
  // webpackFinal: async (config, { configType }) => {
  //   // Make whatever fine-grained changes you need that should apply to all storybook configs
  //   // Return the altered config
  //   return config;
  // },
};

export const framework = {
  name: getAbsolutePath('@storybook/angular'),
  options: {},
};

export const docs = {
  autodocs: true,
};

function getAbsolutePath(value: string): any {
  return dirname(require.resolve(join(value, 'package.json')));
}
export const addons = [getAbsolutePath('@chromatic-com/storybook')];
