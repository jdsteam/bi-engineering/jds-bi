const { resolve } = require('path');
const { writeFileSync } = require('fs-extra');
const { version } = require('../package.json');

const file = resolve(__dirname, '..', 'libs', 'cdk', 'constants', 'version.ts');

console.log(file);

writeFileSync(
  file,
  `export const JDS_BI_VERSION = '${version}';
`,
  { encoding: 'utf-8' },
);
